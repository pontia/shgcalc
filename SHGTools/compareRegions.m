function compareRegions(dirName, xi, yi)
% xi, yi: polygon coordinates as returned by roipoly

if nargin < 3
     xi = [77.9910, 77.7853, 86.2192, 86.0135, 77.9910];
     yi = [63.7545, 72.3941, 72.3941, 63.1374, 63.7545];
end

if nargin == 0
     dirName = 'D:\results\scan_3\tiff\28\';
end

% Get vertices
x0 = round(min(xi)); x = round(max(xi));
y0 = round(min(yi)); y = round(max(yi));

d = dir(dirName);

% Preallocate
fNames = cell(1, numel(d) - 2);
n = 0;
for i = 1 : numel(d)
   
    if d(i).isdir == 1
        continue
    end
    
    fName = d(i).name;
    
    if strcmpi(fName(end - 3:end), '.tif')

        n = n + 1;
        fNames{n} = fullfile(dirName,fName);
        
    end
    
end

fprintf(1, 'Found %d images.\n', n);

fprintf(1, 'Processing... ');

% Preallocate
mnI = zeros(1, n);

for i = 1 : n

    img = imread(fNames{i});
    
    roi = img(y0 : y, x0 : x);
    
    mnI(i) = mean(roi(:));

end

fprintf(1, 'Done.\n');

% Images with strongest signal
[sMnI, indices] = sort(mnI, 'descend');
lastIndx = min(n, 10);
selFNames = fNames(indices(1 : lastIndx));

outDirName = fullfile(dirName, 'high');
mkdir(dirName,'high');

% Write the file with the intensity value prepended to maintain sequence
fprintf(1, 'Copying files... ');
for i = 1 : numel(selFNames)
    [~, f, e] = fileparts(selFNames{i});
    outFName = fullfile(outDirName, [sprintf('%06.0f', sMnI(i)), '_', f, e]);
    copyfile(selFNames{i}, outFName);
end
fprintf(1, 'Done.\n');

   