function B=nrm(A,d,p)
% nrm normalize a matrix to the range [0..1] (double), [0..255] (uint8) or [0..65535] (uint16)
%
% SYNOPSIS B=nrm(A,d)
%
% INPUT    A : matrix (image)
%          d : (optional: default = 1)
%               bit depth (1, 8 or 16)
%                  1 - [0..1]     - output class: double
%                  8 - [0..255]   - output class: uint8
%                 16 - [0..65535] - output class: uint16
%              alternatively, d can also be the class of the matrix:
%           'double' - [0..1]     - output class: double
%            'uint8' - [0..255]   - output class: uint8
%           'uint16' - [0..65535] - output class: uint16
%                    other classes are not supported.
%
%          p : (optional) fraction (0 <= p <= 1) of dark and high 
%              intensities to be dropped; default: all intensities used.
%              Setting p = 0.02 will remove 2% from the bottom range and 
%              2% from the high-range (i.e. a total of 4%).
%
%              Alternatively, p can be [min max] - the range to be used 
%              for stretching (overrides the min and max values of A).
%
%
% OUTPUT   B : normalized A (with class change)

%--------------------------------------------------------------------------
%
%   The contents of this file are subject to the Mozilla Public License
%   Version 1.1 (the "License"); you may not use this file except in
%   compliance with the License. You may obtain a copy of the License at
%   http://www.mozilla.org/MPL/
%
%   Software distributed under the License is distributed on an "AS IS"
%   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
%   License for the specific language governing rights and limitations
%   under the License.
%
%   The Original Code is "Qu for MATLAB".
%
%   The Initial Developer of the Original Code is Aaron Ponti.
%   All Rights Reserved.
%
%--------------------------------------------------------------------------

% By default, return a [0..1] double matrix using all values
if nargin == 1
    d = 1; 
    p = [];
end
if nargin == 2
    p = [];
end

% Check p
if ~isempty( p )
    if ~isnumeric( p )
        error( [ 'p must be either a scalar in the range 0 .. 1 or a ', ...
            '1-by-2 vector with [minValue maxValue] for normalization.' ] );
    end
    if isscalar( p )
        if p < 0 || p > 1
            error( 'p must be between 0 and 1.');
        end
    else
        if numel( p ) ~= 2
            error( 'p must be [min max].');
        end
    end
end

% Make sure p is of class double
p = double( p );

% Make sure A is double
A = double( A );

% Should be discard some extreme values?
if ~isempty( p )
    
    % p is the fraction of intensities to discard
    if isscalar( p )
        
        sA = sort( A( : ) );
        n = numel( sA );
        thresh = round( p * n );
        mn =  sA( thresh + 1 );
        mx = sA( n - thresh - 1 );
    
        % Check
        if mx < mn
            quError( [ 'Please reduce p to avoid that maxValue falls ', ...
                'lower than minValue.' ] );
            return
        end

    else
        
        % p is the [minValue maxValue] vector
        mn = p( 1 );
        mx = p( 2 );
        
        if mx < mn
            quError( 'maxValue must be larger than minValue' );
            return
        end
        
    end
    
else

    % Get the extrema
    mx = double( max( A( : ) ) );
    mn = double( min( A( : ) ) );

end

% Normalize A to [0..1]

% We consider two degenerate cases and the standard one
if ( mx == 0 && mn == 0 )

    % Degenerate case 1
    A = zeros( size( A ) );
    
elseif ( mx == mn && mn > 0 )
    
    % Degenerate case 2
    A = ones( size( A ) );
    
else
    
    % Standard case
    A = ( A - mn ) / ( mx - mn );
    
end

% If A is complex (e.g. a transform)
B = abs( A );

% Conversion
switch d
    case { 1, 'double' }
        % Do nothing
    case { 8, 'uint8' }
        B = uint8( round( ( 2^8 - 1 ) * B ) );
    case { 16, 'uint16' }
        B = uint16( round( ( 2^16 - 1 ) * B ) );
    otherwise
        error( 'The entered argument d is not valid' );
end
