function exportSHGArrayToTIFFImages(dirName, outDirName)
% dirName: directory where the raw files are stored
%
% Remark: this can be run incrementally. It will only process files that do
% not yet have a corresponding (Set of) TIFF file(s) in the output folder.


if nargin == 0
    dirName = 'D:\results\';
    outDirName = fullfile(dirName, 'tiff');
    mkdir(outDirName);
elseif nargin == 1
    outDirName = fullfile(dirName, 'tiff');
    mkdir(outDirName); 
elseif nargin == 2
    % All set
end

rexp = '-sz=\((\d+),(\d+),(\d+)\)';
strg = sprintf('%%.%dd', 3);


d = dir(dirName);

for i = 1 : numel(d)
   
    if d(i).isdir == 1
        continue
    end
    
    fName = d(i).name;
    
    if strcmpi(fName(end - 3:end), '.raw')

        disp(['Processing ', fName]);
        
        % Get the size
        [tokens, startIndx] = regexp( fName, rexp, 'tokens' );
        sz = [ ...
            str2double(tokens{1}{1}), ...
            str2double(tokens{1}{2}), ...
            str2double(tokens{1}{3})];

        % Is there something else after the -sz() block?
        afterIndex = startIndx + 8 + ...
            length(tokens{1}{1}) + ...
            length(tokens{1}{2}) + ...
            length(tokens{1}{3});
        if strcmpi(fName(afterIndex : end), '.raw') == 1
                
            % There is only the extension after the -sz() block
            suffix = '';

        else
                
            % Do not forget the rest of the name
            suffix = fName(afterIndex : end - 4);

        end

        % Since at least one z plane is expected, if the file was processed
        % already, there will be at least a -z=001.tif
        testFName = [fName(1 : startIndx - 1), ...
            suffix, ...
            '_z=', ...
            sprintf(strg, 1), '.tif'];
        testFName = fullfile(outDirName, testFName);
        if exist(testFName, 'file')
            disp('File exists... Skipping.');
            continue;
        end
 
        % Read the file
        h = fopen(fullfile(dirName,fName));
        raw = fread(h, Inf, 'double');
        fclose(h);
        
        % Reshape
        raw3D = reshape(raw, sz);
        
        for z = 1 : sz(3)
           
            % Get plane (and transpose)
            img = raw3D(:, :, z)';

            % Correct
            img = imadjust(img, []);

            % Convert to 16-bit tiff
            imgC = nrm(img, 16);

            % Build file name
            outFName = [fName(1 : startIndx - 1), ...
                suffix, ...
                '_z=', ...
                sprintf(strg, z), '.tif'];
            outFName = fullfile(outDirName, outFName);
            
            % Save
            imwrite(imgC, outFName, 'Compression', 'lzw');
            
        end
        
    end

    
end