reload("SHGcalc2_64.jl")

function printsize(x)
        s = size(x)
        "$(s[1])x$(s[2])x$(s[3])"
end

function MultiParameterScan(;DZ = 0.25, DR = 0.132, sarcowidths = [0.4:0.05:0.6], phases = [0.0:0.1:0.5], NAs = [0.4:0.1:0.7], ex = myfloat([0:0.1:1]), ey = myfloat([0:0.1:1]), ez = myfloat([0:0.1:1]), thetas=myfloat([-pi/6.0:0.1,pi/6.0]),phis=myfloat([-pi/6.0:0.1,pi/6.0]), resultpath = "D:/results/",jitter=false)


    # Constants
    const downsamp = 1
    const downsampz = 3

    const nn = false
   
    const efield_angle_x = ex
    const efield_angle_y = ey
    const efield_angle_z = ez

    # Voxel size with downsampling
    const dz = DZ / float(downsampz)
    const dr = DR / float(downsamp)

    # Load normals
    const normals = loadnormalsOneSide(homepath*datapath*"NORMALS.csv",DZ,DR)

    # Iterate over all parameter combinations
    for NA in NAs

        # Optical parameters
        local p = OpticParams(0.85,NA);

        ### THIS WE WON'T NEED SINCE WE SET THE PHASE EXPLICITLY
        ### const k2w::Floaty = myfloat(2.0 *pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
        ### local dk = phasematch(p.kw,k2w)

        for sarcowidth in sarcowidths

            # Process normals
            bignormals = nn ? [] : growNormals(normals,sarcowidth,jitter)
            (chi,chilist) = nn ?  makeSusceptibilityNN(normals,dr,dr,dz,sarcowidth) : makeSusceptibilitySpartan(bignormals,dr,dr,dz);
            
            strsarcowidth = @sprintf("%.2f", sarcowidth)
            
            for dk in phases
                    for theta in thetas
                            for phi in phis
                                local e_field = get_e_field(dr,dz,4.0,6.0,p,dk,theta,phi);
                                for ax in efield_angle_x
                                    for ay in efield_angle_y
                                        for az in efield_angle_z

                                            efield_angle = myfloat([ax, ay, az])

                                            (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle);use_jitter=jitter);
                                            local shg = convChiE2(chiX,chiY,chiZ,e_field;ds=downsamp,dsz=downsampz);
                                            strna = @sprintf("%.2f", NA)
                                            strphase = @sprintf("%.4f", dk)
                                            strpointing = @sprintf("(%.2f %.2f)",theta,phi)
                                            strefa = @sprintf("(%.4f %.4f %.4f)", ax, ay, az)
                                           # outfile = open(resultpath*"shg-NA=$(strna)-sarcowidth=$(strsarcowidth)-phase=$(strphase)-efa=$(strefa)-sz="*printsize(shg)*".raw","w")
                                            outfile = open(resultpath*"shg-NA=$(strna)-sarcowidth=$(strsarcowidth)-phase=$(strphase)-efa=$(strefa)-sz=$(size(shg))-pointing=$(strpointing).raw","w")
                                            write(outfile,shg)
                                            close(outfile)

                                        end

                                    end

                                end
                            end
                    end

            end
        
        end
    
    end

end
