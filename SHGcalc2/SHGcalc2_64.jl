#cd("/lfs1/users/nhodas/Dropbox/Verniers\ Images/interpolated/")

homepath = "/lfs1/users/nhodas/Dropbox"
#datapath = "Dropbox/Verniers\ Images/GroundTruth/analysis_24/"
datapath = "/shgcalc/shgcalc/data/03_results/analysis_24_extend_5/" #"Dropbox/Verniers\ Images/extended_regions/analysis_24_extend_5/" #

#homepath = "D:/data/"
#datapath = "analysis_24/"
#resultpath = "D:/results/"
resultpath = homepath*"/shgcalc/results/"

#establish the numeric types : 64-bit floats
myfloat(x::Number) = float64(x)
myfloat(x::Array{Int64,1}) = float64(x)
myfloat{T<:Number}(x::Array{T,1}) = float64(x)
const Floaty = Float64
const MyCplx = Complex128

const USE_C_LIBRARIES = false

macro remove(ex)
end

###########Helper Functions
###########Julia is faster when you do individual addressing instead of vector operations (as of 0.2)

#in place element-wise multiplication
#this is faster than a *= b
function myproduct!(a2::Array{MyCplx,3},b2::Array{ MyCplx,3})
    const (x,y,z) = size(a2)
    for i = 1:length(a2)
        a2[i] *= b2[i]
    end
    a2
end

#I think myassign! is faster than
#a[1:x,1:y,1:z] = anew
function myassign!(a::Array{MyCplx,3},x::Int,y::Int,z::Int,anew::Array{MyCplx,3})
    for i = 1:x; for j = 1:y; for k = 1:z;
        a[i,j,k] = anew[i,j,k]
    end; end; end;
    a
end

function myassign2!(a::Array{MyCplx,3},x::Int,y::Int,z::Int,anew::Array{MyCplx,3})
    (x2,y2,z2) = size(a)
    x2 = div(x2 - x,2)
    y2 = div(y2-y,2)
    z2 = div(z2-z,2)
    for i = 1:x; for j = 1:y; for k = 1:z;
        a[x2+i,y2+j,z2+k] = anew[i,j,k]
    end; end; end;
    a
end


function myabs2!(z::Array{MyCplx,3})
    const (sx,sy,sz) = size(z)
    for i = 1:sx; for j = 1:sy; for k = 1:sz
        z[i,j,k] = abs2(z[i,j,k])
    end;end;end
    z
end



function myabs2sum!(out::Array{Floaty,3},z::Array{MyCplx,3})
    @assert size(out) == size(z)
    if USE_C_LIBRARIES
    #    ccall((:myabs2sum,"SHGcalc.so"),Void,(Ptr{Floaty},Ptr{MyCplx},Int),out,z,length(z));
    else
        for i = 1:length(out)
            out[i] += abs2(z[i])
        end
    end
    out
end

function mynorm(z::Array{Floaty,1})
    local s = abs2(z[1])
    for i = 2:length(z)
        s += abs2(z[i])
    end
    sqrt(s)
end


#Do 3d convolution using FFT's.
function myconv3d(a::Array{MyCplx,3},b::Array{MyCplx,3},periodic=false)
    const (ax,ay,az) = size(a);
    const (bx,by,bz) = size(b);
    const mx = int((ax + bx - 1))
    const my = int((ay + by - 1))
    const mz = int((az + bz - 1))
    local a2::Array{MyCplx,3} = zeros(MyCplx,mx,my,mz)
    local b2::Array{MyCplx,3} = zeros(MyCplx,mx,my,mz)
    
    myassign2!(a2,ax,ay,az,a) #my assign
    myassign2!(b2,bx,by,bz,b)

    #do in-place FFT into a2
    if USE_C_LIBRARIES
    #    ccall((:conv3d,"SHGcalc.so"),Void,(Ptr{MyCplx},Ptr{MyCplx},Int,Int,Int),a2,b2,mx,my,mz)       
    else
        ifft!(myproduct!(fft!(a2),fft!(b2)));
    end
    fftshift(a2)[int(bx/2):(int(bx/2) + ax),int(by/2):(int(by/2) + ay),int(bz/2):(int(bz/2) + az)]
end

function myconv3d_2(a::Array{MyCplx,3},b::Array{MyCplx,3})
    const (ax,ay,az) = size(a);
    const (bx,by,bz) = size(b);
    const mx = max(ax,bx)
    const my = max(ay,by)
    const mz = max(az,bz)
    local a2::Array{MyCplx,3} = zeros(MyCplx,mx,my,mz)
    local b2::Array{MyCplx,3} = zeros(MyCplx,mx,my,mz)

    #a2[1:ax,1:ay,1:az] = a;
    #b2[1:bx,1:by,1:bz] = b;
    myassign!(a2,ax,ay,az,a) #my assign
    myassign!(b2,bx,by,bz,b)

    #do in-place FFT into a2
    if USE_C_LIBRARIES
    #    ccall((:conv3d,"SHGcalc.so"),Void,(Ptr{MyCplx},Ptr{MyCplx},Int,Int,Int),a2,b2,mx,my,mz)
    else
        ifft!(myproduct!(fft!(a2),fft!(b2)));
    end
    fftshift(a2)
   #a2
end

#Optical Parameteres for calculations
#in try block because it can only be declared once
try
immutable OpticParams
    lambda0::Floaty # 0.850
    n1::Floaty  # 1.4
    # n2::Floaty
    lambda::Floaty # = lambda0/n1
    NA::Floaty # = 1.40
    kw::Floaty # = 2.0   *pi/lambda
    kw2::Floaty # =2.0   *pi/(lambda/(2*n2))
    # dk::Floaty# 2*kw-kw
    w0::Floaty # = lambda/(pi*NA)
    w0sq::Floaty # = w0*w0
    zr::Floaty # = pi*w0sq/lambda
    b::Floaty # = kw w0^2
end
end

#helper function to construct optical params from 2 inputs
function OpticParams(lambda0in::Real,NAin::Real)
    local lambda0 = myfloat(lambda0in);
    local NA = myfloat(NAin);
    local n1 = myfloat(refractiveindex(lambda0));
    local lambda = lambda0/n1
    local kw = myfloat(2.0*pi)/lambda
    local kw2 = myfloat(4.0*pi)/(lambda0/myfloat(refractiveindex(lambda0/2.0)))
    local w0 = lambda/myfloat(pi*NA)
    local w0sq = w0*w0
    local zr = myfloat(pi*w0sq/lambda)
    local b =  myfloat(kw*w0sq) #MyCplx(0.0   ,-myfloat(kw*w0sq/2.0 ))
    OpticParams(lambda0,n1,lambda,NA,kw,kw2,w0,w0sq,zr,b)
 end



#page 51 of thesis. wavelength in microns
function refractiveindex(lambda::Real)
    sqrt(2.116 + 22.45*lambda^2/(lambda^2-53.9343))
end

function phasematch(kw::Floaty,k2w::Floaty)
    (-2.0  * kw + k2w)
end

function phasematch(p::OpticParams)
    (-2.0*p.kw + p.kw2)
end


const p = OpticParams(0.85,1.4)

#see page 81 of NOH Thesis
function efield(x::Floaty,y::Floaty,z::Floaty, p::OpticParams)
    #println("$x,$y,$z,$(p.cbhalf),$(p.w0sq)")
    const c0:: MyCplx =  1.0 + 2.0im*z/p.b 
    const k2w::Floaty = myfloat(2.0 *pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    exp(-(x^2+y^2)/(p.w0sq*c0)+0.5 *im*z*phasematch(kw,k2w))/c0  #phase dk is here. factor of 0.5 because it will be squared later
end


#the square of the e-field
function efieldsq(x::Floaty,y::Floaty,z::Floaty, p::OpticParams)
    #println("$x,$y,$z,$(p.cbhalf),$(p.w0sq)")
    const c0:: MyCplx =  1.0 + 2.0im*z/p.b 
    const k2w::Floaty = myfloat(2.0 *pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    exp(-2.0*(x^2+y^2)/(p.w0sq*c0)+1im*z*phasematch(p.kw,k2w))/(c0*c0)  #phase dk is here
end

function efieldsq(xin::Floaty,yin::Floaty,zin::Floaty, p::OpticParams, dk::Floaty,theta=0.0,phi=0.0)
    #println("$x,$y,$z,$(p.cbhalf),$(p.w0sq)")
    #const c0:: MyCplx =  1.0 + 2.0im*z/p.b
    #const k2w::Floaty = myfloat(2.0 *pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    #exp(-2.0*(x^2+y^2)/(p.w0sq*c0)+1im*z*dk)/(c0*c0)  #phase dk is here
    local cb = cos(theta)
    local sb = sin(theta)
    local cg = cos(phi)
    local sg = sin(phi)
    local ca = 1.0
    local sa = 0.0

    const x = (cg*cb*ca-sg*sa)*xin + (cg*cb*sa+sg*ca)*yin + (-cg*sb)*zin
    const y = (-sg*cb*ca-cg*sa)*xin + (-sg*cb*sa + cg*ca)*yin + (sg*sb)*zin
    const z = (sb*ca)*xin + (sb*sa)*yin + cb*zin

    const c0 = 1.0+(z/p.zr)^2
    const wz = p.w0*sqrt(c0)
    const Rz = (z+1e-14)*(1.0+(p.zr/(z+1e-14))^2)
    const rsq = (x*x+y*y)
    const e0 = exp(-rsq/(wz*wz) + im*(p.kw*rsq/(2.0*Rz) - atan(z/p.zr)))/sqrt(c0)
    e0*e0*exp(im*dk*z)
end



#calculate 3D laser field
#currently set to return the square of the field
function get_e_field(dr::Floaty,dz::Floaty,radius::Floaty,depth::Floaty,p::OpticParams)
    numr = int(2*radius/dr);
    numz = int(2*depth/dz);
    e_field = zeros( MyCplx,numr,numr,numz);
    dr = myfloat(dr)
    dz = myfloat(dz)
    radius = myfloat(radius)
    depth = myfloat(depth)
    const k2w::Floaty = myfloat(2.0 *pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    println("Phase match: $(phasematch(p.kw,k2w))")
    for xi = 1:numr
        for yi= 1:numr
            for zi = 1:numz
               @inbounds e_field[xi,yi,zi] = efieldsq(dr*myfloat(xi) - radius,dr*myfloat(yi) - radius,dz*myfloat(zi)-depth,p)*dr*dr*dz
            end
        end
    end
    e_field
end

function get_e_field(dr::Floaty,dz::Floaty,radius::Floaty,depth::Floaty,p::OpticParams,dk::Float64,theta::Floaty=0.0,phi::Floaty=0.0)
    numr = int(2*radius/dr);
    numz = int(2*depth/dz);
    e_field = zeros( MyCplx,numr,numr,numz);
    dr = myfloat(dr)
    dz = myfloat(dz)
    radius = myfloat(radius)
    depth = myfloat(depth)
    const k2w::Floaty = myfloat(2.0 *pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    println("Phase match: $dk")
    for xi = 1:numr
        for yi= 1:numr
            for zi = 1:numz
               @inbounds e_field[xi,yi,zi] = efieldsq(dr*myfloat(xi) - radius,dr*myfloat(yi) - radius,dz*myfloat(zi)-depth,p,dk,theta,phi)*dr*dr*dz
            end
        end
    end
    e_field
end

#data type for recording normals from CSV file
try
immutable NormPoint
    x::Floaty
    y::Floaty
    z::Floaty
    nx::Floaty
    ny::Floaty
    nz::Floaty
    function NormPoint{T <: FloatingPoint}(x::T,y::T,z::T,nx::T,ny::T,nz::T)
            new(myfloat(x),myfloat(y),myfloat(z),myfloat(nx/sqrt(nx*nx + ny*ny+nz*nz)),myfloat(ny/sqrt(nx*nx + ny*ny+nz*nz)),myfloat(nz/sqrt(nx*nx + ny*ny+nz*nz)));
    end
end
end

#constructors for NormPoint
NormPoint(n::NormPoint,shift::Floaty) = NormPoint(n.x + shift*n.nx,n.y + shift*n.ny,n.z + shift*n.nz,n.nx,n.ny,n.nz);

flip(n::NormPoint) = NormPoint(n.x,n.y,n.z,-n.nx,-n.ny,-n.nz)

#dz = 0.25
import Base.isnan
isnan(n::NormPoint) = isnan(n.x) || isnan(n.y) || isnan(n.z) || isnan(n.nx)|| isnan(n.ny) || isnan(n.nz)

#Read normals from csv file
#dr = 0.132 micron per pixel
const DR = 0.132
function loadnormalsAll(filename::ASCIIString,dz::Floaty,dr::Floaty)
    (indata,header) = readdlm(filename,'\t',Floaty,has_header=true)
    (len,wid) = size(indata)
    goodlen = 0
    local n::NormPoint
    for i = 2:len
        if ~isnan(NormPoint(indata[i,4],indata[i,5],dz*indata[i,1],indata[i,6],indata[i,7],indata[i,8]))  
            goodlen += 1
       end
    end
    
    normals = Array(NormPoint,goodlen)
    c = 1
    for i = 2:len
        n = NormPoint(dr*indata[i,4],dr*indata[i,5],dz*indata[i,1],indata[i,6],indata[i,7],indata[i,8])
        if  !isnan(n)
          normals[c] = n
          c += 1
        end
    end
    normals
end

function loadnormalsOneSide(filename::ASCIIString,dz::Floaty,dr::Floaty)
    (indata,header) = readdlm(filename,'\t',Floaty,has_header=true)
    (len,wid) = size(indata)
    goodlen = 0
    normals = Array(NormPoint,2*len)
    local n::NormPoint
    c = 1
    for i = 1:len
        n = NormPoint(dr*indata[i,4],dr*indata[i,5],dz*indata[i,1],indata[i,6],indata[i,7],indata[i,8])
        if  !isnan(n)
                if indata[i,3] == 1
                    normals[c] = n
                    normals[c+1] = flip(n)
                    c += 2
                end
        end
    end
    normals[1:(c-1)]
end


#extrapolate out the normals with resolution DX for  distance width
function growNormals(normals::Array{NormPoint,1},widthin::Real,dojitter=false)
    const len = length(normals)
    const width = myfloat(widthin)
    const DX::Floaty = 0.1*DR
    const NUM_STEPS = int(width/ DX) - 1
   
    const newlen = NUM_STEPS*len
    local newnormals = Array(NormPoint,newlen)
    println("NUM_STEPS $NUM_STEPS")
    println("totallen $newlen")
    local i = 0
    local JITTER_FACTOR = 0.2
    local n2::NormPoint
    local newnorm::Float64
    for n in normals
        newnormals[i+=1] = n
        for j = 1.0:(NUM_STEPS-1.0)
            if dojitter
                
                n2nx = n.nx + JITTER_FACTOR*(0.5 - rand())
                n2ny = n.ny + JITTER_FACTOR*(0.5 - rand())
                n2nz = n.nz + JITTER_FACTOR*(0.5 - rand())
                local newnorm = sqrt(n.nx*n.nx + n.ny*n.ny + n.nz*n.nz)
                n2nx /= newnorm
                n2ny /= newnorm
                n2nz /= newnorm
                
                n2x = n.x + JITTER_FACTOR*(0.5 - rand()) + j*DX*n2nx
                n2y = n.y + JITTER_FACTOR*(0.5 - rand()) + j*DX*n2ny
                n2z = n.z + JITTER_FACTOR*(0.5 - rand()) + j*DX*n2nz
                newnormals[i+=1] = NormPoint(n2x,n2y,n2z,n2nx,n2ny,n2nz)
            else
                newnormals[i+=1] = NormPoint(n,j*DX)
            end
        end
    end
    newnormals
end

#helper method to turn position into discrete index
function get_index{T}(x::T,dx::T)
    int(floor(x/dx)) + 1
end


function fill(obj::Array{Array{Floaty,1},3},f::Array{Floaty,1})
    const (x,y,z) = size(obj)
    for i = 1:x
        for j = 1:y
            for k = 1:z
                obj[i,j,k] = f
            end
        end
    end
end

function fill(obj::Array{Array{Floaty,1},2},f::Array{Floaty,1})
    const (x,y) = size(obj)
    for i = 1:x
        for j = 1:y
                obj[i,j] = f
        end
    end
end

function inbounds(x::Array{Int,1},xw::Int,yw::Int,zw::Int)
    (0 < x[1]) && (0 < x[2]) && ( 0 < x[3]) && ( x[1] <= xw) && (x[2] <= yw) && (x[3] <= zw)
end

#Calculate the susceptibility
#return the susceptibility tensor chi[x,y,z] = [chix,chiy,chiz] at (x,y,z)
#also return a dictionary of all non-zero points chidict[[x,y,z]] = [chix,chiy,chiz]
const RETRO = 0.25
function makeSusceptibilitySpartan(normals::Array{NormPoint,1},dx::Real,dy::Real,dz::Real)

    println("Constructing Susceptibility Matrix With Resolution x:$dx y:$dy z:$dz")

    const retro::Floaty = RETRO#2.3 #direction to back up to attempt to get different surfaces to meet at hypothetical M-line
    
    #These could all be vectorized, but I think avoiding temporary arrays makes it a bit faster
    local xmin::Floaty = normals[1].x
    local ymin::Floaty = normals[1].y
    local zmin::Floaty = normals[1].z
    local xmax::Floaty = normals[1].x
    local ymax::Floaty = normals[1].y
    local zmax::Floaty = normals[1].z
    for i = 2:length(normals)
        xmin = min(xmin,normals[i].x)
        ymin = min(ymin,normals[i].y)
        zmin = min(zmin,normals[i].z)
        xmax = max(xmax,normals[i].x)
        ymax = max(ymax,normals[i].y)
        zmax = max(zmax,normals[i].z)
    end
    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xmax: ", xmax," ymax: ", ymax, " zmax: ", zmax)
    
    xmin -= retro
    ymin -= retro
    zmin -= retro
    
    xwidth = int((xmax-xmin)/dx) 
    ywidth = int((ymax-ymin)/dy) 
    zwidth = int((zmax-zmin)/dz)
    

    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xw: ", xwidth," yw: ", ywidth, " zw: ", zwidth)
    
    local chi = Array(Array{Floaty,1},xwidth,ywidth,zwidth)
    local chicount = zeros(size(chi))
    fill(chi,zeros(Floaty,3))

    
    chilist = Set{Array{Int,1}}()
    println("chi size: ", size(chi))
    
    #some helper variables so you don't have to garbage collect after each loop
    local x::Array{Floaty,1}
    local idx::Array{Int,1}
    local cx,cy,cz, ii,jj,kk
    const zero3 = zeros(Floaty,3)
    numdone = 0
    
    progress = 0
    for n in normals
        cx = n.x-xmin-retro*n.nx
        cy = n.y-ymin-retro*n.ny
        cz = n.z-zmin-retro*n.nz
    
        idx = [int(get_index(cx,dx)),int(get_index(cy,dy)),int(get_index(cz,dz))]
        if inbounds(idx,xwidth,ywidth,zwidth)
             chi[idx[1],idx[2],idx[3]] += [n.nx,n.ny,n.nz]

             chicount[idx[1],idx[2],idx[3]] += 1.0
            #add!(chilist,idx)
        end
         if progress % 5000000 == 0; println(progress/length(normals)); end; progress += 1
    end

    for i = 1:length(chi)
        if chicount[i] > 0.0
            chi[i] /= chicount[i]
        end
    end

    (chi,chilist)
end

#reload("entropy_estimators.jl")
#EntropyPy.setnorm(2)

function makeSusceptibilityNN(normals::Array{NormPoint,1},dx::Real,dy::Real,dz::Real,buffer=0.5,test=false)


    println("Constructing Susceptibility Matrix With Resolution x:$dx y:$dy z:$dz")

    #These could all be vectorized, but I think avoiding temporary arrays makes it a bit faster
    local xmin::Floaty = normals[1].x
    local ymin::Floaty = normals[1].y
    local zmin::Floaty = normals[1].z
    local xmax::Floaty = normals[1].x
    local ymax::Floaty = normals[1].y
    local zmax::Floaty = normals[1].z
    for i = 2:length(normals)
        xmin = min(xmin,normals[i].x)
        ymin = min(ymin,normals[i].y)
        zmin = min(zmin,normals[i].z)
        xmax = max(xmax,normals[i].x)
        ymax = max(ymax,normals[i].y)
        zmax = max(zmax,normals[i].z)
    end
    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xmax: ", xmax," ymax: ", ymax, " zmax: ", zmax)


    xwidth = int((xmax-xmin + 2.0*buffer)/dx)
    ywidth = int((ymax-ymin + 2.0*buffer)/dy)
    zwidth = int((zmax-zmin + 2.0*buffer)/dz)


    println("xw: ", xwidth," yw: ", ywidth, " zw: ", zwidth)


    #Make KD Tree out of normals position

    const LEN_NORMS = length(normals)
    const SKIP_FACTOR = 1
    local normPosVector = Array(Float64, length([1:SKIP_FACTOR:LEN_NORMS]),3)
    local normals2 = Array(NormPoint,length([1:SKIP_FACTOR:LEN_NORMS]))
    local j = 0
    for i = 1:SKIP_FACTOR:LEN_NORMS
        normPosVector[j+=1,1] = normals[i].x
        normPosVector[j,2] = normals[i].y
        normPosVector[j,3] = normals[i].z
        normals2[j] = normals[i]
    end
     println("making KDTree of size $(length(normPosVector))")

    local normtree = EntropyPy.makekdtree(normPosVector)

    local chi = Array(Array{Floaty,1},xwidth,ywidth,zwidth)
    local chicount = zeros(size(chi))
    fill(chi,zeros(Floaty,3))



    println("Filling chi")
    if test
        @time EntropyPy.query(normtree,[0.0,0.0,0.0]',1)[2][1]
        n_test = EntropyPy.query(normtree,[normals2[1].x,normals2[1].y,normals2[1].z]',1)[2][1] + 1
        println("$n_test")
        println("$([normals2[1].nx,normals2[1].ny,normals2[1].nz])")
        println("$([normals2[n_test].nx,normals2[n_test].ny,normals2[n_test].nz])")
    end

    local pos = Array(Float64,1,3)
    local nn_idx::Int
    local progress = 0
     (xl,yl,zl) = size(chi)
    local xrange = 1:xl
    local yrange = 1:yl
    local zrange = 1:zl
    if test
        zrange = 10:10
    end
    for i = xrange; for j = yrange; for k = zrange
        pos[1] = dx*i + xmin - buffer
        pos[2] = dy*j + ymin - buffer
        pos[3] = dz*k + zmin - buffer
        nn_idx = EntropyPy.query(normtree,pos,1)[2][1] + 1
        chi[i,j,k] = [normals2[nn_idx].nx,normals2[nn_idx].ny,normals2[nn_idx].nz]
        if progress % 10000 == 0; println(progress/length(chi)); end; progress += 1
    end;end;end;

    chilist = Set{Array{Int,1}}()
    println("chi size: ", size(chi))

    (chi,chilist)
end


#you can get the size of the chi tensor from chidict[SIZE_INDEX] = [lx,ly,lz]
const SIZE_INDEX = [-1,-34,-234]
#sarcoradius = 1.0   
#dx=dy=0.132,dz=0.25
function makeSusceptibilityAll(normals::Array{NormPoint,1},sr::Real,dx::Real,dy::Real,dz::Real)

    const sarcoradius = sr
    const retro::Floaty = 2.3 #direction to back up to attempt to get different surfaces to meet at hypothetical M-line
    
    #These could all be vectorized, but I think avoiding temporary arrays makes it a bit faster
    local xmin::Floaty = normals[1].x
    local ymin::Floaty = normals[1].y
    local zmin::Floaty = normals[1].z
    local xmax::Floaty = normals[1].x
    local ymax::Floaty = normals[1].y
    local zmax::Floaty = normals[1].z
    for i = 2:length(normals)
        xmin = min(xmin,normals[i].x)
        ymin = min(ymin,normals[i].y)
        zmin = min(zmin,normals[i].z)
        xmax = max(xmax,normals[i].x)
        ymax = max(ymax,normals[i].y)
        zmax = max(zmax,normals[i].z)
    end
    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xmax: ", xmax," ymax: ", ymax, " zmax: ", zmax)
    
    xmin -= retro
    ymin -= retro
    zmin -= retro
    
    xwidth = int((xmax-xmin)/dx) 
    ywidth = int((ymax-ymin)/dy) 
    zwidth = int((zmax-zmin)/dz)
    
    extraradius = false
    if extraradius
        xwidth += int(2.01*sarcoradius/dx)
        ywidth += int(2.01*sarcoradius/dy)
        zwidth += int(2.01*sarcoradius/dz)
        xmin -= sarcoradius
        ymin -= sarcoradius
        zmin -= sarcoradius
    end
    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xw: ", xwidth," yw: ", ywidth, " zw: ", zwidth)
    
    local chi = Array(Array{Floaty,1},xwidth,ywidth,zwidth)
    fill(chi,zeros(Floaty,3))

    
    #chidict = Dict{Array{Int,1},Array{Floaty,1}}()
    chilist = Set{Array{Int,1}}()
    #sizehint(chidict,600000)
    #sizehint(chilist,1000000)
    println("chi size: ", size(chi))
    
    #some helper variables so you don't have to reinitialize after each loop
    local x::Array{Floaty,1}
    local x0 = zeros(Floaty,3)
    local x1 = zeros(Floaty,3)
    local dxp = zeros(Floaty,3)
    local xn::Floaty
    local yn::Floaty
    local zn::Floaty
    local bigN
    local idx::Array{Int,1}
    local cx,cy,cz, ii,jj,kk
    const zero3 = zeros(Floaty,3)
    numdone = 0
    
    progress = 0
    #Bresengam's line drawing algorithm in 3D
    #http://www.cb.uu.se/~cris/blog/index.php/archives/400
    for n in normals
        xn = n.nx*sarcoradius
        yn = n.ny*sarcoradius
        zn = n.nz*sarcoradius
        cx = n.x-xmin-retro*n.nx
        cy = n.y-ymin-retro*n.ny
        cz = n.z-zmin-retro*n.nz
        x0[1] = get_index(cx,dx)
        x0[2] = get_index(cy,dy)
        x0[3] = get_index(cz,dz)
        x1[1] = get_index(cx + xn,dx)
        x1[2] = get_index(cy + yn,dy)
        x1[3] = get_index(cz + zn,dz)
        
        dxp = x1-x0;
        x = copy(x0);

        bigN = max(abs(dxp));
        dxp /= bigN
        ii = 0
        jj = 0
        kk = 0
        xn = n.nx
        yn = n.ny
        zn = n.nz

        for blah = 1:int(bigN)
            x += dxp
            #  for kk = -1:1;#for ii = -1:1;for jj = -1:1;
                        idx = [int(x[1]) + ii,int(x[2]) + jj,int(x[3])+kk]
                        if inbounds(idx,xwidth,ywidth,zwidth)
                            chi[idx[1],idx[2],idx[3]] += [xn,yn,zn]
                            #chidict[idx] = get(chidict,idx,Floaty[])
                            add!(chilist,idx)
                       end
            # end;# end; end                   
        end
         if progress % 100000 == 0; println(progress); end; progress += 1
    end

    println("consolidating")
    @remove begin
    local v1::Array{Floaty,1} = copy(zero3)
    local v2::Array{Floaty,1} = copy(zero3)
    local v3::Array{Floaty,1} = copy(zero3)
    if false
        smoothing = false
        if smoothing 
            println("smoothing")
            for vec in keys(chidict)
                v1 = chi[vec[1]+1,vec[2],vec[3]] + chi[vec[1]-1,vec[2],vec[3]]
                v2 = chi[vec[1],vec[2]+1,vec[3]] + chi[vec[1],vec[2]-1,vec[3]]
                v3 = chi[vec[1],vec[2],vec[3]+1] + chi[vec[1],vec[2],vec[3]-1]
                chidict[vec] = 0.142857 *(chi[vec[1],vec[2],vec[3]] + v1 + v2 + v3)
            end
        else
            for vec in chilist#keys(chidict)
                chidict[vec] = chi[vec[1],vec[2],vec[3]]
            end
        end
        chidict[SIZE_INDEX] = [myfloat(s) for s in size(chi)][1:3]
    end
    end
    (chi,chilist)
end


function anglekernel(u::Array{Floaty,1},v::Array{Floaty,1})
    if( sum(abs(u)) > 1e-9)
        return acos(dot(u,v)/norm(u))
    else
        return 0.0 
    end
end

function getangles(chilist::Set{Array{Int,1}},chi::Array{Array{Floaty,1},3},prop_direction::Array{Floaty,1})
    (xl,yl,zl) = size(chi)
    local angles = zeros(Floaty,xl,yl,zl)
    const ehat = prop_direction./norm(prop_direction);
    progress = 0
    for (i,j,k) in chilist
            angles[i,j,k] = anglekernel(chi[i,j,k],ehat)
            if progress %1000000 == 0; println(progress); end
            progress += 1

    end
    angles
end

function getangles(chi::Dict{Array{Int,1},Array{Floaty,1}},prop_direction::Array{Floaty,1})
    const (xl,yl,zl) = int(chi[SIZE_INDEX])
    local angles = zeros(Floaty,xl,yl,zl)
    println(size(angles))
    const ehat = prop_direction./norm(prop_direction);
    progress = 0
    local chidict = copy(chi);
    delete!(chidict,SIZE_INDEX)
    for v in keys(chidict)
        angles[v[1],v[2],v[3]] = anglekernel(chidict[v],ehat)
        if progress % 1000000 == 0; println(progress); end
        progress += 1
    end
    angles
end

function getangles(chi,prop_direction::Array{Floaty,1})
    (xl,yl,zl) = size(chi)
    local angles = zeros(Floaty,xl,yl,zl)
    println(size(angles))
    const ehat = prop_direction./norm(prop_direction);
    progress = 0
    for i = 1:xl; for j = 1:yl; for k = 1:zl;
        angles[i,j,k] = anglekernel(chi[i,j,k],ehat)
        if progress % 1000000 == 0; println(progress); end
        progress += 1
    end; end; end;
    angles
end



#Chi-tensor product for myosin
function polarization(z::Array{Floaty,1},E::Array{Floaty,1},a::Floaty,b::Floaty,c::Floaty)
    const dzE = z[1]*E[1] + z[2]*E[2] + z[3]*E[3] #dot(z,E)
    (a*dzE*dzE+b)*z + c*dzE*E
end

#from “Studies of χ(2)/χ(3) tensors in submicron-scaled bio-tissues by polarization harmonics optical microscopy"
#d15 = 1.15, d31 = 1.0   , d33 = 0.1
function calcSHG2(chilist::Set{Array{Int,1}},chi::Array{Array{Floaty,1},3},e_field_in::Array{ MyCplx,3}, d15in::Real,d31in::Real,d33in::Real, dzin::Real, p::OpticParams,upsample::Int,ehat::Array{Floaty,1})

    #p_par = (d31*sin(theta)^2 + d33*cos(theta)^2)*abs(chi_direction)*E^2 ?abs or not
    #p_perp = 2*d15 sin(theta)*cos(theta)E^2*(abs(chi_direction)cross with z)
    local e_fieldsq = copy(e_field_in)
    const d15 = myfloat(d15in);
    const d33 = myfloat(d33in);
    const d31 = myfloat(d31in);
    const A = d33-myfloat(3.0)*d31
    const B = d31
    const C = myfloat(2.0)*d31
    const dz = myfloat(dzin);
    #const k2w::Floaty = -myfloat(2.0*pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    const uhat = myfloat([0,0,1])
    
    ehat = ehat/norm(ehat)
    
    #const chidict = chi #copy(chi)
    (xl,yl,zl) = size(chi)
    
    const upsamplex = 1
    xl *= upsamplex
    yl *= upsamplex
    zl *= upsample
    #local angles = zeros(Floaty,xl,yl,zl)
    #delete!(chidict,SIZE_INDEX)
     
    local chinetX = zeros(MyCplx,xl,yl,zl)
    local chinetY = zeros(MyCplx,xl,yl,zl)
    local chinetZ = zeros(MyCplx,xl,yl,zl)

    println("calculating chi-effects")

    local n::Floaty
    local pol = myfloat([0,0,0])
   
    println(ehat)
    
    local chitemp::Array{Floaty,1}
    for (i,j,k) in chilist #((i,j,k),chitemp) in chidict
        chitemp = chi[i,j,k]
        n = mynorm(chitemp)
        if (n > 1e-9)
            #chitemp /= n
            i = (i-1)*upsamplex + 1
            j = (j-1)*upsamplex + 1
            k = (k-1)*upsample + 1
       
            #calculate the induced polarization by a unit E-field at each point.
            pol = polarization(chitemp,ehat,A,B,C)
            for ii=0:(upsamplex-1); for jj = 0:(upsamplex-1); for kk = 0:(upsample-1)
                chinetX[i+ii,j+jj,k+kk] += pol[1]
                chinetY[i+ii,j+jj,k+kk] += pol[2]
                chinetZ[i+ii,j+jj,k+kk] += pol[3]
            end; end; end
            
        end
    end
    
    #erase chi and chidict to get memory. They are gone forever!
    chi = []# Dict{Array{Int,1},Array{Floaty,1}}()
    chilist = Set()
    gc()
    
    println("convolving")
    
   # myproduct!(e_field,e_field)
    #e_field *= dx*dx*dz
    
    #calculate abs(chi:EE)^2 = abs2(chiX*|E|^2) + abs2(chiY*|E|^2)+ abs2(chiZ*|E|^2)
    local shg::Array{Floaty,3} = myabs2!(myconv3d(chinetX,e_fieldsq))
    myabs2sum!(shg,myconv3d(chinetY,e_fieldsq))
    myabs2sum!(shg,myconv3d(chinetZ,e_fieldsq))

   shg
end

#Calculate just the Chi:\hat{E}^2 at each point
#d15 = 1.15, d31 = 1.0   , d33 = 0.1
function calcChi2(chilist::Set{Array{Int,1}},chi::Array{Array{Floaty,1},3},e_field_in::Array{ MyCplx,3}, d15in::Real,d31in::Real,d33in::Real, dzin::Real, p::OpticParams,upsample::Int,ehat::Array{Floaty,1};use_jitter=false)

    const d15 = myfloat(d15in);
    const d33 = myfloat(d33in);
    const d31 = myfloat(d31in);
    const A = d33-myfloat(3.0)*d31
    const B = d31
    const C = myfloat(2.0)*d31
    const dz = myfloat(dzin);
    #const k2w::Floaty = -myfloat(2.0*pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    const uhat = myfloat([0,0,1])
    
    ehat = ehat/norm(ehat)
    
    #const chidict = chi #copy(chi)
    (xl,yl,zl) = size(chi)
    
    const upsamplex = 1
    xl *= upsamplex
    yl *= upsamplex
    zl *= upsample
    #local angles = zeros(Floaty,xl,yl,zl)
    #delete!(chidict,SIZE_INDEX)
     
    local chinetX = zeros(MyCplx,xl,yl,zl)
    local chinetY = zeros(MyCplx,xl,yl,zl)
    local chinetZ = zeros(MyCplx,xl,yl,zl)

    println("calculating chi-effects")

    local n::Floaty
    local pol = myfloat([0,0,0])
   
    println(ehat)
    
    local chitemp::Array{Floaty,1}
    local ehattemp::Array{Floaty,1}
    const JITTER_SCALE = 0.1
    
    for i = 1:xl; for j = 1:yl; for k = 1:zl #(i,j,k) in chilist #((i,j,k),chitemp) in chidict
        chitemp = chi[i,j,k]
        ehattemp = copy(ehat)
        if use_jitter
            ehattemp[1] += JITTER_SCALE*(0.5 - rand())
            ehattemp[2] += JITTER_SCALE*(0.5 - rand())
            ehattemp[3] += JITTER_SCALE*(0.5 - rand())
        end
        n = mynorm(chitemp)
        if (n > 1e-9)
            #chitemp /= n
            i = (i-1)*upsamplex + 1
            j = (j-1)*upsamplex + 1
            k = (k-1)*upsample + 1
       
            #calculate the induced polarization by a unit E-field at each point.
            pol = polarization(chitemp,ehattemp,A,B,C)
            for ii=0:(upsamplex-1); for jj = 0:(upsamplex-1); for kk = 0:(upsample-1)
                chinetX[i+ii,j+jj,k+kk] += pol[1]
                chinetY[i+ii,j+jj,k+kk] += pol[2]
                chinetZ[i+ii,j+jj,k+kk] += pol[3]
            end; end; end
            
        end
    end; end; end
    
    (chinetX,chinetY,chinetZ)
end



#Do the final convolution
function convChiE2(chiX,chiY,chiZ,efieldsq;ds=1,dsz=1,wrap=false)
    local myconv::Function = wrap ? myconv3d_2 : myconv3d
    local shg::Array{Floaty,3} = myabs2!(downsample(myconv(chiX,efieldsq),ds,dsz))
    #chiX = []
    myabs2sum!(shg,downsample(myconv(chiY,efieldsq),ds,dsz))
    #chiY = []
    myabs2sum!(shg,downsample(myconv(chiZ,efieldsq),ds,dsz))

   shg
end
 
#if you don't specify the e-field direction, it defaults to [1,0,0]
function calcSHG2(chilist::Set{Array{Int,1}},chi::Array{Array{Floaty,1},3},e_field_in::Array{ MyCplx,3}, d15in::Real,d31in::Real,d33in::Real, dzin::Real, p::OpticParams,upsample::Int)
    calcSHG2(chilist,chi,e_field_in,d15in,d33in,dzin,p,upsample,myfloat([1,0,0]))
end

#write the chidict to a file
function dumpvector(filename, chidict::Dict{Array{Int,1},Array{Floaty,1}})
    outfile = open(filename,"w")
    for (vec,chi) in collect(chidict)
        write(outfile,"$(vec[1]),$(vec[2]),$(vec[3]),$(float64(chi[1])),$(float64(chi[2])),$(float64(chi[3]))\n")
    end
    close(outfile)
end



function randcomplex()
    myfloat(rand()) + 1im*myfloat(rand())
end




function doeverything(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real)
    dz = myfloat(dzin)
    println("Loading Normals")
    normals = loadnormalsAll(filename,dz,DR);
    println("Calculating Susc.")
    (chi,chilist) = makeSusceptibilityAll(normals,sarcowidth,dx,dy,dz);
    println("Calculating Angles")
    angles = getangles(chilist,chi,myfloat([1,1,0]))
end

function doeverything2(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real)
    dz = myfloat(dzin)
    println("Loading Normals")
    normals = loadnormalsAll(filename,dz,DR);
    println("Calculating Susc.")
    (chi,chidict) = makeSusceptibilityAll(normals,sarcowidth,dx,dy,dz);
    println("Calculating E-field")
    e_field = get_e_field(dx,dz,3,5,p)
    println("Calculating SHG")
    (shg,angles) = calcSHG2(chidict,e_field,1.15,1.0   ,0.1,dz,p)
    (shg,angles)
end

#shg = doeverything2(homepath*datapath*"NORMALS.csv",1.25,0.25,0.25,0.25,1);
function doeverything2(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real,upsample::Int)
    local p::OpticParams = OpticParams(0.85,1.3)
    dz = myfloat(dzin)
    println("Loading Normals")
    normals = loadnormalsAll(filename,myfloat(0.25));
    println("Calculating Susc.")
    (chi,chilist) = makeSusceptibilityAll(normals,sarcowidth,dx,dy,dz/myfloat(upsample));
    println("non-zero points $(length(chilist))")
   # dumpvector("chifield.csv",chidict)
    println("Calculating E-field")
    e_field = get_e_field(dx,dz/myfloat(upsample),5.0,8.0,p)
    println("Calculating SHG")
    calcSHG2(chilist,chi,e_field,1.15 ,1.0 ,0.1 ,dz,p,upsample,myfloat([1,1,0]))
end


#This is the one I usually use
function doeverything3(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real,upsample::Int)
    local p::OpticParams = OpticParams(0.85,1.3)
    dz = myfloat(dzin)
    println("Loading Normals")
    local normals = loadnormalsAll(filename,myfloat(0.25));
    local bignormals = growNormals(normals,sarcowidth)
    println("Calculating Susc.")
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,dx,dy,dz/myfloat(upsample));
    println("non-zero points $(length(chilist))")
    println("Calculating E-field")
    e_field = get_e_field(dx,dz/myfloat(upsample),15.0,15.0,p)
    println("Calculating SHG")
    calcSHG2(chilist,chi,e_field,1.15 ,1.0 ,0.1 ,dz,p,upsample,myfloat([0,1,0]))
end

function doeverything2(normals::Array{NormPoint,1},sarcowidth::Real,dx::Real,dy::Real,dzin::Real)
    dz = myfloat(dzin)
    (chi,chidict) = makeSusceptibility(normals,sarcowidth,dx,dy,dz);
    println("calculating angels")
    #angles = getangles(chidict,myfloat([0,0,1]))
    e_field = get_e_field(dx,dz,5*p.w0,4*p.zr,p)
    (shg,angles) = calcSHG2(chidict,e_field,1.15,1.0   ,0.1,dz,p)
    (shg,angles)
end

makechi = quote 
    dr = DR/8.0
    dz = 0.25/4.0
    sarcowidth = 5.0*DR
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",myfloat(0.25),DR)
    println("Extrapolating Normals")
    dojitter = false
    bignormals = growNormals(normals,sarcowidth,dojitter)
    normals = []
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,dr,dr,dz);
end

#to save type eval(savechi)
savechi = quote
    outf1 = open("chi.dat","w")
    outf2 = open("chilist.dat","w")
    write(outf1,chi)
    write(outf2,chilist)
    close(outf1)
    close(outf2)
end

loadchi = quote
    inf1 = open("chi.dat","r")
    inf2 = open("chilist.dat","r")
    chi = read(inf1)
    chilist = read(inf2)
    close(inf1)
    close(inf2)
end

makeshg = quote
    p = OpticParams(0.85,1.4);
    downsamp = int(DR/dr)
    downsampz = int(0.25/dz)
    efield_angle=myfloat([0.0,1,0.01]);
    dk = 0.0
    e_field = get_e_field(dr,dz,4.0,4.5,p,dk);
    (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle);use_jitter=dojitter);
    #chi = []; chilist = []; gc()
    gc()
    shgout = convChiE2(chiX,chiY,chiZ,e_field;ds=downsamp,dsz=downsampz,wrap=true);
end

function PhaseIteration(dz=0.25,dr=0.132,efield_angle = myfloat([0,1,0]);jitter=false, downsamp = 1, downsampz = 1, sarcowidth = 0.6, nn= false)
    phases = [-pi/2.0:(pi/4.0):pi];
    local NA = 1.4
    println("NA = $(NA)")
    local p = OpticParams(0.85,NA);
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",myfloat(0.25),DR)
    println("Extrapolating Normals")
    bignormals = nn ? [] : growNormals(normals,sarcowidth,jitter)
    (chi,chilist) = nn ?  makeSusceptibilityNN(normals,dr,dr,dz,sarcowidth) : makeSusceptibilitySpartan(bignormals,dr,dr,dz);
    normals = []
    strsarcowidth = @sprintf("%.2f", sarcowidth)
    for dk in phases
        local e_field = get_e_field(dr,dz,4.0,6.0,p,dk);
        (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle);use_jitter=jitter);
        local shg = convChiE2(chiX,chiY,chiZ,e_field;ds=downsamp,dsz=downsampz);
        strphase = @sprintf("%.4f", dk)
        outfile = open(resultpath*"shg-NA="*@sprintf("%.2f", NA)*"-sarcowidth=$(strsarcowidth)-phase=$(strphase)-sz=$(size(shg))_phaseIter.raw","w")
        println("phase mismatch was $dk")
        write(outfile,shg)
        close(outfile)
    end
end

function SarcoIteration(downsamp = 1, downsampz = 1, efield_angle = myfloat([0,1,0]);jitter=false,  nn= false)
    const DZ=0.25
    const DR =0.132
    dz = DZ / float(downsampz)
    dr = DR / float(downsamp)
    sarcos = [0.4:0.2:1.6];
    local NA = 1.4

    infofile = open(resultpath*"info.txt","w")

    local datastring = "NA = $(NA)\nefield_angle: $(efield_angle)\njitter=$jitter\,dr=$(dr/downsamp); dz=$(dz/downsampz)\n"
    println(datastring)
    write(infofile,datastring)
    local p = OpticParams(0.85,NA);
    const k2w::Floaty = myfloat(2.0 *pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",DZ,DR)
    println("Extrapolating Normals")

    local i = 0
    local dk = phasematch(p.kw,k2w)
    local e_field = get_e_field(dr,dz,4.0,6.0,p,dk);
    strphase = @sprintf("%.4f", dk)

    for sarcowidth in sarcos
        bignormals = nn ? [] : growNormals(normals,sarcowidth,jitter)
        (chi,chilist) = nn ?  makeSusceptibilityNN(normals,dr,dr,dz,sarcowidth) : makeSusceptibilitySpartan(bignormals,dr,dr,dz);
        (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle);use_jitter=jitter);
        local shg = convChiE2(chiX,chiY,chiZ,e_field;ds=downsamp,dsz=downsampz);
        strsarcowidth = @sprintf("%.2f", sarcowidth)
        local info = resultpath*"shg-NA="*@sprintf("%.2f", NA)*"-sarcowidth=$(strsarcowidth)-phase=$(strphase)-sz=$(size(shg))_sarcoIter.raw"
        outfile = open(info,"w")
        info = "\n *************** \n"*info
        info *= "\nphase mismatch was $dk \nsarco width was $strsarcowidth\nsize: $(size(shg))\n"
        println(info)
        write(outfile,shg)
        close(outfile)
        datastring *= info
        i+=1
        write(infofile,info)
    end

    close(infofile)
end

function PappaBear( efield_angle = myfloat([0,1,0]))
    dz = 0.1
    dr = 0.05
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",myfloat(0.25),DR)
    println("Extrapolating Normals")
    bignormals = growNormals(normals,5.5)
    normals = []
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,dr,dr,dz);
    bignormals = []; gc()
    e_field = get_e_field(0.25,0.25,10.0,10.0,p);
    (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle));
    chi = []; chilist = []; gc()
    convChiE2(chiX,chiY,chiZ,e_field);
end

function PontiBear(dz=0.1,dr=0.1,efield_angle = myfloat([0,1,0]);jitter=false, downsamp = 1, downsampz = 1, sarcowidth = 4.0)
    local p = OpticParams(0.85,1.3)
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",myfloat(0.25),DR)
    println("Extrapolating Normals")
    bignormals = growNormals(normals,sarcowidth,jitter)
    normals = []
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,dr,dr,dz);
    bignormals = []; gc()
    e_field = get_e_field(dr,dz,4.0,6.0,p);
    (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle);use_jitter=jitter);
    chi = []; chilist = []; gc()
    convChiE2(chiX,chiY,chiZ,e_field;ds=downsamp,dsz=downsampz,wrap=false);
end

function MammaBear(efield_angle = myfloat([0,1,0]))
    dz = 0.25
    dr = 0.1
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",myfloat(0.25))
    println("Extrapolating Normals")
    bignormals = growNormals(normals,5.5)
    normals = []
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,dr,dr,dz);
    bignormals = []; gc()
    e_field = get_e_field(dr,dz,10.0,10.0,p);
    (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle));
    chi = []; chilist = []; gc()
    convChiE2(chiX,chiY,chiZ,e_field);
end

function BabyBear(efield_angle = myfloat([1,0,0]))
    dz = 0.25
    dr = 0.25
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",myfloat(0.25))
    println("Extrapolating Normals")
    bignormals = growNormals(normals,5.5)
    normals = []
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,dr,dr,dz);
    bignormals = []; gc()
    e_field = get_e_field(dr,dz,5.0,5.0,p);
    (chiX,chiY,chiZ) = calcChi2(chilist,chi,e_field,1.15 ,1.0 ,0.25 ,dz,p,1,myfloat(efield_angle));
    chi = []; chilist = []; gc()
    convChiE2(chiX,chiY,chiZ,e_field);
end

function downsample{T}(x::Array{T,3},n::Int)
    if n == 1
        return x
    end
    
    const (lx,ly,lz) = size(x)
    const lx2 = int(floor(lx/n))
    const ly2 = int(floor(ly/n))
    const lz2 = int(floor(lz/n))
    println(size(x))
    println("$lx2 $ly2 $lz2")
    const NF = float(n*n*n)
    local out = zeros(T,lx2,ly2,lz2)
    for i = 0:(lx2-1); for j = 0:(ly2-1); for k = 0:(lz2-1)#1:lz2
        for di = 1:n;for dj = 1:n;for dk = 1:n
            out[i+1,j+1,k+1] += x[i*n+di,j*n + dj,k*n + dk]/NF
        end; end; end;
    end; end; end;
    out
end

function downsample{T}(x::Array{T,3},n::Int,nz::Int)
    if n == 1 && nz == 1
        return x
    end
    
    const (lx,ly,lz) = size(x)
    const lx2 = int(floor(lx/n))
    const ly2 = int(floor(ly/n))
    const lz2 = int(floor(lz/nz))
    println(size(x))
    println("$lx2 $ly2 $lz2")
    const NF = float(n*n*nz)
    local out = zeros(T,lx2,ly2,lz2)
    for i = 0:(lx2-1); for j = 0:(ly2-1); for k = 0:(lz2-1)#1:lz2
        for di = 1:n;for dj = 1:n;for dk = 1:nz
            out[i+1,j+1,k+1] += x[i*n+di,j*n + dj,k*nz + dk]/NF
        end; end; end;
    end; end; end;
    out
end

function downsampleABS{T}(x::Array{T,3},n::Int,nz::Int)
    if n == 1 && nz == 1
	return x
    end

    const (lx,ly,lz) = size(x)
    const lx2 = int(floor(lx/n))
    const ly2 = int(floor(ly/n))
    const lz2 = int(floor(lz/nz))
    println(size(x))
    println("$lx2 $ly2 $lz2")
    const NF = float(n*n*nz)
    local out = zeros(T,lx2,ly2,lz2)
    for i = 0:(lx2-1); for j = 0:(ly2-1); for k = 0:(lz2-1)#1:lz2
	for dk = 1:nz;for dj = 1:n;for di = 1:n
	    out[i+1,j+1,k+1] += sqrt(abs2(x[i*n+di,j*n + dj,k*nz + dk]))/NF
	end; end; end;
    end; end; end;
    out
end
