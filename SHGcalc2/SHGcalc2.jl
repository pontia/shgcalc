#cd("/lfs1/users/nhodas/Dropbox/Verniers\ Images/interpolated/")

homepath = "/lfs1/users/nhodas/"
#datapath = "Dropbox/Verniers\ Images/GroundTruth/analysis_24/"
datapath = "Dropbox/Verniers\ Images/analysis_24/"

#establish the numeric types : 32-bit floats
myfloat(x::Number) = float32(x)
myfloat(x::Array{Int64,1}) = float32(x)
myfloat{T<:Number}(x::Array{T,1}) = float32(x)
const Floaty = Float32
const MyCplx = Complex64


#in place element-wise multiplication
function myproduct!(a2::Array{MyCplx,3},b2::Array{ MyCplx,3})
    const (x,y,z) = size(a2)
    for i = 1:x
        for j = 1:y
            for k = 1:z
                a2[i,j,k] *= b2[i,j,k]
            end
        end
    end
    a2
end

#I think myassign! is faster than
#a[1:x,1:y,1:z] = anew
function myassign!(a::Array{MyCplx,3},x::Int,y::Int,z::Int,anew::Array{MyCplx,3})
    for i = 1:x; for j = 1:y; for k = 1:z;
        a[i,j,k] = anew[i,j,k]
    end; end; end;
    a
end

function myassign2!(a::Array{MyCplx,3},x::Int,y::Int,z::Int,anew::Array{MyCplx,3})
    (x2,y2,z2) = size(a)
    x2 = div(x2 - x,2)
    y2 = div(y2-y,2)
    z2 = div(z2-z,2)
    for i = 1:x; for j = 1:y; for k = 1:z;
        a[x2+i,y2+j,z2+k] = anew[i,j,k]
    end; end; end;
    a
end

function myconv3d(a::Array{MyCplx,3},b::Array{MyCplx,3})
    const (ax,ay,az) = size(a);
    const (bx,by,bz) = size(b);
    const mx = int((ax + bx - 1))
    const my = int((ay + by - 1))
    const mz = int((az + bz - 1))
    #const mx = int(max(ax,bx))
    #const my = int(max(ay,by))
    #const mz = int(max(az,bz))
    local a2::Array{MyCplx,3} = zeros(MyCplx,mx,my,mz)
    local b2::Array{MyCplx,3} = zeros(MyCplx,mx,my,mz)
    
    #a2[1:ax,1:ay,1:az] = a;
    #b2[1:bx,1:by,1:bz] = b;
    myassign!(a2,ax,ay,az,a) #my assign 
    myassign!(b2,bx,by,bz,b)
   # const ffa = plan_fft!(a2);
   # const ffb = plan_fft!(b2);
    ifft!(myproduct!(fft!(a2),fft!(b2)));
end


#page 51 of thesis. wavelength in microns
function refractiveindex(lambda::Real)
    sqrt(2.116 + 22.45*lambda^2/(lambda^2-53.9343))
end

function phasematch(kw::Floaty,k2w::Floaty)
    -0.2*(-2.0f0 * kw + k2w)
end

#Optical Parameteres for calculations
#in try block because it can only be declared once
try
immutable OpticParams
    lambda0::Floaty # 0.850
    n1::Floaty  # 1.4
    # n2::Floaty
    lambda::Floaty # = lambda0/n1
    NA::Floaty # = 1.40
    kw::Floaty # = 2.0f0  *pi/lambda
    # kw2::Floaty # =2.0f0  *pi/(lambda/(2*n2))
    # dk::Floaty# 2*kw-kw
    w0::Floaty # = lambda/(pi*NA)
    w0sq::Floaty # = w0*w0
    zr::Floaty # = pi*w0sq/lambda
    b::Floaty # = kw w0^2
end
end

#helper function to construct optical params from 2 inputs
function OpticParams(lambda0in::Real,NAin::Real)
    local lambda0 = myfloat(lambda0in);
    local NA = myfloat(NAin);
    local n1 = myfloat(refractiveindex(lambda0));
    local lambda = lambda0/n1
    local kw = myfloat(2.0*pi)/lambda
    local w0 = lambda/myfloat(pi*NA)
    local w0sq = w0*w0
    local zr = myfloat(pi*w0sq/lambda)
    local b =  myfloat(kw*w0sq) #MyCplx(0.0f0  ,-myfloat(kw*w0sq/2.0f0))
    OpticParams(lambda0,n1,lambda,NA,kw,w0,w0sq,zr,b)
 end

 
const p = OpticParams(0.85,1.3)

#see page 81 of NOH Thesis
function efield(x::Floaty,y::Floaty,z::Floaty, p::OpticParams)
    #println("$x,$y,$z,$(p.cbhalf),$(p.w0sq)")
    const c0:: MyCplx =  1.0 + 2.0im*z/p.b 
    const k2w::Floaty = myfloat(2.0f0*pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    exp(-(x^2+y^2)/(p.w0sq*c0)+0.5f0*im*z*phasematch(kw,k2w))/c0  #phase dk is here. factor of 0.5 because it will be squared later
end

function efieldsq(x::Floaty,y::Floaty,z::Floaty, p::OpticParams)
    #println("$x,$y,$z,$(p.cbhalf),$(p.w0sq)")
    const c0:: MyCplx =  1.0 + 2.0im*z/p.b 
    const k2w::Floaty = myfloat(2.0f0*pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    exp(-2.0*(x^2+y^2)/(p.w0sq*c0)+1im*z*phasematch(p.kw,k2w))/(c0*c0)  #phase dk is here
end

#calculate 3D laser field
function get_e_field(dr::Real,dz::Real,radius::Real,depth::Real,p::OpticParams)
    numr = int(2*radius/dr);
    numz = int(2*depth/dz);
    e_field = zeros( MyCplx,numr,numr,numz);
    dr = myfloat(dr)
    dz = myfloat(dz)
    radius = myfloat(radius)
    depth = myfloat(depth)
    const k2w::Floaty = myfloat(2.0f0*pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    println("Phase match: $(phasematch(p.kw,k2w))")
    for xi = 1:numr
        for yi= 1:numr
            for zi = 1:numz
                e_field[xi,yi,zi] = efieldsq(dr*myfloat(xi) - radius,dr*myfloat(yi) - radius,dz*myfloat(zi)-depth,p)*dr*dr*dz
            end
        end
    end
    e_field
end

#data type for recording normals from CSV file
try
immutable NormPoint
    x::Floaty
    y::Floaty
    z::Floaty
    nx::Floaty
    ny::Floaty
    nz::Floaty
end
end

#constructors for NormPoint
NormPoint(x::Number,y::Number,z::Number,nx::Number,ny::Number,nz::Number) = NormPoint(myfloat(x),myfloat(y),myfloat(z),myfloat(nx/sqrt(nx*nx + ny*ny+nz*nz)),myfloat(ny/sqrt(nx*nx + ny*ny+nz*nz)),myfloat(nz/sqrt(nx*nx + ny*ny+nz*nz)));
NormPoint(n::NormPoint,shift::Floaty) = NormPoint(n.x + shift*n.nx,n.y + shift*n.ny,n.z + shift*n.nz,n.nx,n.ny,n.nz);

#dz = 0.25
import Base.isnan
isnan(n::NormPoint) = isnan(n.x) || isnan(n.y) || isnan(n.z) || isnan(n.nx)|| isnan(n.ny) || isnan(n.nz)

#Read normals from csv file
function loadnormalsAll(filename::ASCIIString,dz::Floaty)
    (indata,header) = readdlm(filename,'\t',Floaty,has_header=true)
    (len,wid) = size(indata)
    goodlen = 0
    local n::NormPoint
    for i = 2:len
        if ~isnan(NormPoint(indata[i,4],indata[i,5],dz*indata[i,1],indata[i,6],indata[i,7],indata[i,8]))  
            goodlen += 1
       end
    end
    
    normals = Array(NormPoint,goodlen)
    c = 1
    for i = 2:len
        n = NormPoint(indata[i,4],indata[i,5],dz*indata[i,1],indata[i,6],indata[i,7],indata[i,8])
        if  !isnan(n)
            normals[c] = n
            c += 1
        end
    end
    normals
end

#extrapolate out the normals with resolution DX for  distance width
function growNormals(normals::Array{NormPoint,1},widthin::Real)
    const len = length(normals)
    const width = myfloat(widthin)
    const DX::Floaty = 0.01
    const NUM_STEPS = int(width/ DX) - 1
   
    const newlen = NUM_STEPS*len
    local newnormals = Array(NormPoint,newlen)
    println("NUM_STEPS $NUM_STEPS")
    println("totallen $newlen")
    local i = 0
    for n in normals
        newnormals[i+=1] = n
        for j = 1:(NUM_STEPS-1)
            newnormals[i+=1] = NormPoint(n,j*DX)
        end
    end
    newnormals
end

#helper method to turn position into discrete index
function get_index(x::Real,dx::Real)
    round(x/dx) + myfloat(1.0)  
end


function fill(obj::Array{Array{Floaty,1},3},f::Array{Floaty,1})
    const (x,y,z) = size(obj)
    for i = 1:x
        for j = 1:y
            for k = 1:z
                obj[i,j,k] = f
            end
        end
    end
end

function fill(obj::Array{Array{Floaty,1},2},f::Array{Floaty,1})
    const (x,y) = size(obj)
    for i = 1:x
        for j = 1:y
                obj[i,j] = f
        end
    end
end

function inbounds(x::Array{Int,1},xw::Int,yw::Int,zw::Int)
    (0 < x[1]) && (0 < x[2]) && ( 0 < x[3]) && ( x[1] <= xw) && (x[2] <= yw) && (x[3] <= zw)
end

#Calculate the susceptibility
#return the susceptibility tensor chi[x,y,z] = [chix,chiy,chiz] at (x,y,z)
#also return a dictionary of all non-zero points chidict[[x,y,z]] = [chix,chiy,chiz]
function makeSusceptibilitySpartan(normals::Array{NormPoint,1},dx::Real,dy::Real,dz::Real)

    const retro::Floaty = 2.3 #direction to back up to attempt to get different surfaces to meet at hypothetical M-line
    
    #These could all be vectorized, but I think avoiding temporary arrays makes it a bit faster
    local xmin::Floaty = normals[1].x
    local ymin::Floaty = normals[1].y
    local zmin::Floaty = normals[1].z
    local xmax::Floaty = normals[1].x
    local ymax::Floaty = normals[1].y
    local zmax::Floaty = normals[1].z
    for i = 2:length(normals)
        xmin = min(xmin,normals[i].x)
        ymin = min(ymin,normals[i].y)
        zmin = min(zmin,normals[i].z)
        xmax = max(xmax,normals[i].x)
        ymax = max(ymax,normals[i].y)
        zmax = max(zmax,normals[i].z)
    end
    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xmax: ", xmax," ymax: ", ymax, " zmax: ", zmax)
    
    xmin -= retro
    ymin -= retro
    zmin -= retro
    
    xwidth = int((xmax-xmin)/dx) 
    ywidth = int((ymax-ymin)/dy) 
    zwidth = int((zmax-zmin)/dz)
    

    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xw: ", xwidth," yw: ", ywidth, " zw: ", zwidth)
    
    local chi = Array(Array{Floaty,1},xwidth,ywidth,zwidth)
    fill(chi,zeros(Floaty,3))

    
    chilist = Set{Array{Int,1}}()
    println("chi size: ", size(chi))
    
    #some helper variables so you don't have to garbage collect after each loop
    local x::Array{Floaty,1}
    local x0 = zeros(Floaty,3)
    local x1 = zeros(Floaty,3)
    local dxp = zeros(Floaty,3)
    local xn::Floaty
    local yn::Floaty
    local zn::Floaty
    local bigN
    local idx::Array{Int,1}
    local cx,cy,cz, ii,jj,kk
    const zero3 = zeros(Floaty,3)
    numdone = 0
    
    progress = 0
    #Bresengam's line drawing algorithm in 3D
    #http://www.cb.uu.se/~cris/blog/index.php/archives/400
    for n in normals
        cx = n.x-xmin-retro*n.nx
        cy = n.y-ymin-retro*n.ny
        cz = n.z-zmin-retro*n.nz
    
        idx = [int(get_index(cx,dx)),int(get_index(cy,dy)),int(get_index(cz,dz))]
        if inbounds(idx,xwidth,ywidth,zwidth)
            chi[idx[1],idx[2],idx[3]] += [n.nx,n.ny,n.nz]
            add!(chilist,idx)
        end
         if progress % 1000000 == 0; println(progress/length(normals)); end; progress += 1
    end

    println("consolidating")
    
    #not doing this part for now
    blah = quote
        local v1::Array{Floaty,1} = copy(zero3)
        local v2::Array{Floaty,1} = copy(zero3)
        local v3::Array{Floaty,1} = copy(zero3)
        if false
            smoothing = false
            if smoothing 
                println("smoothing")
                for vec in keys(chidict)
                    v1 = chi[vec[1]+1,vec[2],vec[3]] + chi[vec[1]-1,vec[2],vec[3]]
                    v2 = chi[vec[1],vec[2]+1,vec[3]] + chi[vec[1],vec[2]-1,vec[3]]
                    v3 = chi[vec[1],vec[2],vec[3]+1] + chi[vec[1],vec[2],vec[3]-1]
                    chidict[vec] = 0.142857 *(chi[vec[1],vec[2],vec[3]] + v1 + v2 + v3)
                end
            else
                for vec in chilist#keys(chidict)
                    chidict[vec] = chi[vec[1],vec[2],vec[3]]
                end
            end
            chidict[SIZE_INDEX] = [myfloat(s) for s in size(chi)][1:3]
        end
    end
    
    (chi,chilist)
end


#you can get the size of the chi tensor from chidict[SIZE_INDEX] = [lx,ly,lz]
const SIZE_INDEX = [-1,-34,-234]
#sarcoradius = 1.0f0  
#dx=dy=0.132,dz=0.25
function makeSusceptibilityAll(normals::Array{NormPoint,1},sr::Real,dx::Real,dy::Real,dz::Real)

    const sarcoradius = sr
    const retro::Floaty = 2.3 #direction to back up to attempt to get different surfaces to meet at hypothetical M-line
    
    #These could all be vectorized, but I think avoiding temporary arrays makes it a bit faster
    local xmin::Floaty = normals[1].x
    local ymin::Floaty = normals[1].y
    local zmin::Floaty = normals[1].z
    local xmax::Floaty = normals[1].x
    local ymax::Floaty = normals[1].y
    local zmax::Floaty = normals[1].z
    for i = 2:length(normals)
        xmin = min(xmin,normals[i].x)
        ymin = min(ymin,normals[i].y)
        zmin = min(zmin,normals[i].z)
        xmax = max(xmax,normals[i].x)
        ymax = max(ymax,normals[i].y)
        zmax = max(zmax,normals[i].z)
    end
    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xmax: ", xmax," ymax: ", ymax, " zmax: ", zmax)
    
    xmin -= retro
    ymin -= retro
    zmin -= retro
    
    xwidth = int((xmax-xmin)/dx) 
    ywidth = int((ymax-ymin)/dy) 
    zwidth = int((zmax-zmin)/dz)
    
    extraradius = false
    if extraradius
        xwidth += int(2.01*sarcoradius/dx)
        ywidth += int(2.01*sarcoradius/dy)
        zwidth += int(2.01*sarcoradius/dz)
        xmin -= sarcoradius
        ymin -= sarcoradius
        zmin -= sarcoradius
    end
    println("xmin: ", xmin," ymin: ", ymin, " zmin: ", zmin)
    println("xw: ", xwidth," yw: ", ywidth, " zw: ", zwidth)
    
    local chi = Array(Array{Floaty,1},xwidth,ywidth,zwidth)
    fill(chi,zeros(Floaty,3))

    
    #chidict = Dict{Array{Int,1},Array{Floaty,1}}()
    chilist = Set{Array{Int,1}}()
    #sizehint(chidict,600000)
    #sizehint(chilist,1000000)
    println("chi size: ", size(chi))
    
    #some helper variables so you don't have to reinitialize after each loop
    local x::Array{Floaty,1}
    local x0 = zeros(Floaty,3)
    local x1 = zeros(Floaty,3)
    local dxp = zeros(Floaty,3)
    local xn::Floaty
    local yn::Floaty
    local zn::Floaty
    local bigN
    local idx::Array{Int,1}
    local cx,cy,cz, ii,jj,kk
    const zero3 = zeros(Floaty,3)
    numdone = 0
    
    progress = 0
    #Bresengam's line drawing algorithm in 3D
    #http://www.cb.uu.se/~cris/blog/index.php/archives/400
    for n in normals
        xn = n.nx*sarcoradius
        yn = n.ny*sarcoradius
        zn = n.nz*sarcoradius
        cx = n.x-xmin-retro*n.nx
        cy = n.y-ymin-retro*n.ny
        cz = n.z-zmin-retro*n.nz
        x0[1] = get_index(cx,dx)
        x0[2] = get_index(cy,dy)
        x0[3] = get_index(cz,dz)
        x1[1] = get_index(cx + xn,dx)
        x1[2] = get_index(cy + yn,dy)
        x1[3] = get_index(cz + zn,dz)
        
        dxp = x1-x0;
        x = copy(x0);

        bigN = max(abs(dxp));
        dxp /= bigN
        ii = 0
        jj = 0
        kk = 0
        xn = n.nx
        yn = n.ny
        zn = n.nz

        for blah = 1:int(bigN)
            x += dxp
            #  for kk = -1:1;#for ii = -1:1;for jj = -1:1;
                        idx = [int(x[1]) + ii,int(x[2]) + jj,int(x[3])+kk]
                        if inbounds(idx,xwidth,ywidth,zwidth)
                            chi[idx[1],idx[2],idx[3]] += [xn,yn,zn]
                            #chidict[idx] = get(chidict,idx,Floaty[])
                            add!(chilist,idx)
                       end
            # end;# end; end                   
        end
         if progress % 100000 == 0; println(progress); end; progress += 1
    end

    println("consolidating")
    blah = quote
    local v1::Array{Floaty,1} = copy(zero3)
    local v2::Array{Floaty,1} = copy(zero3)
    local v3::Array{Floaty,1} = copy(zero3)
    if false
        smoothing = false
        if smoothing 
            println("smoothing")
            for vec in keys(chidict)
                v1 = chi[vec[1]+1,vec[2],vec[3]] + chi[vec[1]-1,vec[2],vec[3]]
                v2 = chi[vec[1],vec[2]+1,vec[3]] + chi[vec[1],vec[2]-1,vec[3]]
                v3 = chi[vec[1],vec[2],vec[3]+1] + chi[vec[1],vec[2],vec[3]-1]
                chidict[vec] = 0.142857 *(chi[vec[1],vec[2],vec[3]] + v1 + v2 + v3)
            end
        else
            for vec in chilist#keys(chidict)
                chidict[vec] = chi[vec[1],vec[2],vec[3]]
            end
        end
        chidict[SIZE_INDEX] = [myfloat(s) for s in size(chi)][1:3]
    end
    end
    (chi,chilist)
end


function anglekernel(u::Array{Floaty,1},v::Array{Floaty,1})
    if( sum(abs(u)) > 1e-9)
        return acos(dot(u,v)/norm(u))
    else
        return 0.0f0
    end
end

function getangles(chilist::Set{Array{Int,1}},chi::Array{Array{Floaty,1},3},prop_direction::Array{Floaty,1})
    (xl,yl,zl) = size(chi)
    local angles = zeros(Floaty,xl,yl,zl)
    const ehat = prop_direction./norm(prop_direction);
    progress = 0
    for (i,j,k) in chilist
            angles[i,j,k] = anglekernel(chi[i,j,k],ehat)
            if progress %1000000 == 0; println(progress); end
            progress += 1

    end
    angles
end

function getangles(chi::Dict{Array{Int,1},Array{Floaty,1}},prop_direction::Array{Floaty,1})
    const (xl,yl,zl) = int(chi[SIZE_INDEX])
    local angles = zeros(Floaty,xl,yl,zl)
    println(size(angles))
    const ehat = prop_direction./norm(prop_direction);
    progress = 0
    local chidict = copy(chi);
    delete!(chidict,SIZE_INDEX)
    for v in keys(chidict)
        angles[v[1],v[2],v[3]] = anglekernel(chidict[v],ehat)
        if progress % 1000000 == 0; println(progress); end
        progress += 1
    end
    angles
end




function myabs2!(z::Array{MyCplx{Floaty},3})
    const (sx,sy,sz) = size(z)
    for i = 1:sx; for j = 1:sy; for k = 1:sz
        z[i,j,k] = abs2(z[i,j,k])
    end;end;end
   #ccall((:myabs2,"SHGcalc.so"),Void,(Ptr{Complex64},Int),z,length(z));
    z
end

function myabs2sum!(out::Array{Floaty,3},z::Array{MyCplx{Floaty},3})
    @assert size(out) == size(z)
    ccall((:myabs2sum,"SHGcalc.so"),Void,(Ptr{Floaty},Ptr{Complex64},Int),out,z,length(z));
    out
end

function mynorm(z::Array{Floaty,1})
    local s = abs2(z[1])
    for i = 2:length(z)
        s += abs2(z[i])
    end
    sqrt(s)
end

function polarization(z::Array{Floaty,1},E::Array{Floaty,1},a::Floaty,b::Floaty,c::Floaty)
    const dzE = z[1]*E[1] + z[2]*E[2] + z[3]*E[3] #dot(z,E)
    (a*dzE*dzE+b)*z + c*dzE*E
end

#from “Studies of χ(2)/χ(3) tensors in submicron-scaled bio-tissues by polarization harmonics optical microscopy"
#d15 = 1.15, d31 = 1.0f0  , d33 = 0.1
function calcSHG2(chilist::Set{Array{Int,1}},chi::Array{Array{Floaty,1},3},e_field_in::Array{ MyCplx,3}, d15in::Real,d31in::Real,d33in::Real, dzin::Real, p::OpticParams,upsample::Int,ehat::Array{Floaty,1})

    #p_par = (d31*sin(theta)^2 + d33*cos(theta)^2)*abs(chi_direction)*E^2 ?abs or not
    #p_perp = 2*d15 sin(theta)*cos(theta)E^2*(abs(chi_direction)cross with z)
    local e_fieldsq = copy(e_field_in)
    const d15 = myfloat(d15in);
    const d33 = myfloat(d33in);
    const d31 = myfloat(d31in);
    const A = d33-myfloat(3.0)*d31
    const B = d31
    const C = myfloat(2.0)*d31
    const dz = myfloat(dzin);
    #const k2w::Floaty = -myfloat(2.0*pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    const uhat = myfloat([0,0,1])
    
    ehat = ehat/norm(ehat)
    
    #const chidict = chi #copy(chi)
    (xl,yl,zl) = size(chi)
    
    const upsamplex = 1
    xl *= upsamplex
    yl *= upsamplex
    zl *= upsample
    #local angles = zeros(Floaty,xl,yl,zl)
    #delete!(chidict,SIZE_INDEX)
     
    local chinetX = zeros(MyCplx,xl,yl,zl)
    local chinetY = zeros(MyCplx,xl,yl,zl)
    local chinetZ = zeros(MyCplx,xl,yl,zl)

    println("calculating chi-effects")

    local n::Floaty
    local pol = myfloat([0,0,0])
   
    println(ehat)
    
    local chitemp::Array{Floaty,1}
    for (i,j,k) in chilist #((i,j,k),chitemp) in chidict
        chitemp = chi[i,j,k]
        n = mynorm(chitemp)
        if (n > 1e-9)
            #chitemp /= n
            i = (i-1)*upsamplex + 1
            j = (j-1)*upsamplex + 1
            k = (k-1)*upsample + 1
       
            #calculate the induced polarization by a unit E-field at each point.
            pol = polarization(chitemp,ehat,A,B,C)
            for ii=0:(upsamplex-1); for jj = 0:(upsamplex-1); for kk = 0:(upsample-1)
                chinetX[i+ii,j+jj,k+kk] += pol[1]
                chinetY[i+ii,j+jj,k+kk] += pol[2]
                chinetZ[i+ii,j+jj,k+kk] += pol[3]
            end; end; end
            
        end
    end
    
    #erase chi and chidict to get memory. They are gone forever!
    chi = []# Dict{Array{Int,1},Array{Floaty,1}}()
    chilist = Set()
    gc()
    
    println("convolving")
    
   # myproduct!(e_field,e_field)
    #e_field *= dx*dx*dz
    
    #calculate abs(chi:EE)^2 = abs2(chiX*|E|^2) + abs2(chiY*|E|^2)+ abs2(chiZ*|E|^2)
    local shg::Array{Floaty,3} = myabs2!(myconv3dc(chinetX,e_fieldsq))
    myabs2sum!(shg,myconv3dc(chinetY,e_fieldsq))
    myabs2sum!(shg,myconv3dc(chinetZ,e_fieldsq))

   shg
end

#if you don't specify the e-field direction, it defaults to [1,0,0]
function calcSHG2(chilist::Set{Array{Int,1}},chi::Array{Array{Floaty,1},3},e_field_in::Array{ MyCplx,3}, d15in::Real,d31in::Real,d33in::Real, dzin::Real, p::OpticParams,upsample::Int)
    calcSHG2(chilist,chi,e_field_in,d15in,d33in,dzin,p,upsample,myfloat([1,0,0]))
end

#write the chidict to a file
function dumpvector(filename, chidict::Dict{Array{Int,1},Array{Floaty,1}})
    outfile = open(filename,"w")
    for (vec,chi) in collect(chidict)
        write(outfile,"$(vec[1]),$(vec[2]),$(vec[3]),$(float64(chi[1])),$(float64(chi[2])),$(float64(chi[3]))\n")
    end
    close(outfile)
end



function test1d()
    test = float32(rand(Float32,1024)) + 1im*float32(rand(Float32,1024))
    t1 = copy(test)
    ccall((:testfft,"SHGcalc.so"),Void,(Ptr{Complex64},Int),t1,length(t1))
    t2 = fft(copy)
    t1 - t2
end

function randcomplex()
    float32(rand()) + 1im*float32(rand())
end

function test3d()
    const dim = 100;
    test = Array(Complex64,dim,dim,dim)
    for i = 1:dim; for j = 1:dim; for k = 1:dim;
        test[i,j,k] = randcomplex()
    end;end;end
    
    t1 = copy(test);#reshape(copy(test),length(test));
    ccall((:fft3d,"SHGcalc.so"),Void,(Ptr{Complex64},Int,Int,Int),t1,dim,dim,dim)
    #t1 = reshape(t1,dim,dim,dim)
    t2 = fft(test)
    t1 - t2
end

#3d convolve using julia FFTW library
function myconv3d(a::Array{Complex128,3},b::Array{Complex128,3})
    const (ax,ay,az) = size(a);
    const (bx,by,bz) = size(b);
    const mx = int((ax + bx - 1))
    const my = int((ay + by - 1))
    const mz = int((az + bz - 1))
    #const mx = int(max(ax,bx))
    #const my = int(max(ay,by))
    #const mz = int(max(az,bz))
    local a2::Array{Complex128,3} = zeros(Complex128,mx,my,mz)
    local b2::Array{Complex128,3} = zeros(Complex128,mx,my,mz)
    
    a2[1:ax,1:ay,1:az] = a;
    b2[1:bx,1:by,1:bz] = b;
    #myassign!(a2,ax,ay,az,a)
    #myassign!(b2,bx,by,bz,b)
   # const ffa = plan_fft!(a2);
   # const ffb = plan_fft!(b2);
    ifft!(fft!(a2).*fft!(b2));
end


function testconv3d()
    const dim = 100;
    test = Array(Complex64,dim,dim,dim)
    test2 = Array(Complex64,dim,dim,dim)
    for i = 1:dim; for j = 1:dim; for k = 1:dim;
        test[i,j,k] = 0#randcomplex()
        test2[i,j,k] = 0#randcomplex()
    end;end;end
    test[30,30,30] = 1.0
    test2[30,30,30] = 1.0
    t1 = copy(test);#reshape(copy(test),length(test));
    ccall((:conv3d,"SHGcalc.so"),Void,(Ptr{Complex64},Ptr{Complex64},Int,Int,Int),t1,test2,dim,dim,dim)
    #t1 = reshape(t1,dim,dim,dim)
    
    t2 = myconv3d(test,test2)
    t1 - t2
end

#3d convolve using my OPENMP FFTW function written in c. not sure why it differs from julia, but it's multithreaded.
function myconv3dc(a::Array{Complex64,3},b::Array{Complex64,3})
    const (ax,ay,az) = size(a);
    const (bx,by,bz) = size(b);
    const mx = int((ax + bx - 1))
    const my = int((ay + by - 1))
    const mz = int((az + bz - 1))
    const mx = int(max(ax,bx))
    const my = int(max(ay,by))
    const mz = int(max(az,bz))
    local a2::Array{Complex64,3}
    local b2::Array{Complex64,3} = zeros(Complex64,mx,my,mz)
    if (ax == mx) && (ay == my ) && (az == mz)
        a2 = copy(a)
    else
        a2 = zeros(Complex64,mx,my,mz)
        #a2[1:ax,1:ay,1:az] = a;
        myassign2!(a2,ax,ay,az,a)
    end
    myassign2!(b2,bx,by,bz,b)
    ccall((:conv3d,"SHGcalc.so"),Void,(Ptr{Complex64},Ptr{Complex64},Int,Int,Int),a2,b2,mx,my,mz)
    fftshift(a2)
end

const BUFFER_WIDTH = 0
function slabSusceptibility(thickness::Real,width::Real,dx::Real,dz::Real)
    const lx::Int = get_index(width,dx)
    const lz::Int = get_index(thickness,dz) + 2*BUFFER_WIDTH
    
    local chi = zeros(MyCplx,lx,lx,lz) 
    
    const CHI0 = myfloat([1.0,0.0,0.0])

    for i = 1:lx; for j = 1:lx; for k = (BUFFER_WIDTH+1):(lz-BUFFER_WIDTH)
            chi[i,j,k] = 1.0 + 0im
    end;end;end

    chi
end

#for comparing theoretical SHG in finite slab to calculated SHG test case
function testSHG(lambda::Real, NA::Real,dx::Real,dz::Real)
    const width = 5.0f0
    const p::OpticParams = OpticParams(lambda,NA)
    const maxthickness = 10.0
    const zdepth = maxthickness
    const e_fieldsq = get_e_field(dx,dz,width/2.0,zdepth/2,p)
    const thicknessrange = myfloat([dz:max(dz,0.1):maxthickness])
    const k2w::Floaty = myfloat(2.0f0*pi/(0.5*p.lambda0/refractiveindex(0.5*p.lambda0)))
    const mismatch = Array(Floaty,length(thicknessrange))
    local intensity = Array(Floaty,length(thicknessrange))
    local theory = Array(Floaty,length(thicknessrange))
    i = 0
    println(size(intensity))
    println(phasematch(p.kw,k2w))
    local shgg::Array{Floaty,3}
    local lastthickness::Floaty
    for thickness = thicknessrange
        chi = slabSusceptibility(thickness,width,dx,dz)
        local shg = myabs2!(myconv3dc(chi,e_fieldsq))#calcSHG2(chidict,e_field,1.15f0,1.0f0,0.1f0,dz,p,1)
        shg *= (dx*dx*dz)
        xi = int(size(shg,1)/2)
        zi = max(int(size(shg,3)/2.0)+1,1)
        intensity[i+=1] = shg[xi,xi,zi]
        mismatch[i] = phasematch(p.kw,k2w)*thickness
        theory[i] = abs2(theorySHG(p,thickness,0.01f0))
        shgg = shg
        lastthickness = thickness
    end
    
    blah = quote
    trange = myfloat([-maxthickness:dz:(lastthickness+maxthickness)])
    theoryg = zeros(Floaty,length(trange))
    shgout = zeros(Floaty,length(trange))
    shgsize = size(shgg)
    i = 0
    for z = trange
        theoryg[i+=1] = abs2(theorySHG2(p,0.0f0-z,lastthickness-z,0.001f0))
        shgout[i] = shgg[int(shgsize[1]/2),int(shgsize[2]/2),i*int(0.1/dz)]
    end
    end
    
    (mismatch,intensity,theory,shgg)#,theoryg,shgout)
end

function mykern(p::OpticParams,z::Floaty,dk::Floaty)
    exp(im*dk*z)/(1.0+1.0im*z/p.b)
end

function theorySHG(p::OpticParams,thickness::Floaty,dz::Floaty)
    const range = (-thickness/2.0f0):dz:(thickness/2.0f0)
    local out::MyCplx = myfloat(0.0)
    const k2w::Floaty = myfloat(2.0f0*pi/(0.5f0*p.lambda0/refractiveindex(0.5f0*p.lambda0)))
    for z in range
        out += mykern(p,z,myfloat(phasematch(p.kw,k2w)))
    end
    out *= dz
end

function theorySHG2(p::OpticParams,z0::Floaty,zf::Floaty,dz::Floaty)
    const range = z0:dz:zf
    local out::MyCplx = myfloat(0.0)
    const k2w::Floaty = myfloat(2.0f0*pi/(0.5f0*p.lambda0/refractiveindex(0.5f0*p.lambda0)))
    i = 0
    for z in range
        out += mykern(p,z,myfloat(phasematch(p.kw,k2w)))
    end
    out *= dz
end

function doeverything(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real)
    dz = myfloat(dzin)
    println("Loading Normals")
    normals = loadnormalsAll(filename,dz);
    println("Calculating Susc.")
    (chi,chilist) = makeSusceptibilityAll(normals,sarcowidth,dx,dy,dz);
    println("Calculating Angles")
    angles = getangles(chilist,chi,myfloat([1,1,0]))
end

function doeverything2(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real)
    dz = myfloat(dzin)
    println("Loading Normals")
    normals = loadnormalsAll(filename,dz);
    println("Calculating Susc.")
    (chi,chidict) = makeSusceptibilityAll(normals,sarcowidth,dx,dy,dz);
    println("Calculating E-field")
    e_field = get_e_field(dx,dz,3,5,p)
    println("Calculating SHG")
    (shg,angles) = calcSHG2(chidict,e_field,1.15,1.0f0  ,0.1,dz,p)
    (shg,angles)
end

#shg = doeverything2(homepath*datapath*"NORMALS.csv",1.25,0.25,0.25,0.25,1);
function doeverything2(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real,upsample::Int)
    local p::OpticParams = OpticParams(0.85,1.3)
    dz = myfloat(dzin)
    println("Loading Normals")
    normals = loadnormalsAll(filename,myfloat(0.25));
    println("Calculating Susc.")
    (chi,chilist) = makeSusceptibilityAll(normals,sarcowidth,dx,dy,dz/myfloat(upsample));
    println("non-zero points $(length(chilist))")
   # dumpvector("chifield.csv",chidict)
    println("Calculating E-field")
    e_field = get_e_field(dx,dz/myfloat(upsample),5.0,8.0,p)
    println("Calculating SHG")
    calcSHG2(chilist,chi,e_field,1.15f0,1.0f0,0.1f0,dz,p,upsample,myfloat([1,1,0]))
end

makechi = quote 
    normals = loadnormalsAll(homepath*datapath*"NORMALS.csv",myfloat(0.25))
    bignormals = growNormals(normals,6.0)
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,0.1,0.1,0.25);
end
  
makeshg = quote
    e_field = get_e_field(0.1,0.25,10.0,10.5,p)
    shg = calcSHG2(chilist,chi,e_field,1.15f0,1.0f0,0.1f0,0.25,p,1,myfloat([1,1,0]))
end
    
#This is the one I usually use
function doeverything3(filename,sarcowidth::Real,dx::Real,dy::Real,dzin::Real,upsample::Int)
    local p::OpticParams = OpticParams(0.85,1.3)
    dz = myfloat(dzin)
    println("Loading Normals")
    local normals = loadnormalsAll(filename,myfloat(0.25));
    local bignormals = growNormals(normals,sarcowidth)
    println("Calculating Susc.")
    (chi,chilist) = makeSusceptibilitySpartan(bignormals,dx,dy,dz/myfloat(upsample));
    println("non-zero points $(length(chilist))")
    println("Calculating E-field")
    e_field = get_e_field(dx,dz/myfloat(upsample),15.0,15.0,p)
    println("Calculating SHG")
    calcSHG2(chilist,chi,e_field,1.15f0,1.0f0,0.1f0,dz,p,upsample,myfloat([0,1,0]))
end

function doeverything2(normals::Array{NormPoint,1},sarcowidth::Real,dx::Real,dy::Real,dzin::Real)
    dz = myfloat(dzin)
    (chi,chidict) = makeSusceptibility(normals,sarcowidth,dx,dy,dz);
    println("calculating angels")
    #angles = getangles(chidict,myfloat([0,0,1]))
    e_field = get_e_field(dx,dz,5*p.w0,4*p.zr,p)
    (shg,angles) = calcSHG2(chidict,e_field,1.15,1.0f0  ,0.1,dz,p)
    (shg,angles)
end


