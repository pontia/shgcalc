//
//  SHGcalc.c
//  
//
//  Created by Nathan Oken Hodas on 6/26/13.
//
// Compile shared library for Julia to call:
// icc SHGcalc.c -lm -lfftw3f -lfftw3f_omp -lfftw3 -L\usr\lib64 -O3 -fopenmp -parallel -par-threshold=1 -par-report=2 -vec-report=3 -o SHGcalc.so -shared -fPIC -DNOALIAS -DALIGN -vec-threshold=3


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <fftw3.h>


#ifdef _OPENMP
#include <omp.h>
#endif

//abreviate FFTW exentsions
#define CONCAT(prefix,name) prefix ## name
#define FFTW(x) CONCAT(fftwf_,x)

//startup routing for Fourier Transforms
int done = 0;
void initthread(){
#ifdef _OPENMP
    printf("init threads\n");
    FFTW(init_threads)();
    FFTW(plan_with_nthreads)(20);
    omp_set_num_threads(20);
#endif
    done = 1;
}



/* helper functions */
void myprod(float complex* a, float complex* b, const int len){
    int i;
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(i=0;i<len;i++){
        a[i] *= b[i];
    }
}

void mydiv(float complex* a, const int len, const float denom){
    int i;
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(i=0;i<len;i++){
        a[i] /= denom;
    }
}

void myprodd(double complex* a, double complex* b, const int len){
    int i;
    for(i=0;i<len;i++){
        a[i] *= b[i];
    }
}

void mydivd(double complex* a, const int len, const double denom){
    int i;
#pragma vector
    for(i=0;i<len;i++){
        a[i] /= denom;
    }
}

void myabs2(float complex* a, const long len){
    long i;
    for( i =0; i < len; i++ ){
        float temp = cabsf(a[i]);
        a[i] = temp*temp;
    }
}

void myabs2sum(float* out, float complex* in, const long len){
    long i;
#pragma parallel
#pragma vector
    for( i = 0; i < len; i++){
        float temp = cabsf(in[i]);
        out[i] += temp*temp;
    }
}



void fft3d(float complex* a, const int x, const int y, const int z){
    if(!done){
        initthread();
        
    }
    FFTW(plan) p;
    p = FFTW(plan_dft_3d)(x,y,z,a,a,FFTW_FORWARD,FFTW_ESTIMATE);
    FFTW(execute)(p);
    FFTW(destroy_plan)(p);
    #ifdef _OPENMP
    FFTW(cleanup_threads)();
#endif
}


void conv3d(float complex* a, float complex* b, const int x, const int y, const int z){
    if(!done){
        initthread();
    }
    const int len = x*y*z;
    FFTW(plan) p1,p2,p3;

        //    p1 = FFTW(plan_dft_3d)(x,y,z,a,a,FFTW_FORWARD,FFTW_ESTIMATE);
         //   p2 = FFTW(plan_dft_3d)(x,y,z,b,b,FFTW_FORWARD,FFTW_ESTIMATE);
    p1 = FFTW(plan_dft_3d)(z,y,x,a,a,FFTW_FORWARD,FFTW_ESTIMATE);
    p2 = FFTW(plan_dft_3d)(z,y,x,b,b,FFTW_FORWARD,FFTW_ESTIMATE);

    
#ifdef _OPENMP
#pragma omp parallel sections
    {
#pragma omp section
        {
            FFTW(execute)(p1);
        }
#pragma omp section
        {
            FFTW(execute)(p2);
        }
    }
#else
            FFTW(execute)(p1);
            FFTW(execute)(p2);
#endif
            FFTW(destroy_plan)(p1);
            FFTW(destroy_plan)(p2);

    myprod(a,b,len);
    mydiv(a,len,(float) (len));
    
    //p3 = FFTW(plan_dft_3d)(x,y,z,a,a,FFTW_BACKWARD,FFTW_ESTIMATE);
    p3 = FFTW(plan_dft_3d)(z,y,x,a,a,FFTW_BACKWARD,FFTW_ESTIMATE);
   
    FFTW(execute)(p3);
    FFTW(destroy_plan)(p3);
#ifdef _OPENMP
    FFTW(cleanup_threads)();
#endif
}

void conv3dd(double complex* a, double complex* b, const int x, const int y, const int z){
    if(!done){
        initthread();
    }
    const int len = x*y*z;
    FFTW(plan) p1,p2,p3;
    
    p1 = FFTW(plan_dft_3d)(x,y,z,a,a,FFTW_FORWARD,FFTW_ESTIMATE);
    p2 = FFTW(plan_dft_3d)(x,y,z,b,b,FFTW_FORWARD,FFTW_ESTIMATE);
    FFTW(execute)(p1);
    FFTW(execute)(p2);
    FFTW(destroy_plan)(p1);
    FFTW(destroy_plan)(p2);
    
    myprodd(a,b,len);
    
    p3 = FFTW(plan_dft_3d)(x,y,z,a,a,FFTW_BACKWARD,FFTW_ESTIMATE);
    FFTW(execute)(p3);
    FFTW(destroy_plan)(p3);
#ifdef _OPENMP
    FFTW(cleanup_threads)();
#endif
    mydivd(a,len,(double) (len*len*len));
}

float getrand(){
    return (float) rand() /((float) RAND_MAX + 1);
}
float getrandd(){
    return (double) rand() /((double) RAND_MAX + 1);
}


//test methods
int main(){

    srand(time(NULL));
    const int N= 200;
    float complex t1,t2;
   // float complex test[N][N][N];
    fftw_complex* test = (fftw_complex *) fftw_malloc(N*N*N*sizeof(fftw_complex));
    fftw_complex* test2 = (fftw_complex *) fftw_malloc(N*N*N*sizeof(fftw_complex));
    int i,j,k;
    for(i= 0; i < N; i++){
        for(j=0; j < N; j++){
            for(k=0;k<N;k++){
                test[k+N*(j+N*i)] = getrandd() + getrandd()*_Complex_I;
                test2[k+N*(j+N*i)] = getrandd() + getrandd()*_Complex_I;
            }
        }
    }
    conv3dd(test,test2,N,N,N);
    printf("%f %f \n",__real__ test[100],__imag__ test[100]);
    fftw_free(test);
    fftw_free(test2);
    
    t1 = 1.0 + 1.0*_Complex_I;
    t2 = 1.0 - 1.0*_Complex_I;
    myprod(&t1,&t2,1);
    printf("%f %f \n",__real__ t1,__imag__ t1);
    return 1;
}