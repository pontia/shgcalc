


module EntropyPy

using  PyCall #Pkg.add("PyCall")
#to use my custom python dist: pyinitialize("/Users/nhodas/anaconda/lib/libpython2.7.dylib")
try
pyinitialize("/Users/nhodas/anaconda/lib/libpython2.7.dylib")
end

try
pyinitialize("/nfs/isd2/nhodas/anaconda/bin/python2.7")#libpython2.7.so")
end

@pyimport scipy.spatial as ss
@pyimport numpy.random as nr #use numpy to generate normal distributions
@pyimport numpy as np

function mysub!(v::Vector{Float64},x::Float64)
    for i = 1:length(v)
        v[i] -= x
    end
end

function mydiv!(v::Vector{Float64},x::Float64)
    for i = 1:length(v)
        v[i] /= x
    end
end

function mymax!(v::Array{Float64,1},x::Float64)
    for i = 1:length(v)
        if v[i] < x
            v[i] = x
        end
    end
end

function avgdigamma(points::Array{Float64},dvec::Array{Float64,1})
    const tree = makekdtree(points)#
    #dvec -= 1e-15
    mysub!(dvec,1e-15)
    local num_points::Array{Float64,1} = query_len_ball_points(tree,points,dvec)
    mymax!(num_points,1.0)
    digammareduce(num_points)
end

function digammareduce(counts::Array{Float64,1})
    ccall((:avedigamma,"avedigamma.so"),Cdouble,(Ptr{Float64},Cint),counts,length(counts))
end

function digammareduce2(counts::Array{Float64,1})
    const N = float(length(counts))
    local avg::Float64 = 0.0
    for i in 1:N
        avg += digamma(counts[i])
    end
    avg/N
end

function digammareduceP(counts::Array{Float64,1})
    r = pmap(digamma,counts)
    local avg = r[1]
    for i = 2:length(counts)
        avg += r[i]
    end
    avg/float(length(counts))
end

function jitter!(points::Array{Float64,2})
    const INTENS = 1e-12
    const (nx,ny) = size(points)
    for i = 1:nx
        for j = 1:ny
            points[i,j] += INTENS*rand()
        end
    end
    points
end

function jitter!(points::Array{Float64})
    const INTENS = 1e-12
    const nx = length(points)
    for i = 1:nx
        points[i] += INTENS*rand()
    end
    points
end

function entHelper(nn)
    const N = length(nn)
    local res = log(nn[1])
    for i = 2:N
        res += log(nn[i]+1e-12)
    end
    res / float(N)
end

#entropy from continous distribution
function entropyc(points::Array{Float64},k=3,base=2.0)
    @assert k <= size(points,1) - 1
    const d = float(length(points[1,:]))
    const N = size(points,1)
    #jitter!(points)
    const tree = makekdtree(points)
    const dvec = query(tree,points,k+1)[1]
    const c1 = digamma(N) - digamma(k) +  d*log(2.0)
    return (c1 + d*entHelper(dvec[1:N,k+1]))/log(base)
end

function conditionalentropy(points::Array{Float64,2},k=3)
    const ejoint = entropyc(points,k)
    const e2 = entropyc([shuffle(points[:,1]) points[:,2:size(points,2)]],k)
    #println(ejoint - e2)
    ejoint - e2
end

function conditionalentropy(x::Array{Float64},y::Array{Float64},k=3)
    const ejoint = entropyc([x y],k)
    const e2 = entropyc(y,k)
    #println(ejoint - e2)
    ejoint - e2
end

function entFromProbs(probs::Array{Float64},base=2)
    local ent = 0.0
    local p::Float64
    local lg::Function
    if base == 2
        lg = log2
    elseif base == e
        lg = log
    elseif base == 10
        lg = log10
    else
        lg = (x) -> log(base,x)
    end
    
    for i = 1:length(probs)
        p = probs[i]
        ent += p > 0.0 ? p*lg(p) : 0.0
    end
    -ent/log(base)
end


function entropyd(points::Array{Float64,1},resolution=1.0,base=2)
    local probs::Array{Float64} = hist(points,min(points):resolution:(max(points)+resolution))[2]
    mydiv!(probs,float(length(points)))
    const N::Float64 = size(points,1)
    entFromProbs(probs,base)/float(length(points)) #+log(base,N)  
end
    

function mi(x::Array{Float64},y::Array{Float64},k::Int=3,base=2)
    @assert size(x,1) == size(y,1)
    const intens = 1e-10
    const len = size(x,1)
    jitter!(x)
    jitter!(y)
    local points = [x y]
    const tree = makekdtree(points)
    const dvec = query(tree,points,k+1)[1][1:len,k+1]#[1:len]
    #println(size(dvec))
    const a::Float64 = avgdigamma(x,dvec)#[k+1,:])
    const b::Float64 = avgdigamma(y,dvec)#[k+1,:])
    const c::Float64 = digamma(k)
    const d::Float64 = digamma(len)
    (-a-b+c+d)/log(base)
end

function mi2(points::Array{Float64,2},k::Int=3)
    const intens = 1e-10
    const (len,width) = size(points)
    jitter!(points)
    const tree = makekdtree(points)
    const dvec = query(tree,points,k+1)[1]#[1:len,k+1]#[1:len]
    #println(size(dvec))
    const a::Float64 = avgdigamma(points[1:len,1],dvec[1:len,k+1])#[k+1,:])
    const b::Float64 = avgdigamma(points[1:len,2],dvec[1:len,k+1])#[k+1,:])
    const c::Float64 = digamma(k)
    const d::Float64 = digamma(len)
    -a-b+c+d
end

function logmean(x)
    const len = length(x)
    local s = log(x[1])
    for i = 2:len
        s += log(x[2])
    end
    s / float(len)
end


function kldiv(x::Array{Float64},y::Array{Float64},k=3,base=2)
    const n = size(x,1)
    const m = size(y,1)
    const d = length(x[1])
    @assert (k < n) && (k < m) && (d == length(y[0]))
    const c0 = log(m) - log(n-1)
    local treex = makekdtree(x)
    local treey = makekdtree(y)
    local nnx = query(treex,x,k+1)[1:n,k+1]
    local nny = query(treey,y,k+1)[1:n,k+1]
    (c0 + d*logmean(nny) - d*logmean(nnx))/log(base)
end

function klhelper(x::Array{Float64},k=3)
    local treex = makekdtree(x)
    local nnx = query(treex,x,k+1)[1:n,k+1]
    d*logmean(nny)
end

function kldivp(x::Array{Float64},y::Array{Float64},k=3,base=2)
    const n = size(x,1)
    const m = size(y,1)
    const d = length(x[1])
    @assert (k < n) && (k < m) && (d == length(y[0]))
    const c0 = log(m) - log(n-1)
    local refx = @spawn d*logmean(query(makekdtree(x),x,k+1)[1:n,k+1])
    local refy = @spawn d*logmean(query(makekdtree(y),y,k+1)[1:n,k+1])

    (c0 +  fetch(refx) - fetch(refy))/log(base)
end


function mysum(x::Array{Bool,1})
    local s = x[1] ? 1.0 : 0.0
    for i = 2:length(x)
        s += x[i] ? 1.0 : 0.0
    end
    s
end

#H(x|y), where y is binary (boolean)
function condEntb(x::Array{Float64},y::Array{Bool,1},k::Int=3,base=2)
    const len = length(y)
    const lenf = float(len)
    @assert size(x,1) == len
    local e1::Float64 = entropyc(x[y,:],k,base)
    local e2::Float64 = entropyc(x[!y,:],k,base)
    local numtrue = mysum(y)
    (numtrue*e1 + (lenf-numtrue)*e2)/lenf
    #e1+e2 - entFromProbs([numtrue/lenf,1.0 - numtrue/lenf],base)
end

#CMI(x,y|z), where y is binary (boolean)
function condMutEntb(x::Array{Float64},y::Array{Float64},z::Array{Bool,1},k::Int=3,base=2)
    const len = length(z)
    const lenf = float(len)
    @assert (size(x,1) == len) && (size(y,1) == len)
    local e1::Float64 = mi(x[z,:],y[z,:],k,base)
    local e2::Float64 = mi(x[!z,:],y[!z,:],k,base)
    local numtrue = mysum(z)
    (numtrue*e1 + (lenf-numtrue)*e2)/lenf
    #e1+e2 - entFromProbs([numtrue/lenf,1.0 - numtrue/lenf],base)
end

#H(X,Y|A), where X is binary, the rest are continuous
function cmib(x::Array{Bool,1},y::Array{Float64},z::Array{Float64},k::Int=3,base=2,nreps=20)
    local xshuf = shuffle(x)
    local e1::Float64 = condEntb(y,xshuf,k,base)
    local e2::Float64 = condEntb(y,x,k,base)
    local e3::Float64 = condMutEntb(y,z,xshuf,k,base)
    local e4::Float64 = condMutEntb(y,z,x,k,base)
    e1-e2-e3+e4
end

SHUFF = [1,2,3]
LEN_SHUFF = length(SHUFF)
function cmib_ns(x::Array{Bool,1},y::Array{Float64},z::Array{Float64},k::Int=3,base=2,nreps=20)

    global SHUFF
    global LEN_SHUFF
    
    if (SHUFF == [1,2,3]) || length(x) != LEN_SHUFF
        SHUFF = shuffle(x)
        LEN_SHUFF = length(SHUFF)
    end
    local xshuf = SHUFF

    local e1::Float64 = condEntb(y,xshuf,k,base)
    local e2::Float64 = condEntb(y,x,k,base)
    local e3::Float64 = condMutEntb(y,z,xshuf,k,base)
    local e4::Float64 = condMutEntb(y,z,x,k,base)
    e1-e2-e3+e4
end

function cmibp(x::Array{Bool,1},y::Array{Float64},z::Array{Float64},k::Int=3,base=2,nreps=20)
    function helper(x)
        local xshuf = shuffle(x)
        condEntb(y,xshuf,k,base) - condMutEntb(y,z,xshuf,k,base)
    end
    
    local ref1 = @spawn -condEntb(y,x,k,base)
    local ref2 = @spawn condMutEntb(y,z,x,k,base)

    local etemp = @parallel (+) for i = 1:nreps
                    helper(x)
                end



    etemp/float(nreps) + fetch(ref1) + fetch(ref2)
end

function cmib2(x::Array{Bool,1},y::Array{Float64},z::Array{Float64},k::Int=3,base=2,nreps=20)

    local ref1 = @spawn mi(y,z,k,base)
    local ref2 = @spawn entropyc(y,k,base)
    local ref3 = @spawn condEntb(y,x,k,base)
    local ref4 = @spawn condMutEntb(y,z,x,k,base)

    fetch(ref2) - fetch(ref3) - fetch(ref1) + fetch(ref4)
end

function cmi(x::Array{Float64},y::Array{Float64},z::Array{Float64},k::Int=3)
    const len = size(x,1)
     @assert (len == size(y,1)) && (len == size(z,1))
    const intens = 1e-10

    jitter!(x)
    jitter!(y)
    jitter!(z)
    local points = [x y z]
    const tree = makekdtree(points) # +intens*rand(size(points))
    local dvec::Array{Float64} = query(tree,points,k+1)[1:len,k+1]
    const a::Float64 = avgdigamma([x z],dvec)#[k+1,:])
    const b::Float64 = avgdigamma([y z],dvec)#[k+1,:])
    const c::Float64 = avgdigamma(z,dvec)#[k+1,:])
    const d::Float64 = digamma(k)
    (-a-b+c+d)/log(base)
end

function cmi2(points::Array{Float64,2},k::Int=3)
    const intens = 1e-10
    const len = size(points)[1]
    jitter!(points)
    const tree = makekdtree(points) # +intens*rand(size(points))
    local dvec::Array{Float64} = query(tree,points,k+1)[1:len,k+1]
    const a::Float64 = avgdigamma(points[1:len,[1,3]],dvec)#[k+1,:])
    const b::Float64 = avgdigamma(points[1:len,[2,3]],dvec)#[k+1,:])
    const c::Float64 = avgdigamma(points[1:len,3],dvec)#[k+1,:])
    const d::Float64 = digamma(k)
    return -a-b+c+d
end

function mymean(x::Array{Float64,1})
    local len::Float64 = length(x)
    local m = 0.0
    for i = 1:len
        m += x[i]
    end
    m/len
end

testmat = [[4.0,3.0,1.0] [3.0,4.0,1.0] [1.0,1.0,2.0]]
mydet = det(testmat) == 12.0 ? (x) -> det(x) : (x) -> -det(x)

function main()
    mi = mi2
    cmi = cmi2
    const Ntry = [10,25,50,100]#,200,400]#,2000] #Number of samples to use in estimate

    const nsamples = 100 #Number of times to est mutual information for CI
    const samplo = int(0.025*nsamples)+1 #confidence intervals
    const samphi = int(0.975*nsamples)+1

    println("test 1: gaussian/random")
    const d1 = [1.0,1.0,0.0]
    const d2 = [1.0,0.0,1.0]
    const d3 = [0.0,1.0,1.0]
    const mat = [d1 d2 d3]
    const tmat = mat'
    const diag = [[3.0,0.0,0.0] [0.0,1.0,0.0] [0.0,0.0,1.0]]
    const mean = [0.0,0.0,0.0]
    const cov = tmat * (diag*mat)#dot(tmat,dot(diag,mat))
    println("covariance matrix $cov")
    local trueent = -0.5*(3.0+log(8.*pi*pi*pi*mydet(cov))) 
    trueent += -0.5*(1.0+log(2.*pi*cov[3,3])) #z sub
    trueent += 0.5*(2.0+log(4.*pi*pi*det([[cov[1,1],cov[1,3]] [cov[3,1],cov[3,3]]] ))) #xz sub
    trueent += 0.5*(2.0+log(4.*pi*pi*det([[cov[2,2],cov[2,3]] [cov[3,2],cov[3,3]] ]))) #yz sub
    println( "true CMI $trueent")
    trueent = 0.5*(1.0+log(2.*pi*cov[1,1])) #x sub
    trueent += 0.5*(1.0+log(2.*pi*cov[2,2])) #y sub
    trueent += -0.5*(2.0+log(4.*pi*pi*det([[cov[1,1],cov[1,2]] [cov[2,1],cov[2,2]]] ))) #xz sub
    println( "true MI $trueent")
    local ent = Float64[]
    local err::Array{(Float64,Float64)} = []
    local tempent = zeros(nsamples)
    local tempmean::Float64
    for NN in Ntry
        local points = Array(Float64,NN,3)
        for j in 1:nsamples
          points = nr.multivariate_normal(mean,cov,NN)
          tempent[j] = mi(points[1:NN,[1,2]],3)
        end
        sort!(tempent)
        #println(tempent)
        local tempmean = mymean(tempent)
        #println(tempmean)
        push!(ent,tempmean)
        #println(tempmean)
        push!(err,(tempmean - tempent[samplo],tempent[samphi]-tempmean)) 
    end

    println("samples used $Ntry")
    println("estimated MI $ent")
    println("95% conf int. $err")
    
    ent = Float64[]
    err = Float64[]
    for NN in Ntry
        local points = Array(Float64,NN,3)
        for j in 1:nsamples
          points = nr.multivariate_normal(mean,cov,NN)
          tempent[j] = cmi(points,3)
        end
        sort!(tempent)
        local tempmean = mymean(tempent)
        push!(ent,tempmean)
        push!(err,(tempmean - tempent[samplo],tempent[samphi]-tempmean))
    end

    println("samples used $Ntry")
    println("estimated CMI $ent")
    println("95% conf int. (a,b) means (mean-a,mean+b)is interval $err")

    println( "If you permute the indices of x, e.g., MI(X:Y) = 0")

    ent = Float64[]
    err = Float64[]
    for NN in Ntry
        local points = Array(Float64,NN,3)
        for j in 1:nsamples
          points = nr.multivariate_normal(mean,cov,NN)
          points[1:NN,2] = shuffle!(points[1:NN,2])
          points[1:NN,1] = shuffle!(points[1:NN,1])  
          tempent[j] = mi(points[1:NN,1:2],3)
        end
        sort!(tempent)
        local tempmean = mymean(tempent)
        push!(ent,tempmean)
        push!(err,(tempmean - tempent[samplo],tempent[samphi]-tempmean)) 
    end
    println("samples used $Ntry")
    println("estimated MI $ent")
    println("95% conf int. $err")
    
    ent = Float64[]
    err = Float64[]
    for NN in Ntry
        local points = Array(Float64,NN,3)
        for j in 1:nsamples
          points = nr.multivariate_normal(mean,cov,NN)
          points[1:NN,2] = shuffle!(points[1:NN,2])
          points[1:NN,1] = shuffle!(points[1:NN,1])  
          points[1:NN,3] = shuffle!(points[1:NN,3])
          tempent[j] = cmi(points[1:NN,1:3],3)
        end
        sort!(tempent)
        tempmean = mymean(tempent)
        push!(ent,tempmean)
        push!(err,(tempmean - tempent[samplo],tempent[samphi]-tempmean)) 
    end
    println("samples used $Ntry")
    println("estimated CMI $ent")
    println("95% conf int. $err")
end

function toAofA{T}(x::Array{T,2})
    const (ny,nx) = size(x)
    local out::Array{Array{T,1}} = Array(Array{T,1},ny)
    for j = 1:ny
        out[j] = x[j,1:nx][1:nx]
    end
    out
end

#convert 1d matrix to array of arrays, each element is an array
function toAofA{T}(x::Array{T,1})
    const ny = length(x)
    local out::Array{Array{T,1}} = Array(Array{T,1},ny)
    for j = 1:ny
       # out[j] = [x[j]]
       out[j] = [x[j]]
    end
    out
end

function toAofA{T}(x::T)
    local out::Array{Array{T,1}} = Array(Array{T,1},1)
    out[1] = [x]
    out
end

function makekdtree{T}(points::Array{T,2})
    ss.cKDTree(points)
end

function makekdtree{T}(points::Array{T,1})
    ss.cKDTree(toAofA(points))
end

const PYSCRIPT = "[len(tree.query_ball_point(pointsin[i],dvecin[i],p=float('inf'))) for i in xrange(0,length)]"
NORM = Inf
function setnorm(n::Number)
    global NORM
    NORM = n
    global PYSCRIPT
    if n == Inf
        PYSCRIPT =  "[len(tree.query_ball_point(pointsin[i],dvecin[i],p=float('inf'))) for i in xrange(0,length)]"
    else
        PYSCRIPT =  "[len(tree.query_ball_point(pointsin[i],dvecin[i],p=float($n))) for i in xrange(0,length)]"
    end
end

function query(kd::PyObject,points::Array{Float64,2},k::Int)
    @assert k > 0
    kd[:query](points,k,p=NORM)
end

function query(kd::PyObject,points::Array{Float64,1},k::Int)
     @assert k > 0
    kd[:query](toAofA(points),k,p=NORM)
end

#query(kd::PyObject,points::Array{Float64,1},k::Int) = query(kd,points',k)

function query_ball_point(kd::PyObject,points::Array{Float64,2},r::Float64)
  PyVector(kd[:query_ball_point](points,r,p=NORM))
end

function query_ball_point(kd::PyObject,points::Array{Float64,1},r::Float64)
  kd[:query_ball_point](points,r,p=NORM)
end

query_ball_point(kd::PyObject,points::Float64,r::Float64) = query_ball_point(kd,[points],r)

function py2a(p::PyVector{Float64},len::Int)
    out = Array(Float64,len)
    for i = 1:len
        out[i] = p[i]
    end
    out
end


function query_len_ball_points(kd::PyObject,points::Array{Float64,2},dvec::Array{Float64,1})
    const nx::Int = length(dvec)
    const p = toAofA(points)
    pyeval(PYSCRIPT,Array{Float64},tree=kd,pointsin=p,dvecin=dvec,length=nx)
    #py2a(pyvec,nx)
end

function query_len_ball_points(kd::PyObject,points::Array{Float64,1},dvec::Array{Float64,1})
    const nx::Int = length(dvec)
    const p = toAofA(points)
    pyeval(PYSCRIPT,Array{Float64},tree=kd,pointsin=p,dvecin=dvec,length=nx)
    #py2a(pyvec,nx)
end


end
    
