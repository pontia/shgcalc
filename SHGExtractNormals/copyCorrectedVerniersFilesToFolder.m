function [status, cmdout] = copyCorrectedVerniersFilesToFolder(dataBackupDir, workDir)

if nargin == 0
    dataBackupDir = '/work/projects/bill/GroundTruth_Verniers/aaron/segmented_postprocessed_backup/';
    workDir = '/work/projects/bill/GroundTruth_Verniers/aaron/analysis_24/';
end

if ~exist(workDir, 'dir')
    mkdir(workDir);
end

copyfile(fullfile(dataBackupDir, 'region3_*_segmented.tif'), workDir);


% !cp /work/projects/bill/GroundTruth_Verniers/aaron/segmented_postprocessed_backup/region3_*_segmented.tif /work/projects/bill/GroundTruth_Verniers/aaron/analysis_24/

