function fNames = sortFilesNumerically( fNames )
% sortFilesNumerically sorts file names numerically by up to 25 indices 
%
% Often, numerical indices are saved with inconsitent digits (no padding),
% and this causes the standard alphanumeric sorting to mess up the real
% sorting. Example, 33 files with bad naming would be sorted as follows:
%
%    image1.tif
%    image10.tif
%    image11.tif
%    ...
%    image19.tif
%    image2.tif
%    image20.tif
%    image21.tif
%    ...
%    image29.tif
%    image3.tif
%    image30.tif
%    image31.tif
%    image32.tif
%    image33.tif
%    ...
%    image4.tif
%    image5.tif
%    ...
%    image9.tif
%
% SYNOPSIS   fNames = quSortFilesNumerically( fNames )
%
% INPUT      fNames   : cell array with file names (no directories)
%
% OUTPUT     fNames   : sorted cell array with file names
%

if ~iscell( fNames )
    return
end

% First sort alphanumerically
fNames = sort( fNames );

% Number of files
nFiles = numel( fNames );

% We use a regular expression to find all numerical indices in the file names
rexp = [ ...
    '^\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)', ...
    '\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)', ...
    '\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)', ...
    '\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)', ...
    '\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)\D*(\d*)\.\w{2,4}' ];

% We also use a 'sorting matrix' that we will fill incrementally
M = zeros( nFiles, 25 );

% Go over all files and find the indices
for i = 1 : numel( fNames )
    tokens = regexp( fNames{ i }, rexp, 'tokens' );
    if isempty( tokens )
        % The regexp failed, we just return the alphabetically sorted
        % file name list
        return
    end
    tokens  = tokens{ 1 };
    numTokens = cellfun( @str2double, tokens );
    numTokens( isnan( numTokens ) ) = 0;
    M( i, : ) = numTokens;
end
    
% Now sort and get the permutation
[ ~, sortedIndices ] = sortrows( M );
    
% Use the soerted indices to sort the files
fNames = fNames( sortedIndices );