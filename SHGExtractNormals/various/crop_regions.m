function crop_regions(conn)

channel = 1; % 0-based

% Region 1
%
% Middle, start at slice 20, end at slice 180
% 
% X start: 48 um from left
% Y start: 36 um from top
% Width: 34 um
% Height: 34 um

crop_current(conn, 48, 36, 34, 34, 20, 180, channel, 'region1_channel2_SHG');

% Region 2
%
% For Lower Middle, start at slice 20, end at slice 146
% 
% X start: 45 um from left
% Y start: 73 um from top
% Width: 26 um
% Height: 26 um

crop_current(conn, 45, 73, 26, 26, 20, 146, channel, 'region2_channel2_SHG');


% Region 3
% 
% Top right, start at 35, end at slice 182
% 
% X start: 98 um from left
% Y start: 43 um from top
% Width: 17 um
% Height: 17 um

crop_current(conn, 98, 43, 17, 17, 35, 182, channel, 'region3_channel2_SHG');

% Region 4
% 
% For bottom right corner, Start at slice 23, end at end
% 
% X start: 72 um from left
% Y start: 84 um from top
% Width: 34 um
% Height: 34 um

crop_current(conn, 72, 84, 34, 34, 23, 182, channel, 'region4_channel2_SHG');


% Region 5
% 
% Left side, start at 20, end at slice 136
% 
% X start: 19 um from left
% Y start: 49 um from top
% Width: 23 um
% Height: 23 um

crop_current(conn, 19, 49, 23, 23, 20, 136, channel, 'region5_channel2_SHG');


function crop_current(conn, x0, y0, w, h, z0p, zp, channel, subFolderName)
% x0, y0 : coordinates in um
% w, h   : width and heigth in um
% z0p, z0: start and end plane number (pixels)
% subFolderName: name of the subfolder where to save the extracted images

uCoords = [ x0, y0, 1;
    x0 + w, y0 + h, 1];

cd('/work/projects/bill/');

pCoords = round(conn.mapPositionsUnitsToVoxels(uCoords));
x0 = pCoords(1, 1); dx = pCoords(2, 1) - x0;
y0 = pCoords(1, 2); dy = pCoords(2, 2) - y0;
z0 = z0p;           dz = zp - z0p;

sub = conn.getDataSubVolumeRM(x0, y0, z0, channel, 0, dx, dy, dz);

mkdir(subFolderName);
cd(subFolderName);

nPlanes = size(sub, 3);
s       = length(num2str(nPlanes));
strg    = sprintf('%%.%dd', s);

for i = 1 : nPlanes

    imwrite(sub(:, :, i), [subFolderName, '_', sprintf( strg,  i), '.tif'], ...
        'Compression', 'none');
    
end

cd('..');