function copyCorrectedFilesToFolder

if ~exist('/work/projects/bill/GroundTruth/analysis_24', 'dir')
    mkdir('/work/projects/bill/GroundTruth/analysis_24');
end
    
!cp /work/projects/bill/GroundTruth/segmented_postprocessed_backup/groundTruth_*_segmented.tif /work/projects/bill/GroundTruth/analysis_24/
