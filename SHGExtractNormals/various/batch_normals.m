% REGION 1
try
    extract_normals('/work/projects/bill/region1/segmented_6', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region1/segmented_12', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region1/segmented_24', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region1/segmented_48', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region1/segmented_96', 0.132, 0.250, 5, 1);
catch
end

% REGION 2
try
    extract_normals('/work/projects/bill/region2/segmented_6', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region2/segmented_12', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region2/segmented_24', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region2/segmented_48', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region2/segmented_96', 0.132, 0.250, 5, 1);
catch
end

% REGION 3
try
    extract_normals('/work/projects/bill/region3/segmented_6', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region3/segmented_12', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region3/segmented_24', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region3/segmented_48', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region3/segmented_96', 0.132, 0.250, 5, 1);
catch
end

% REGION 4
try
    extract_normals('/work/projects/bill/region4/segmented_6', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region4/segmented_12', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region4/segmented_24', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region4/segmented_48', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region4/segmented_96', 0.132, 0.250, 5, 1);
catch
end

% REGION 5
try
    extract_normals('/work/projects/bill/region5/segmented_6', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region5/segmented_12', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region5/segmented_24', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region5/segmented_48', 0.132, 0.250, 5, 1);
catch
end
try
    extract_normals('/work/projects/bill/region5/segmented_96', 0.132, 0.250, 5, 1);
catch
end


