function averageGroundTruth()

outDir = '/work/projects/bill/GroundTruth/';

dirName = '/work/projects/bill/GroundTruth/Aaron_segmentationByEye/';
fName = 'subregion1_q3_z_1_14_01.tif';
stackA = readStack(quGetFileSequence(dirName, fName));

dirName = '/work/projects/bill/GroundTruth/Bill_SegmentationByEye_Reg1_1to14/HandDrawnLabels/Photoshop Processed/';
fName = 'bill01.png';
stackB = readStack(quGetFileSequence(dirName, fName));

dirName = '/work/projects/bill/GroundTruth/Bill_SegmentationByEye_Reg1_1to14/IlastikTrainAndPredict_AfterHandDrawn/Photoshop Processed/';
fName = 'bill01.png';
stackC = readStack(quGetFileSequence(dirName, fName));

dirName = '/work/projects/bill/GroundTruth/NOHSegmentation/preprocessed';
fName = 'segment1.png';
stackD = readStack(quGetFileSequence(dirName, fName));

% dirName = '/work/projects/bill/Algorithm_Segmentation_LowThreshold/';
% fName = 'subregion1_01.tif';
% stackD = readStack(quGetFileSequence(dirName, fName));

stack = stackA + stackB + stackC + stackD;

% stack = stackA + stackB + stackC;

stack = nrm(stack, 8);

nPlanes = size(stack, 3);
strg    = sprintf('%%.%dd', length(num2str(nPlanes)));

for i = 1 : nPlanes

    indxStr  = sprintf(strg, i);
    currentFileName = fullfile(outDir, ...
        ['groundTruth_', indxStr, '.tif']);
    
    imwrite(stack(:, :, i), currentFileName, 'Compression', 'none');
    
end

% nVotes = 3;
% stack = stack >= nVotes;
% stack = 255 .* uint8(stack);


function stack = readStack(stackFileNames)

% Read the first image
img = imread(stackFileNames{1});

% If its RGB just take the first channel
img = img(:, :, 1);

% Allocate space
stack = zeros([size(img), numel(stackFileNames)], class(img));

% Put the first image
stack(:, :, 1) = img;

% Add all the others
for i = 2 : numel(stackFileNames)
    img = imread(stackFileNames{i});
    img = img(:, :, 1);
    stack(:, :, i) = img;
end

% Make sure to return a double binarized stack
stack = double(stack > 0);

    