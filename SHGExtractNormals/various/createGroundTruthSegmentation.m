function createGroundTruthSegmentation(hQu)

outpath = '/work/projects/bill/Aaron_segmentationByEye';
fileBody = 'subregion1_q3_z_1_14';

aDataset = quGetDataset(hQu);

nTimepoints = getNTimepoints(aDataset);

strg = sprintf('%%.%dd', length(num2str(nTimepoints)));

selections = quGetSelections(hQu);

for i = 1 : nTimepoints
   
    bw = zeros([getHeight(aDataset), getWidth(aDataset)]);
    
    currentSelections = getSelectionsForTimepoint(selections, i);
    
    for j = 1 : numel(currentSelections)
       
        selection = currentSelections{j};
        
        [x0, y0, ~, x, y] = getBoundingBox(selection);
        bwMask = double(getBWMask(selection));
        bwMask = bwMask(:, :, 1);
        
        bwEmpty = zeros([getHeight(aDataset), getWidth(aDataset)]);
        bwEmpty(y0 : y, x0: x) = bwMask;

        bw = bw + bwEmpty;

    end
    
    bw(bw > 0) = 255;
    bw = cast(bw, 'uint8');
    
    % Save the image
    indxStr = sprintf(strg,  i);
    
    imwrite(bw, fullfile(outpath, [fileBody, '_', indxStr, '.tif']), ...
        'Compression', 'None');
     
end

function sel = getSelectionsForTimepoint(selections, timepoint)

sel = {};

c = 0;

for i = 1 : numel(selections)

    if eq(anchor(selections{i}, 'get'), timepoint)
        c = c + 1;
        sel{c} = selections{i};
    end
    
end
