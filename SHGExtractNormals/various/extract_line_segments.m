function BW = extract_line_segments(img, orientations, DEBUG)

JUST_BINARIZE = 0;

% =========================================================================
%
% CHECK INPUT
%
% =========================================================================

if nargin == 1
    orientations = 24;
    DEBUG = 0;
elseif nargin == 2
    DEBUG = 0;
elseif nargin == 3
    % Ok
else
    error('Wrong number of input arguments.');
end

% =========================================================================
%
% PROCESS
%
% =========================================================================

if DEBUG == 1
    figure('NumberTitle', 'off', 'Name', num2str(orientations));
    subplot(2, 2, 1); imshow(img, []); xlabel('Original');
end

img = uint16(round(noisecomp(img, 2, 6, 2, orientations, 1)));

if DEBUG == 1
    subplot(2, 2, 2); imshow(img, []); xlabel('Pre-processed');
end

if JUST_BINARIZE == 1
    BW = img > 0;
    BW = bwmorph(BW, 'clean', Inf);
    BW = bwmorph(BW, 'spur', Inf);
    return;
end

BW = adaptivethresh(img, 25, 0, 'gaussian', 'relative');

BW = imerode(BW, strel('disk', 1));
 
BW = bwmorph(BW, 'clean', Inf);
BW = bwmorph(BW, 'spur', Inf);

BW = imdilate(BW, strel('disk', 1));

if DEBUG == 1
    subplot(2, 2, 3); imshow(BW, []); xlabel('Thresholded and cleaned');
end

BW = edge(BW, 'canny');

% Create an overlay
if DEBUG == 1
    imgRGB = zeros([size(img), 3], class(img));
    img = cast(double(intmax(class(img))) .* nrm(img, 1), class(img));
    imgRGB(:, :, 1) = cast(0.5 * (double(img) + double(intmax(class(img))) .* BW), class(img));
    imgRGB(:, :, 2) = img;
    imgRGB(:, :, 3) = img;
    subplot(2, 2, 4); imshow(imgRGB, []); xlabel('Edges');
end
