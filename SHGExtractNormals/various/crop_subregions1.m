% Crops the stack 'region1'

strg    = sprintf( '%%.%dd', 3 );

stack = zeros([258, 258, 160], 'uint16');

for i = 1 : 160
   
    indxStr  = sprintf(strg,  i);
    stack(:, :, i) = imread(fullfile('/work/projects/bill/region1', ...
        ['region1_', indxStr, '.tif']));

end

subregion1_82_147 = stack(86 : 86 + 127 -1, 103 : 103 + 127 - 1, 82 : 147);
subregion1_23_55 = stack(86 : 86 + 127 -1, 103 : 103 + 127 - 1, 23 : 55);
subregion1_1_14 = stack(86 : 86 + 127 -1, 103 : 103 + 127 - 1, 1 : 14);


% First range

z0 = 82;

for i = 1 : size(subregion1_82_147, 3)
    
    indxStr  = sprintf(strg,  z0 + i - 1);
    
    fileName = fullfile('/work/projects/bill/subregion1_q1_z_82_147', ...
        ['subregion1_', indxStr, '.tif']);
    
    imwrite(subregion1_82_147(:, :, i), fileName, 'Compression', 'None');
    
    
end

% Second range

z0 = 23;

for i = 1 : size(subregion1_23_55, 3)
    
    indxStr  = sprintf(strg,  z0 + i - 1);
    
    fileName = fullfile('/work/projects/bill/subregion1_q2_z_23_55', ...
        ['subregion1_', indxStr, '.tif']);
    
    imwrite(subregion1_23_55(:, :, i), fileName, 'Compression', 'None');
    
    
end


% Third range

z0 = 1;

for i = 1 : size(subregion1_1_14, 3)
    
    indxStr  = sprintf(strg,  z0 + i - 1);
    
    fileName = fullfile('/work/projects/bill/subregion1_q3_z_1_14', ...
        ['subregion1_', indxStr, '.tif']);
    
    imwrite(subregion1_1_14(:, :, i), fileName, 'Compression', 'None');
    
    
end
