% Crops the stack 'region3' in both channels 
% (region3_channel1_fluorescence and region3_channel2_SHG)

strg    = sprintf( '%%.%dd', 3 );

% --- FLUORESCENCE --------------------------------------------------------

stack = zeros([129, 129, 41], 'uint16');

z = 1;

for i = 70 : 110
   
    indxStr  = sprintf(strg,  i);
    
    stack(:, :, z) = imread(fullfile(...
        '/work/projects/bill/region3_channel1_fluorescence', ...
        ['region3_', indxStr, '.tif']));

    z = z + 1;

end

subregion3_70_110 = stack(21 : 93, 30 : 94, 1 : 41);

z0 = 70;

for i = 1 : size(subregion3_70_110, 3)
    
    indxStr  = sprintf(strg,  z0 + i - 1);
    
    fileName = fullfile(...
        '/work/projects/bill/subregion3_channel1_fluorescence_70-110', ...
        ['subregion3_', indxStr, '.tif']);
    
    imwrite(subregion3_70_110(:, :, i), fileName, 'Compression', 'None');
    
    
end

% --- SHG --------------------------------------------------------

stack = zeros([129, 129, 41], 'uint16');

z = 1;

for i = 70 : 110
   
    indxStr  = sprintf(strg,  i);
    
    stack(:, :, z) = imread(fullfile(...
        '/work/projects/bill/region3_channel2_SHG', ...
        ['region3_channel2_SHG_', indxStr, '.tif']));

    z = z + 1;

end

subregion3_70_110 = stack(21 : 93, 30 : 94, 1 : 41);

z0 = 70;

for i = 1 : size(subregion3_70_110, 3)
    
    indxStr  = sprintf(strg,  z0 + i - 1);
    
    fileName = fullfile(...
        '/work/projects/bill/subregion3_channel2_SHG_70-110', ...
        ['subregion3_', indxStr, '.tif']);
    
    imwrite(subregion3_70_110(:, :, i), fileName, 'Compression', 'None');
    
    
end
