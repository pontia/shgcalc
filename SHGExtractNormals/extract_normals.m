function extract_normals(workingFolder, pixelSize, zStep, maxLateralD, saveQCImages, interpStep, extendLength)

if nargin < 6 && nargin > 7 
    error('6 or 7 input parameters expected.');
end

if nargin == 6
    extendLength = 0;
end

global DEBUG

% We don't really care about the voxel size, we just need the correct axial
% to lateral aspect ratio
ASPECT_RATIO = zStep / pixelSize;

% Calculate the interpolation density
interpN = 1 + floor(pixelSize ./ interpStep);
if interpN < 2
    interpN = 2;
end

DEBUG = 0;

if DEBUG == 1
    hDebugFigure = figure('Name', 'DEBUG', 'NumberTitle', 'off');
end

% Prepare quality control figures file names (if needed)
if saveQCImages == 1
    
    hQCFigure = figure('Name', 'Boundaries', 'NumberTitle', 'off');
    
    % Scan the working folder for images
    d = dir(workingFolder);
    nImages = numel(d);
    
    % First find all the file names
    fileNames = cell(1, nImages);
    c = 0;
    for i = 1 :nImages
        
        % Skip non-image entries
        if d(i).isdir == 1
            continue
        end
        if isempty(strfind(d(i).name, '_segmented_post_processed.tif'))
            continue
        end
        
        c = c + 1;
        fileNames{c} = d(i).name;
        
    end
    
    fileNames = fileNames(1 : c);
    
    % Sort the file names
    fileNames = sortFilesNumerically(fileNames);
    
end

% =========================================================================
%
% Load the boudaries results
%
% =========================================================================

try
    S = load(fullfile(workingFolder, 'BOUNDARIES.mat'));
    BOUNDARIES = S.BOUNDARIES;
    ALL_COORDS = S.ALL_COORDS;
    clear('S');
catch
    disp(['File BOUNDARIES.mat not found in ', workingFolder]);
    return
end

% Get axes extends for the plots
if saveQCImages == 1
    maxX = -1;
    maxY = -1;
    for i = 1 : numel(ALL_COORDS)
        
        tmpX = max(ALL_COORDS{i}(:, 2));
        if tmpX > maxX
            maxX = tmpX;
        end
        
        tmpY = max(ALL_COORDS{i}(:, 1));
        if tmpX > maxY
            maxY = tmpY;
        end
        
    end
    
    maxX = maxX + 1;
    maxY = maxY + 1;
    
end

% =========================================================================
%
% Go over all boundaries, extract the linear stretches, and extend them by
% a user defined amount
%
% =========================================================================

BOUNDARIES_EXTENDED = {};

if extendLength > 0

    nPlanes = numel(BOUNDARIES);
    
    BOUNDARIES_EXTENDED = cell(1, nPlanes);
    
    nPointsToAdd = 2 * extendLength + 1;
    
    % Inform
    disp('Extending boundaries...');
    
    for plane = 1 : nPlanes
        
        % Inform
        disp(['Processing plane ', num2str(plane), ' of ', num2str(nPlanes)]);
        
        % Get the boundaries
        allB  = BOUNDARIES{plane};
        
        % Number of boundaries in this plane
        nBoundariesInPlane = numel(allB);
        
        % Process all boundaries in current plane
        for boundary = 1 : nBoundariesInPlane
            
            % Get the boundary
            B = allB{boundary};
            if isempty(B)
                continue;
            end
            
            % Find the subset with high curvature (i.e. the curved ends)
            linearSegments = findSubsetWithHighCurvature(B);
            
            extendedLinearSegments = cell(1, 2);
            
            if numel(linearSegments) == 2
                
                % We process only the linear segments of the boundaries
                for segment = 1 : 2
                    
                    % Extract vectors for ease of calculation
                    x = linearSegments{segment}(:, 2);
                    y = linearSegments{segment}(:, 1);
                    
                    % Make sure we have enough points to work with
                    lenStretch = numel(x);
                    if lenStretch < 3
                        
                        if lenStretch == 1
                            xnew = [x, x, x];
                            ynew = [y, y, y];
                        elseif lenStrtch == 2
                            xnew = [ x; x(2)];
                            ynew = [ y; y(2)];
                        else
                            %This cannot happen
                        end
                       
                        % Now use the first three and last three points to calculate
                        % the line to be used to extend the boundary
                        tLeft  = [xnew(1) - xnew(3); ynew(1) - ynew(3)];
                        tRight = [xnew(end) - xnew(end - 2); ynew(end) - ynew(end - 2)];
                        tLeft  = tLeft ./ norm(tLeft, 2);
                        tRight  = tRight ./ norm(tRight, 2);

                    elseif lenStretch < (2 * extendLength + 3)

                        if lenStretch >= 2 * extendLength
                            
                            % Take 2 x 'extendLength' positions in the middle
                            dC = extendLength;
                            nC = fix(lenStretch / 2) + 1;

                        elseif lenStretch >= extendLength
                            
                            % Take 'extendLength' positions in the middle
                            dC = fix(extendLength / 2);
                            nC = fix(lenStretch / 2) + 1;

                        elseif lenStretch >= 3

                            dC = fix(lenStretch / 2);
                            nC = fix(lenStretch / 2) + 1;

                        else
                            
                           % This cannot happen

                        end

                        start = nC - dC;
                        if start < 1, start = 1; end
                        stop = nC + dC;
                        if stop > lenStretch, stop = lenStretch; end
                        xnew = x(start : stop);
                        ynew = y(start : stop);
                        
                        % Now use the first three and last three points to calculate
                        % the line to be used to extend the boundary
                        tLeft  = [xnew(1) - xnew(3); ynew(1) - ynew(3)];
                        tRight = [xnew(end) - xnew(end - 2); ynew(end) - ynew(end - 2)];
                        tLeft  = tLeft ./ norm(tLeft, 2);
                        tRight  = tRight ./ norm(tRight, 2);

                    else
                        
                        % Drop the last entendLength on both sides
                        xnew = x(extendLength + 1 : end - extendLength);
                        ynew = y(extendLength + 1 : end - extendLength);
                    
                        % Now use the first three and last three points to calculate
                        % the line to be used to extend the boundary
                        tLeft  = [xnew(1) - xnew(3); ynew(1) - ynew(3)];
                        tRight = [xnew(end) - xnew(end - 2); ynew(end) - ynew(end - 2)];
                        tLeft  = tLeft ./ norm(tLeft, 2);
                        tRight  = tRight ./ norm(tRight, 2);
                    
                    end
                    
                    % Now extend them
                    lastPosLeft = [xnew(1); ynew(1)] + 2 * extendLength * tLeft;
                    lastPosRight = [xnew(end); ynew(end)] + 2 * extendLength * tRight;
                    xAddLeft  = linspace(lastPosLeft(1), xnew(1), nPointsToAdd);
                    yAddLeft  = linspace(lastPosLeft(2), ynew(1), nPointsToAdd);
                    xAddRight = linspace(xnew(end), lastPosRight(1), nPointsToAdd);
                    yAddRight = linspace(ynew(end), lastPosRight(2), nPointsToAdd);
                    xfinal = [xAddLeft(:); xnew; xAddRight(:)];
                    yfinal = [yAddLeft(:); ynew; yAddRight(:)];
                    
                    % Store the final segment
                    extendedLinearSegments{segment} = ...
                        [yfinal(:), xfinal(:), ...
                        segment .* ones(numel(yfinal), 1)];

                    % Plot debug figure
                    if DEBUG == 1
                        
                        figure(hDebugFigure);
                        clf(hDebugFigure);
                        
                        plot(x, y, 'rx');
                        axis equal;
                        hold on;
                        plot(xnew, ynew, 'gs');
                        plot(lastPosLeft(1), lastPosLeft(2), 'ks');
                        plot(lastPosRight(1), lastPosRight(2), 'ks');
                        plot(xAddLeft, yAddLeft, 'bs');
                        plot(xAddRight, yAddRight, 'bs');
                        axis equal
                        
                        pause
                    end
                    
                end
                
            else
                
                nPoints = fix(size(B, 1) / 2);
                extendedLinearSegments{1} = ...
                    [B(1 : nPoints, :), ones(nPoints, 1), ];
                extendedLinearSegments{2} = ...
                    [B(nPoints + 1 : end, :), 2 .* ones(size(B, 1) - nPoints, 1)];
                
            end
            
            % Now reassemble the boundary and store it
            BOUNDARIES_EXTENDED{plane}{boundary} = cat(1, ...
                extendedLinearSegments{1}, ...
                extendedLinearSegments{2});
            
        end
        
    end
end

% =========================================================================
%
% Go over all edge pixels, find the corresponding boundaries in (plane -
% 1), plane and (plane + 1), and calculate the normals to the outline.
%
% =========================================================================

% If we extended the boundaries, now we use them
if extendLength > 0 && ~isempty(BOUNDARIES_EXTENDED)
    
    BOUNDARIES = BOUNDARIES_EXTENDED;

end

nPlanes = numel(BOUNDARIES);

ALL_NORMALS = cell(1, nPlanes);

% Inform
disp('Calculating normals...');

for plane = 2 : nPlanes - 1
    
    % Inform
    disp(['Processing plane ', num2str(plane - 1), ' of ', ...
        num2str(nPlanes - 2)]);
    
    if saveQCImages == 1
        clf(hQCFigure);
        axes(); %#ok<LAXES>
        axis([0 maxX 0 maxY]);
        axis ij
        hold on;
    end
    
    if DEBUG == 1
                
        figure(hDebugFigure);
        clf(hDebugFigure);
    
    end
    
    % ---------------------------------------------------------------------
    %
    % First, find global displacement between planes by comparing the
    % centers of mass of the boundaries
    %
    % ---------------------------------------------------------------------
    
    allB  = BOUNDARIES{plane};
    allBL = BOUNDARIES{plane - 1};
    allBU = BOUNDARIES{plane + 1};
    
    % Get the coordinates into matrices
    mB  = getCentersOfMassAsMatrix(allB);
    mBL = getCentersOfMassAsMatrix(allBL);
    mBU = getCentersOfMassAsMatrix(allBU);
    
    lOffset = getGlobalDisplacement(mBL, mB);
    uOffset = getGlobalDisplacement(mB, mBU);
    
    fprintf(1, ['Plane ', num2str(plane), ...
        ' vs. ', num2str(plane - 1), ' and ', ...
        num2str(plane + 1), ': displ = (', ...
        num2str(lOffset), ') and (', num2str(uOffset), ')\n']);
    
    % ---------------------------------------------------------------------
    %
    % Now apply the corection for the global displacement for all
    % coordinates in (plane - 1) and (plane + 1)
    %
    % ---------------------------------------------------------------------
    
    % Get the coordinates from plane - 1 and plane + 1 and apply the
    % global-displacement correction
    all_xl       = ALL_COORDS{plane - 1}(:, 2) + lOffset(2);
    all_yl       = ALL_COORDS{plane - 1}(:, 1) + lOffset(1);
    all_indicesL = ALL_COORDS{plane - 1}(:, 3);
    all_xu       = ALL_COORDS{plane + 1}(:, 2) - uOffset(1);
    all_yu       = ALL_COORDS{plane + 1}(:, 1) - uOffset(2);
    all_indicesU = ALL_COORDS{plane + 1}(:, 3);
    
    % Clear unneeded variables
    clear allB allBL allBU mB mBL mBU
    
    % ---------------------------------------------------------------------
    %
    % Now find for each boundary in (plane), the closest ones (corrected
    % for the global displacement) in (plane - 1) and (plane + 1)
    %
    % ---------------------------------------------------------------------
    
    % Initialize the normals matrix with NaNs - the normals that cannot be
    % calculated will be automatically marked
    nBoundariesInPlane = numel(BOUNDARIES{plane});
    
    % Allocate space to store the boundaries for the quality control image
    if saveQCImages == 1
        qcBoundaries = [];
        qcBoundariesL = [];
        qcBoundariesU = [];
        qcBoundariesID = {};
        qcBoundariesCM = [];
    end
    
    % Process all boundaries in current plane
    for boundary = 1 : nBoundariesInPlane
        
        % Get the boundary
        B = BOUNDARIES{plane}{boundary};
        if isempty(B)
            continue;
        end
        
        % Plot the complete boundary
        if DEBUG == 1
            
            figure(hDebugFigure);
            
            % Plot the (uncorrected) curves
            plot3(B(:, 2), B(:, 1), zeros(size(B, 1)), 'r-');
            axis equal
            hold on

        end
        
        % Figure out if we are growing clockwise, or counter-clockwise
        ccw = isCounterClockWise(B(:, 2), B(:, 1));
        
        % Find the subset with high curvature (i.e. the curved ends)
        if extendLength > 0 && ~isempty(BOUNDARIES_EXTENDED)
            linearSegments = cell(1, 2);
            linearSegments{1} = B(B(:, 3) == 1, 1 : 2);
            linearSegments{2} = B(B(:, 3) == 2, 1 : 2);
        else
            linearSegments = findSubsetWithHighCurvature(B);
        end

        % We process only the linear segments of the boundaries
        for segment = 1 : numel(linearSegments)
            
            % Extract vectors for ease of calculation
            x = linearSegments{segment}(:, 2);
            y = linearSegments{segment}(:, 1);
            
            % Find the closest (global-displacement corrected) boundaries
            % in plane - 1 and plane + 1
            BL = findClosestBoundary([x, y], [all_xl, all_yl, all_indicesL]);
            if isempty(BL)
                disp('No boundaries found!');
                continue;
            end
            BU = findClosestBoundary([x, y], [all_xu, all_yu, all_indicesU]);
            if isempty(BU)
                disp('No boundaries found!');
                continue;
            end
            
            % Extract vectors for ease of calculation
            xl = BL(:, 1);
            yl = BL(:, 2);
            clear BL
            
            xu = BU(:, 1);
            yu = BU(:, 2);
            clear BU
            
            % -------------------------------------------------------------
            %
            % Now go over all pixels of current segment in (plane) and find
            % the closest pixel in the corrected boundaries in (plane - 1)
            % and (plane + 1)
            %
            % -------------------------------------------------------------
            
            if DEBUG == 1
                
                figure(hDebugFigure);
                
                % Plot the linear segment and the complete boundaries
                % in (plane - 1) and (plane + 1)
                plot3(x, y, zeros(size(x)), 'mo');
                axis equal
                hold on
                plot3(xl - lOffset(2), yl - lOffset(1), ...
                    -1 .* ASPECT_RATIO .* ones(size(xl)), 'k-');
                plot3(xu + uOffset(2), yu + uOffset(2), ...
                    ASPECT_RATIO .* ones(size(xu)), 'k-');
            end
            
            % Number of pixels in the boundary
            nPixelsBoundary = numel(x);
            
            % Allocate space for the normals
            % The matrix contains:
            % BoundaryID SegmentID x y nx ny nz
            normals = nan(nPixelsBoundary - 1, 7);
            
            % Go over all positions in current boundary
            for pos = 1 : nPixelsBoundary - 1
                
                % Store the first part of the normals info
                normals(pos, 1 : 4) = [boundary segment x(pos) y(pos)];
              
                % Calculate the tangent vector
                if pos == 1
                    t = [
                        x(pos + 1) - x(pos);
                        y(pos + 1) - y(pos);
                        0
                        ];
                elseif pos == (nPixelsBoundary - 1)
                    t = [
                        x(end) - x(pos);
                        y(end) - y(pos);
                        0
                        ];
                else
                    t = 0.5 .* [
                        x(pos + 1) - x(pos - 1);
                        y(pos + 1) - y(pos - 1);
                        0];
                end
                
                % Normalize it
                t = t ./norm(t);
                
                % Find the closest position to x(p), y(p) in the -1 (l) plane
                posL = findClosestPointFitted([x(pos), y(pos)], [xl, yl], ...
                    maxLateralD, 1);
                if isempty(posL)
                    continue;
                end
                
                % Undo the global displacement
                posL = posL - lOffset;
                
                % Find the closest position to x(p), y(p) in the +1 (u) plane
                posU = findClosestPointFitted([x(pos), y(pos)], [xu, yu], ...
                    maxLateralD, 1);
                if isempty(posU)
                    continue;
                end
                
                % Undo the global displacement
                posU = posU + uOffset;
                
                % Calculate the perpendicular vector
                p = 0.5 .* ([posU, ASPECT_RATIO] - [posL, -ASPECT_RATIO]);
                p = p ./ norm(p);
                
                % -------------------------------------------------------------
                %
                % Calculate and store the normal
                %
                % -------------------------------------------------------------
                
                % Calculate the normal to the curve
                if ccw == 1
                    n = cross(t, p);
                else
                    n = -cross(t, p);
                end
                
                % Normalize it
                n = n ./ norm(n);
                
                % Append to the normals matrix
                normals(pos, 5 : 7) = n;
                
            end
            
            % Interpolate normals
            if interpN > 2
                
                normals_interp = [];
                
                for k = 1 : size(normals, 1) - 1
                    
                    start = normals(k, :);
                    stop  = normals(k + 1, :);
                    
                    xi  = linspace(start(1, 3), stop(1, 3), interpN);
                    yi  = linspace(start(1, 4), stop(1, 4), interpN);
                    nxi = linspace(start(1, 5), stop(1, 5), interpN);
                    nyi = linspace(start(1, 6), stop(1, 6), interpN);
                    nzi = linspace(start(1, 7), stop(1, 7), interpN);
                    
                    normals_interp = cat(1, normals_interp, ...
                        [...
                        normals(1, 1) .* ones(numel(xi), 1), ...
                        normals(1, 2) .* ones(numel(yi), 1), ...
                        xi', yi', nxi', nyi', nzi', ...
                        ]);
                  
                end
                
                % Now replace the original normals with the interpolated
                % one
                normals = normals_interp;
                
            end
            
            
            % Store the calculated normals for current plane
            ALL_NORMALS{plane} = cat(1, ALL_NORMALS{plane}, normals);

            % Append the boundaries for plane, plane - 1, and plane + 1 for
            % generating the quality control image later
            if saveQCImages == 1
                
                % Store boundaries for plane
                qcBoundaries = cat(1, qcBoundaries, ...
                    [x, y, zeros(size(x))]);
                qcBoundaries = cat(1, qcBoundaries, [NaN, NaN, NaN]);
                
                % Store boundaries for plane - 1
                qcBoundariesL = cat(1, qcBoundariesL, ...
                    [xl - lOffset(2), yl - lOffset(1), ...
                    -ASPECT_RATIO .* ones(size(xl))]);
                qcBoundariesL = cat(1, qcBoundariesL, [NaN, NaN, NaN]);
                
                % Store boundaries for plane + 1
                qcBoundariesU = cat(1, qcBoundariesU, ...
                    [xu + uOffset(2), yu + uOffset(1), ...
                    ASPECT_RATIO .* ones(size(xu))]);
                qcBoundariesU = cat(1, qcBoundariesU, [NaN, NaN, NaN]);
                
                % Store the boundary ID
                qcBoundariesID = cat(1, qcBoundariesID, ...
                    [num2str(boundary), ' - ', num2str(segment)]);
                
                % Store the boundary position
                qcBoundariesCM = cat(1, qcBoundariesCM, [mean(x), mean(y)]);
                
            end
            
        end
       
        
    end
    
    if DEBUG == 1
        pause;
    end
    
    % Plot all boundaries for plane, plane - 1 and plane + 1 and
    % all normals for plane at once and save as image
    if saveQCImages == 1
        
        figure(hQCFigure);
        
        % Plot the boundaries and the ID
        plot3(qcBoundaries(:, 1), qcBoundaries(:, 2), ...
            qcBoundaries(:, 3), 'r-');
        plot3(qcBoundariesL(:, 1), qcBoundariesL(:, 2), ...
            qcBoundariesL(:, 3), 'g-');
        plot3(qcBoundariesU(:, 1), qcBoundariesU(:, 2), ...
            qcBoundariesU(:, 3), 'b-');
        text(qcBoundariesCM(:, 1), qcBoundariesCM(:, 2), ...
            zeros(size(qcBoundariesCM, 1), 1), qcBoundariesID);
        
        % Prepare the data for a quick plot
        nBoundaries = max(ALL_NORMALS{plane}(:, 1));
        data = [];
        for i = 1 : nBoundaries
            data = cat(1, data, ...
                ALL_NORMALS{plane}(ALL_NORMALS{plane}(:, 1) == i, :));
            data = cat(1, data, nan(1, size(data, 2)));
        end
        
        % Plot the normals
        plot3( ...
            [data(:, 3)'; data(:, 3)' + data(:, 5)'], ...
            [data(:, 4)'; data(:, 4)' + data(:, 6)'], ...
            [zeros(1, size(data, 1)); data(:, 7)'], ...
            'r-');
        
        % Savr the image to disk
        [~, b, e] = fileparts(fileNames{plane});
        fName = [b, '_normals', e];
        print(['-f', num2str(hQCFigure)], '-dpng', '-r200', ...
            fullfile(workingFolder, fName));
        
    end
    
end

% Close the figure
if saveQCImages == 1
    close(hQCFigure);
end

% Save the results
save(fullfile(workingFolder, 'NORMALS.mat'), 'ALL_NORMALS');

% =========================================================================
% =========================================================================
% =========================================================================

function mB = getCentersOfMassAsMatrix(B)
nB = numel(B);
mB  = zeros([numel(B), 2]);
for c = 1 : nB
    if ~isempty(B{c})
        mB(c, 1 : 2) = mean(B{c}(:, 1 : 2), 1);
    else
        mB(c, 1 : 2) = nan(1, 2);
    end
end
[yB, ~] = find(isnan(mB));
mB(unique(yB), :) = [];

% =========================================================================

function AB = getGlobalDisplacement(A, B)

D = createDistanceMatrix(A, B);

d = zeros(size(D, 1), 2);

for i = 1 : size(D, 1)
    indx = find(D(i, :) == min(D(i, :)), 1);
    d(i, 1 : 2) = B(indx, :) - A(i, :);
end

AB = median(d, 1);

% =========================================================================

function nB = findClosestBoundary(B, allB)

votes = zeros(1, size(allB, 1));

D = createDistanceMatrix(B, allB(:, 1 : 2));

for i = 1 : size(D, 1)
    b = allB(D(i, :) == min(D(i, :)), 3);
    votes(b) = votes(b) + 1;
end

indx = find(votes == max(votes), 1);

nB = allB(allB(:, 3) == indx, 1 : 2);

% =========================================================================

function cw = isCounterClockWise(x, y)

% All delta-angles should be either positive or negative depending on
% whether the boundary was traced counter-clockwise or clockwise,
% respectively, with the exception of one, when one crosses either 0 or
% 180 (I should check which).
dAngles = diff(atan2(y - mean(y), x - mean(x)));
cw = numel(find(dAngles > 0)) > numel(find(dAngles < 0));

% =========================================================================

function cPos = findClosestPointFitted(pos, B, maxLateralD, fitRange)
% fitRange: set to 0 to disable; otherwise, take a range of 'fitRange'
% positions around the closest point ans fits a straight line. Take the
% x of the closest point and the fitted y (at the x position of the found
% point).

cPos = [];
D = createDistanceMatrix(pos, B);
d = min(D);
if d > maxLateralD
    return
end

indx = find(D == d, 1);
if fitRange == 0 || fitRange == 1
    cPos = B(indx, :);
    return;
end

if mod(fitRange, 2) == 0
    fitRange = fitRange + 1;
end

hR = fix(fitRange / 2);

indxStart = indx - hR;
if indxStart < 1
    indxStart = 1;
end

indxStop = indx + hR;
if indxStop > size(B, 1)
    indxStop = size(B, 1);
end

x = B(indxStart : indxStop, 1);
y = B(indxStart : indxStop, 2);
X = [ones(numel(x), 1) x];
theta = pinv(X'*X)*X'*y;
yp = [1 B(indx, 1)] * theta;
cPos = [B(indx, 1) yp];

% =========================================================================

function linearSegments = findSubsetWithHighCurvature(B)

global DEBUG

fitRange = 7;
curvRange = 5;
t = 2.5; % 2.5; %3.0; % 2.5; %2.0; % 0.5; % 1.0;

% Make sure the range length is odd
if mod(fitRange, 2) == 0
    fitRange = fitRange + 1;
end

% Define the half-range
hR = fix(fitRange / 2);

% To prevent border effect, we wrap around
wrapAroundLength = hR + 1;
if wrapAroundLength >= size(B, 1)
    wrapAroundLength = fix(size(B, 1) / 2);
end
firstB = B(1 : wrapAroundLength, :);
lastB = B(end - wrapAroundLength + 1 : end, :);
B = cat(1, lastB, B);
B = cat(1, B, firstB);

% fitRange: number of points to use to fit a straight line
if fitRange < 3
    error('fitRange must be at least 3.');
end

curv = CurvatureEstimation(B, curvRange, curvRange);
curv = curv';

% Modulate the threshold to find two segments with longest stretch
allTs = 0 : 0.1 : 5;
bestT = -1;
bestLength = -1;
for t = allTs
    linearSegments = ...
        splitIntoSegments(curv, t, B, fitRange, wrapAroundLength);
    if numel(linearSegments) == 2
        len = size(linearSegments{1}, 1) + size(linearSegments{2}, 1);
        if len > bestLength;
            bestLength = len;
            bestT = t;
        end
    end
end

% Now use the best t for the actual processing
if bestT ~= -1
    [linearSegments, B, curv, candidates, highCurvIndices, th] = ...
        splitIntoSegments(curv, bestT, B, fitRange, wrapAroundLength);
else
    linearSegments = {};
end

% Filter the curve
% curv = medfilt1(curv, fitRange);

segColors = [
    0.1255, 0.6980, 0.6667;
    0.7294, 0.3333, 0.8275;
    1.0000, 0.6275, 0.4784;
    0.1804, 0.5451, 0.3412;
    0.0000, 1.0000, 1.0000;
    1.0000, 0.0000, 1.0000;
    1.0000, 1.0000, 0.0000;
    0.0000, 0.0000, 1.0000;
    0.0000, 1.0000, 0.0000];

if DEBUG == 1
    if bestT ~= -1
        h = figure;
        subplot(1,2,1);
        plot(curv, 'r-o'); hold on
        plot([0 numel(curv)], [bestT bestT], 'k-o');
        subplot(1,2,2);
        plot(B(:, 1), B(:, 2), 'k-o');
        hold on
        plot(B(candidates(~isinf(candidates)), 1), ...
            B(candidates(~isinf(candidates)), 2), 'kx', 'MarkerSize', 10);
        plot(B(highCurvIndices, 1), B(highCurvIndices, 2), 'ro', 'markerSize', 10);
        for i = 1 : numel(linearSegments)
            n = 1 + rem(i - 1, size(segColors, 1));
            clr = segColors(n, :);
            plot(linearSegments{i}(:, 1), linearSegments{i}(:, 2), 'p', ...
                'MarkerSize', 12, 'Color', clr);
        end
        axis equal
        pause
        close(h);
    end
end

% =========================================================================

function [linearSegments, B, curv, candidates, highCurvIndices, th] = ...
    splitIntoSegments(curv, t, B, fitRange, wrapAroundLength)
%
% When you are testing values for t ONLY SET linearSegments AS AN OUTPUT
% ARGUMENT, OR YOU WILL MESS UP THE INPUT ARGUMENTS
%
% Remove high-curvature regions (provided there are at least 'fitRange'
% continuous stretches (with 1-position gaps allowed)
th = median(curv) + t * mad(curv, 1);
candidates = [find(curv > th) Inf];
highCurvIndices = [];
n = 0;
stretch = candidates(1);
for i = 2 : numel(candidates)
    if candidates(i) - candidates(i - 1) == 1
        n = n + 1;
        stretch = cat(2, stretch, candidates(i));
    elseif candidates(i) - candidates(i - 1) == 2
        % Restore the position in the gap
        n = n + 1;
        stretch = cat(2, stretch, candidates(i) - 1);
        % Add current position
        n = n + 1;
        stretch = cat(2, stretch, candidates(i));
    else
        if n >= fitRange / 2
            highCurvIndices = cat(2, highCurvIndices, stretch);
        end
        n = 0;
        stretch = candidates(i);
    end
    
end


% Break down the low-curvature areas into separate segments that we will
% use to unroll the normals later. Each boundary should have two linear
% segments. If a linear segment is too short we discar it.
indices = find(diff(highCurvIndices) > 1);
nSegments = numel(indices);
linearSegments = cell(1, nSegments);
n = 0;
for nS = 1 : nSegments
    start = highCurvIndices(indices(nS)) + 1;
    stop  = highCurvIndices(indices(nS) + 1) - 1;
    if (stop - start + 1) >= (fitRange / 2)
        n = n + 1;
        linearSegments{n} = B(start : stop, :);
    end
end
linearSegments = linearSegments(1 : n);

% Now remove the wrapped-around borders
firstIndx = wrapAroundLength + 1;
lastIndx  = size(B, 1) - wrapAroundLength;
B = B(wrapAroundLength + 1 : end - wrapAroundLength, :);
curv = curv(wrapAroundLength + 1 : end - wrapAroundLength);
candidates(candidates < firstIndx) = [];
candidates(candidates > lastIndx) = [];
candidates = candidates - firstIndx + 1;
highCurvIndices(highCurvIndices < firstIndx) = [];
highCurvIndices(highCurvIndices > lastIndx) = [];
highCurvIndices = highCurvIndices - firstIndx + 1;
