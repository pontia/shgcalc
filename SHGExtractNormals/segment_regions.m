function segment_regions(saveQCImages, workingFolder, regions, orientations)
% saveQCImages : {0|1} save quality control figures
% workingFolder: folder where the region subfolders are located (results
%                wil be saved here)
% regions      : cell array with subfolder names (representing regions),
%                relative to the working folder
% orientations : number of orientations for the noisecomp() filter

if nargin == 0
    saveQCImages = 0;
    workingFolder = '/work/projects/bill/';
    regions = {'region1', 'region2', 'region3', 'region4', 'region5',};
    orientations = 24;
elseif nargin == 1
    workingFolder = '/work/projects/bill/';
    regions = {'region1', 'region2', 'region3', 'region4', 'region5',};
    orientations = 24;
elseif nargin == 2
    regions = {'region1', 'region2', 'region3', 'region4', 'region5',};
    orientations = 24;
elseif nargin == 3
    orientations = 24;
elseif nargin == 4
    % Ok
else
    error('Bad number of input parameters.');
end

% Process the regions
for i = 1 : numel(regions)
    process_region(orientations, workingFolder, regions{i}, saveQCImages);
end

% =========================================================================
% =========================================================================

function process_region(orientations, workingFolder, subFolderName, saveQCImages)
% subFolderName: name of the subfolder where to save the processed images

imgDirName = fullfile(workingFolder, subFolderName);
procDirName = [imgDirName, '/analysis_', num2str(orientations)];

mkdir(procDirName);

d = dir(imgDirName);

% Estimate number of digits needed for numerical extensions
nImages = numel(d) - 2; % We remove '.' and '..'. There might be more files,
                        % but overestimating is fine.
s       = length(num2str(nImages));
strg    = sprintf('%%.%dd', s);

hWaitbar = waitbar(0, ['Processing ', subFolderName, '...']);

% Process all images in the folder
nImages = numel(d);
for i = 1 :nImages
    
    % Skip non-image entries
    if d(i).isdir == 1
        continue
    end
    [~, f, e] = fileparts(d(i).name);
    if ~strcmpi(e, '.tif')
        continue
    end

    % Get numerical index
    [indx, body] = getNumericalIndex(f);
    
    % Read the image
    img = imread([imgDirName, '/', d(i).name]);
    
    % Segment the image
    BW = extract_line_segments(img, orientations);
    
    switch class(img)
        case 'uint8',
            BW = nrm(BW, 8);
        case {'uint16', 'single'}
            BW = nrm(BW, 16);
        otherwise,
            error('Unsupported data type!');
    end
    
    % Save the segmented image
    imwrite(BW, ...
        [procDirName, '/', body, sprintf(strg, indx), '_segmented.tif'], ...
        'Compression', 'none');
   
    % Create a quality control image and save it as well
    if saveQCImages == 1
        % Original image
        imwrite(nrm(img, 16), ...
            [procDirName, '/', body, sprintf(strg, indx), '_original.tif'], ...
            'Compression', 'none');
        % Overlay
        imgRGB = zeros([size(img), 3], class(img));
        img = cast(double(intmax(class(img))) .* nrm(img, 1), class(img));
        imgRGB(:, :, 1) = cast(...
            0.5 * (double(img) + double(intmax(class(img))) .* double(BW)), ...
            class(img));
        imgRGB(:, :, 2) = img;
        imgRGB(:, :, 3) = img;
        imwrite(imgRGB, ...
            [procDirName, '/', body, sprintf(strg, indx), '_segmentedQC.tif'], ...
            'Compression', 'none');
        
    end
    
    % Update waitbar
    waitbar(i / nImages, hWaitbar);
    
end
    
% Close waitbar
close(hWaitbar);

% =========================================================================
 
function [indx, body] = getNumericalIndex(f)

indx = '';

for i = numel(f) : -1 : 1
   
    n = str2double(f(i));
    if isnan(n)
        break
    end
    
    indx = [f(i), indx];

end

indx = str2double(indx);
body = f(1 : i);
