function normals = simNormals()
% This simulate the extraction of normals as in extract_normals, but under
% well controlled conditions.

% Parameters
lOffset = [1 1];  % "Global" displacement from plane -1 to plane 0: this
                  % will be SUBTRACTED from the -1 positions.
uOffset = [1 1];  % "Global" displacement from plane 0 to plane +1: this
                  % will be ADDED to the +1 positions.                 
invert  = 0;      % If invert is set to 0, the sequence of positions along 
                  % the circle grow in counter-clockwise sense; if set to
                  % 1, it grows in clockwise sense. This simulates the
                  % direction by which boundaries are traced by
                  % bwboundaries.

% =========================================================================
%
% PREPARE THE SYNTHETIC BOUNDARIES
%
% =========================================================================

% We use three slightly shifted circles in planes p - 1; p; p + 1
if invert == 0
    t = -pi : 0.05 : pi;
else
    t = pi : -0.05 : -pi;
end

% Plane 0
x = cos(t);
y = sin(t);
z = zeros(size(x));

% Figure out if we are growing clockwise, or counter-clockwise
ccw = isCounterClockWise(x, y);

% Plane -1
xl = x - lOffset(1);
yl = y - lOffset(2);
zl = -1 .* ones(size(xl));

% Plane + 1
xu = x + uOffset(1);
yu = y + uOffset(2);
zu = ones(size(xu));

plot3(x, y, z, 'r-');
axis equal
hold on
plot3(xl, yl, zl, 'k-');
plot3(xu, yu, zu, 'k-');

% =========================================================================
%
% PROCESS
%
% =========================================================================

% Apply global-displacement correction - this simulates finding the
% global displacement (by plane) with which the outlines are corrected
% before the closest pixels in the -1 and +1 planes are searched for.
xl = xl + lOffset(1);
yl = yl + lOffset(2);
xu = xu - uOffset(1);
yu = yu - uOffset(2);

% Go over all points on plane, and:
% - calculate the tangent vector
% - calculate the perpendicular vector
% - calculate the normal
% - plot

nPoints = numel(x);

% Allocate space for the normals
normals = nan(nPoints - 1, 3);
        
for i = 1 : nPoints - 1
   
    % Calculate the tangent vector
    if i == 1
        t = [x(i + 1) - x(i); y(i + 1) - y(i); z(i + 1) - z(i)];
    elseif i == (nPoints - 1)
        t = [x(end) - x(i); y(end) - y(i); z(end) - z(i)];
    else
        t = 0.5 .* [x(i + 1) - x(i - 1); y(i + 1) - y(i - 1); z(end) - z(i)];
    end

    % Normalize it
    t = t ./norm(t);
    
    % Find the closest position to x(p), y(p) in the -1 (l) plane
    L = [xl', yl'];
    DL = createDistanceMatrix([x(i), y(i)], L);
    posL = L(find(DL == min(DL), 1), :);

    % Undo the global displacement
    posL = posL - lOffset;
    
    % Find the closest position to x(p), y(p) in the +1 (u) plane
    U = [xu', yu'];
    DU = createDistanceMatrix([x(i), y(i)], U);
    posU = U(find(DU == min(DU), 1), :);
    
    % Undo the global displacement
    posU = posU + uOffset;
            
    % Calculate the perpendicular vector
    p = 0.5 .* ([posU, 1] - [posL, -1]);
    p = p ./ norm(p);
    
    % Calculate the normal to the curve
    if ccw == 1
        n = cross(t, p);
    else
        n = -cross(t, p);
    end
    
    % Normalize it
    n = n ./ norm(n);

    % Store
    normals(i, :) = n;

    % Plot the tangent vector
    quiver3(x(i), y(i), z(i), t(1), t(2), t(3), 1);
    
    % Plot the perpendicular vector
    quiver3(x(i), y(i), z(i), p(1), p(2), p(3), 1);
    
    % Plot the normal
    quiver3(x(i), y(i), z(i), n(1), n(2), n(3), 1);

end

% =========================================================================

function cw = isCounterClockWise(x, y)
angles = atan2(y - mean(y), x - mean(x));
cw = sum(diff(angles)) > 0;
