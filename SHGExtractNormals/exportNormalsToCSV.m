function exportNormalsToCSV(workingFolder)

% Load the normals
try
    S = load(fullfile(workingFolder, 'NORMALS.mat'));
    ALL_NORMALS = S.ALL_NORMALS;
    clear('S');
catch
    disp(['File NORMALS.mat not found in ', workingFolder]);
    return
end

% Export as csv file
% =========================================================================

fileName = fullfile(workingFolder, 'NORMALS.csv');

fid = fopen(fileName, 'w');
if fid == -1
    error('Could not create report file.' );
end

fprintf( fid, ...
    [ ...
    '%10s\t', ...    % Plane
    '%10s\t', ...    % Boundary
    '%10s\t', ...    % Segment
    '%10s\t', ...    % x
    '%10s\t', ...    % y
    '%10s\t', ...    % nx
    '%10s\t', ...    % ny
    '%10s\n' ], ...  % nz   
    'Plane', ...
    'Boundary', ...
    'Segment', ...
    'x', ...
    'y', ...
    'nx', ...
    'ny', ...
    'nz' );

for i = 1 : numel(ALL_NORMALS)
    
    % Write to console
    fprintf(1, ['Writing results for plane ', num2str(i), '.\n']);
    
    % Append plane number
    data = cat(2, i .* ones(size(ALL_NORMALS{i}, 1), 1), ALL_NORMALS{i});
    if isempty(data)
        continue;
    end
        
    % Write to file
    fprintf(fid, ...
        [ ...
        '%10d\t', ...      % Plane
        '%10d\t', ...      % Boundary
        '%10d\t', ...      % Segment
        '%10.4f\t', ...    % x
        '%10.4f\t', ...    % y
        '%10.4f\t', ...    % nx
        '%10.4f\t', ...    % ny
        '%10.4f\n' ], ...  % nz
        data' );
    
end

fclose(fid);