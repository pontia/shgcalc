function post_process_line_segments(workingFolder, saveQCImages)

global DEBUG
global H_FIGURE

if nargin == 1
    saveQCImages = 0;
end

DEBUG = 0;

% =========================================================================
%
% Read in all files
%
% =========================================================================

% Scan the working folder for images
d = dir(workingFolder);
nImages = numel(d);

% First find all the file names
fileNames = cell(1, nImages);
c = 0;
for i = 1 :nImages
    
    % Skip non-image entries
    if d(i).isdir == 1
        continue
    end
    if isempty(strfind(d(i).name, '_segmented.tif'))
        continue
    end
    
    c = c + 1;
    fileNames{c} = d(i).name;
    
end

fileNames = fileNames(1 : c);

% Sort the file names numerically!
fileNames = sortFilesNumerically(fileNames);

% Now read them
firstImage = imread(fullfile(workingFolder, fileNames{1}));

% Allocate memory
stack = zeros([size(firstImage) numel(fileNames)], class(firstImage));

stack(:, :, 1) = firstImage;

for i = 2 : numel(fileNames)
    stack(:, :, i) = imread(fullfile(workingFolder, fileNames{i}));
end

% =========================================================================
%
% Trace all boundaries
%
% =========================================================================

nPlanes = size(stack, 3);

BOUNDARIES = cell(1, nPlanes);
ALL_COORDS = cell(1, nPlanes);

if DEBUG == 1
    H_FIGURE = figure('NumberTitle', 'off', 'Name', '');
    subplot(1, 2, 1); axes()
    subplot(1, 2, 2); axes()
end

se = strel('disk', 1);

% Go over all planes
for plane = 1 : nPlanes

    disp(['Processing plane ', num2str(plane), ' of ', num2str(nPlanes)]);
    
    % Master
    M = stack(:, :, plane);

    % Allocate image to store the procesed outlines for quality control
    if saveQCImages == 1
        qcImage = zeros(size(M));
    end
    
    % Find all the objects in the master - we used a dilate version of the
    % segmented image to reduce the risk of breaking objects
%     [Mlabel, numLabels] = bwlabel(imdilate(M, se));
    [Mlabel, numLabels] = bwlabel(M);
    
    % Go over all objects
    for object = 1 : numLabels
        
        % Get just current object in a mask
        mask = Mlabel == object;
       
        % Bring it back to the original shape
        %mask = bwmorph(mask, 'thin', Inf);
        
        % Make sure we have more than 6 points in an outline...
        if numel(find(mask>0)) <= 6
            continue;
        end
        
        if DEBUG == 1
            figure(H_FIGURE);
            set(H_FIGURE, 'Name', ['Boundary ', num2str(object)]);
            subplot(1, 2, 2);
            cla;
            subplot(1, 2, 1);
            cla;
            imshow(mask, []); 
            hold on;
            
            % Find the coordinates
            [y, x] = find(mask);

            minX = min(x); maxX = max(x);
            minY = min(y); maxY = max(y);
            dRangeX = max( 0.1 * (maxX - minX), 5 );
            dRangeY = max( 0.1 * (maxY - minY), 5 );
            figure(H_FIGURE);
            subplot(1, 2, 1);
            axis([minX - dRangeX maxX + dRangeX ...
                minY - dRangeY maxY + dRangeY]);
            xlabel(['Plane ', num2str(plane), '; object ', num2str(object)]);
            pause;
        end
        
        % Make sure the boundary is closed
        mask = closeBoundary(mask);
        
% % % % %         % Find the coordinates
% % % % %         [y, x] = find(mask);
% % % % %        
% % % % %         % Compare the first with the last coordinate and pick the closest
% % % % %         % to (0, 0) as a starting point for tracing
% % % % %         if (y(1)^2 + x(1)^2) < (y(end)^2 + x(end)^2)
% % % % %             y0 = y(1);
% % % % %             x0 = x(1);
% % % % %         else
% % % % %             y0 = y(end);
% % % % %             x0 = x(end);
% % % % %         end
% % % % %         
% % % % %         % Trace the object boundary (build in some fallbacks)
% % % % %         success = false;
% % % % %         directions = { 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N', 'NE' };
% % % % %         nDir = numel(directions);
% % % % %         d = 1;
% % % % %         while success == false
% % % % %             if d > nDir
% % % % %                 error('Could not trace boundary!');
% % % % %             end
% % % % %             try
% % % % %                 B = bwtraceboundary(mask, [y0 x0], ...
% % % % %                     directions{d}, 8, Inf, 'clockwise');
% % % % %                 success = true;
% % % % %             catch
% % % % %                 success = false;
% % % % %                 d = d + 1;
% % % % %             end
% % % % %         end
% % % % %         

        % Fill
        props = regionprops(mask, {'FilledImage', 'BoundingBox'});
        
        % Check if the filling worked
        nWhitePixelsBefore = numel(find(mask));
        nWhitePixelsAfter  = numel(find(props.FilledImage));
        if nWhitePixelsBefore == nWhitePixelsAfter
           % Filling failed
           aarp = 23;
        end
        bX0 = round(props.BoundingBox(1)); 
        bX  = bX0 + props.BoundingBox(3) - 1;
        bY0 = round(props.BoundingBox(2)); 
        bY  = bY0 + props.BoundingBox(4) - 1;
        mask(bY0 : bY, bX0 : bX) = props.FilledImage;
        
        % Clean up (if we have just a line segment it will disappear, so we
        % keep a copy to run an additional processing step on it)
        maskBackup = mask;
        mask = imerode(mask, se);
        mask = imdilate(mask, se);

        B = bwboundaries(mask);
        if isempty(B)
            mask = maskBackup;
            B = bwboundaries(mask);
            if numel(B) > 1
                disp('More than one boundary for an expected line.');
            end
            B = processBoundary(B{1}); 
        elseif numel(B) > 1
            % If there are two boundaries, usually it is because the
            % outline was not closed and now we have it traced once from
            % outside and once from inside. The one outside is usually the
            % first one. The other one is usually completely enclosed.
            % It can also happen that the boundary was not closed and now
            % we have separate segments.
            % To be sure we calculate the internal areas and pick the
            % largest
            B = findLargestBoundary(B, size(mask));
        else
            B = B{1};
        end

        % Since the objects are empty, the bwtraceboundary algorithm traces
        % them outside and inside, thus practically counting the same
        % positions twice. Since once the tracing is on the inside and once
        % on the outside, the positions are not exactly the same, so we
        % just drop the second half of the trace.
        %         p = round(size(B, 1) / 2) + 1;
        %         B = B(1 : p, 1 : 2);
        if DEBUG == 1
            figure(H_FIGURE);
            subplot(1, 2, 1);
            plot(B(:, 2), B(:, 1), 'r-o', 'MarkerSize', 8); hold on;
        end
        
        % TEMP
        % Disabled
%         B = processBoundary(B);
        % /TEMP
        
        % Smooth the boundary
        B = smoothBoundary(B, 15);

        if DEBUG == 1
            figure(H_FIGURE);
            subplot(1, 2, 1);
            plot(B(:, 2), B(:, 1), 'ks');
            pause;
        end
        
        % Store the boundary
        BOUNDARIES{plane}{object} = B;
        
        % Store all coords (with a pointer to the BOUNDARY)       
        ALL_COORDS{plane} = cat(1, ALL_COORDS{plane}, ...
            [B, object .* ones(size(B, 1), 1)]);
        
        % Check tracing
        if DEBUG == 1
            figure(H_FIGURE);
            subplot(1, 2, 1);
            plot(B(:, 2), B(:, 1), '-', 'LineWidth', 3, ...
                'Color', rand(1, 3));
        end
        
        if DEBUG == 1
            pause;
        end
        
        % Save quality control images
        if saveQCImages == 1
            for p = 1 : size(B, 1)
                yQC = round(B(p, 1));
                if yQC < 1
                    yQC = 1;
                end
                if yQC > size(Mlabel, 1)
                    yQC = size(Mlabel, 1);
                end
                xQC = round(B(p, 2));
                if xQC < 1
                    xQC = 1;
                end
                if xQC > size(Mlabel, 2)
                    xQC = size(Mlabel, 2);
                end
                qcImage(yQC, xQC) = object;
            end
        end
        
    end
    
    % Now save the quality control image
    if saveQCImages == 1
        qcImageRGB = label2rgb(qcImage);
        [~, b, e] = fileparts(fileNames{plane});
        fName = [b, '_post_processed', e];
        imwrite(qcImageRGB, ...
            fullfile(workingFolder, fName), 'Compression', 'none');
    end

end

% Save the results
save(fullfile(workingFolder, 'BOUNDARIES.mat'), ...
    'ALL_COORDS', 'BOUNDARIES');

% =========================================================================
% =========================================================================

function mask = closeBoundary(mask)

global DEBUG
global H_FIGURE

if DEBUG == 1
    maskOrig = mask;
end

% Clean up the mask a bit
mask = bwmorph(mask, 'thin', Inf);

% Get the coordinates
[y, ~] = find(mask);
if numel(y) < 3
    return
end

% props = regionprops(mask, 'FilledImage', 'MajorAxisLength', ...
%     'MinorAxisLength');
props = regionprops(mask, 'MajorAxisLength', 'MinorAxisLength');

% Find the endpoints
% If 0, the outline is closed
% if 2, the outline is opened
% If >2, the outline is opened and on top of that the segmentation was not
% clean.
maskE = bwmorph(mask, 'endpoints');
[ye, xe] = find(maskE);
nEndPoints = numel(ye);
if nEndPoints == 0
    return

elseif nEndPoints == 2

    % Fill in the gap only if the distance is not too large
    dD = sqrt(2) * props.MajorAxisLength;
    d = [ye xe];
    dY = sqrt(sum((d(1, :) - d(2, :)).^2));
    if dY > dD
        return
    end
    nPoints = 2 * fix(dY) + 1; % Make sure to cover all pixels!
    yg = round(linspace(ye(1), ye(2), nPoints));
    xg = round(linspace(xe(1), xe(2), nPoints));
    
    % Make sure to close all edges
    for i = 1 : nPoints
        mask(yg(i), xg(i)) = 1;
    end
    
    % Remove bridges
    mask = bwmorph(mask, 'hbreak');

elseif nEndPoints == 3
    
    % Heuristic: if there are three, close the two closest
    
    dAll = createDistanceMatrix([ye xe], [ye xe]);
    dTri = triu(dAll);
    dTri(dTri == 0) = Inf;
    [m, p] = min(dTri, [], 2);
    c = find(m == min(m));
    r = p(c);
    ye = ye([c; r]);
    xe = xe([c; r]);
    
    % Fill in the gap only if the distance is not too large
    dD = 0.5 * props.MajorAxisLength;
    d = [ye xe];
    dY = sqrt(sum((d(1, :) - d(2, :)).^2));
    if dY > dD
        return
    end
    nPoints = 2 * fix(dY) + 1; % Make sure to cover all pixels!
    yg = round(linspace(ye(1), ye(2), nPoints));
    xg = round(linspace(xe(1), xe(2), nPoints));
    
    % Make sure to close all edges
    for i = 1 : nPoints
        mask(yg(i), xg(i)) = 1;
    end
    
    % Remove bridges
    mask = bwmorph(mask, 'hbreak');    
else
    
%     B = bwboundaries(mask);
%     if numel(B) == 1
%         B = processBoundary(B);
%         mask = false(size(mask));
%         for i = 1 : size(B{1}, 1)
%             mask(B{1}(i, 1), B{1}(i, 2)) = 1;
%         end
%     else
%         aarp = 23;
%     end

   
end

if DEBUG == 1
    maskRGB = zeros([size(mask), 3], 'uint8');
    maskRGB(:, :, 1) = nrm(maskOrig, 8);
    maskRGB(:, :, 2) = nrm(mask, 8);
    figure(H_FIGURE);
    subplot(1, 2, 1);
    a = axis;
    imshow(maskRGB);
    axis(a);
    pause;
end

% =========================================================================

function B = processBoundary(B)

global DEBUG
global H_FIGURE

nElements = size(B, 1);
if nElements < 5
    return;
end
D = zeros(nElements, 4);

for i = 5 : size(B, 1)
    
    a = B(i - 4, :);
    b = B(i - 3, :);
    c = B(i - 2, :);
    d = B(i - 1, :);
    e = B(i    , :);

    D(i, 1) = sqrt(sum((b - a).^2));
    D(i, 2) = sqrt(sum((c - a).^2));
    D(i, 3) = sqrt(sum((d - a).^2));
    D(i, 4) = sqrt(sum((e - a).^2));
    
end

D = sum(D, 2);
m = median(D(5 : end));
% t = m - 7 .* mad(D(5 : end), 1);
t = 0.5 * m;
D(1 : 4) = Inf;
Dm = locmin1d(D);
DM = Inf(size(D));

DM(Dm) = D(Dm);

% Take the largest minima under the threshold
p = find(DM < t, 1, 'last');
if ~isempty(p)
    if DEBUG == 1
        figure(H_FIGURE)
        subplot(1, 2, 2);
        plot(D); hold on; 
        plot(p, D(p), 'ks');
    end
    % We subtract 2 since we must be in the middle of a 5-element vector
    B = B(1 : p - 2, :);
else
    % disp('No cut needed.');
end

% =========================================================================

function largestB = findLargestBoundary(B, imSize)

% % % % maxX = -Inf;
% % % % maxY = -Inf;
% % % % for i = 1 : numel(B)
% % % %     currMaxX = max(B{1}(:, 2));
% % % %     if currMaxX > maxX
% % % %        maxX = currMaxX;
% % % %     end
% % % %     currMaxY = max(B{i}(:, 1));
% % % %     if currMaxY > maxY
% % % %        maxY = currMaxY;
% % % %     end
% % % % end

% % % % M = zeros(maxY + 1, maxX + 1);
M = zeros(imSize);

for j = 1 : numel(B)
    for i = 1 : size(B{j}, 1)
        M(B{j}(i, 1), B{j}(i, 2)) = 1;
    end
end
props = regionprops(M, {'FilledImage', 'BoundingBox'});
bX0 = round(props.BoundingBox(1));
bX  = bX0 + props.BoundingBox(3) - 1;
bY0 = round(props.BoundingBox(2));
bY  = bY0 + props.BoundingBox(4) - 1;
M(bY0 : bY, bX0 : bX) = props.FilledImage;

% Pad the array to avoid border effects
M = padarray(M, [3 3], 0, 'both');

% Now try to fuse them
se = strel('disk', 2);
M = imdilate(M, se);
M = imerode(M, se);

% Remove the pad
M = M(4 : end - 3, 4 : end - 3);

% Find the boundary
B = bwboundaries(M);

nBoundaries = numel(B);

if nBoundaries == 1
    largestB = B{1};
    return
end

% If we still have more than one boundary, we try some additional
% processing

% figure; hold on
% clr = {'r', 'g', 'k', 'b', 'm', 'y'};

areas = zeros(1, nBoundaries);

for i = 1 : nBoundaries
%     plot(B{i}(:, 2), B{i}(:, 1), clr{i})
    areas(i) = calcBoundaryArea(B{i});
end

indx = find(areas == max(areas), 1, 'first');

% Check if the smaller ones are contained in the larger one. If not we
% "fuse" them.
largestB = B{indx};
B(indx) = [];
toBeDel = [];
c = 0;
for i = 1 : nBoundaries - 1
   
    % TODO
    in = inpolygon(B{i}(:,2), B{i}(:,1), largestB(:, 2), largestB(:, 1));
    if numel(find(in == 1)) / numel(in) > 0.95
        c = c + 1;
        toBeDel(c) = i;
    end
    
end

% If there are "external" boundaries, fuse them to the largest one
if ~isempty(toBeDel)
    B(toBeDel) = [];
end

nB = numel(B);
if nB > 0
    for i = 1 :nB
        largestB = cat(1, largestB, B{i});
    end
end


% =========================================================================

function area = calcBoundaryArea(B)

M = zeros(max(B(:, 1)) + 1, max(B(:, 2)) + 1);
for i = 1 : size(B, 1)
    M(B(i, 1), B(i, 2)) = 1;
end
props = regionprops(M, 'FilledImage');
area = numel(props.FilledImage > 0);

% =========================================================================

function coords = smoothBoundary(coords, frame)

% Filter the coordinates
if size( coords, 1 ) > frame
    if mod( frame, 2 ) == 0
        frame = frame + 1;
    end
    % Pad ...
    yCoords = [ repmat( coords( 1, 1 ), frame, 1 ); coords( :, 1 ); repmat( coords( end, 1 ), frame, 1 ) ];
    xCoords = [ repmat( coords( 1, 2 ), frame, 1 ); coords( :, 2 ); repmat( coords( end, 2 ), frame, 1 ) ];
    % ... filter ...
    yCoords = sgolayfilt( yCoords, 3, frame );
    xCoords = sgolayfilt( xCoords, 3, frame );
    % Remove pad
    coords( :, 1 ) = yCoords( frame + 1 : end - frame );
    coords( :, 2 ) = xCoords( frame + 1 : end - frame );
    % Close outline smoothly
    coords( end, 1 : 2 ) = coords( 1, 1 : 2 );
end
