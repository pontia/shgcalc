function analysis()

% Same as next but with Windows paths (notice that the relevant functions
% now take paths as input parameters)

% GROUND TRUTH WITH VERNIERS

dataBackupDir = 'D:\data\SHG\segmented_postprocessed_backup';
workDir = 'D:\data\SHG\results\analysis_24';

extendLength = 7;

% I manually corrected some issues in post_process_line_segments: to rerun
% analysis, first call this function that copies the corrected functions
% into the folder for subsequent analysis
copyCorrectedVerniersFilesToFolder(dataBackupDir, workDir)

% Post-process the outlines
post_process_line_segments(workDir, 1);

% Extract normals
extract_normals(workDir, 0.132, 0.250, 25, 1, 0.025, extendLength);

% Export normals to csv
exportNormalsToCSV(workDir);

% Append extendLength to workDir name
movefile(workDir, [workDir, '_extend_', num2str(extendLength)]);

return;

%%
%   This is the same as the one above, just with Windows paths. 
%

% GROUND TRUTH WITH VERNIERS

workDir = '/work/projects/bill/GroundTruth_Verniers/aaron/analysis_24';

extendLength = 7;

% I manually corrected some issues in post_process_line_segments: to rerun
% analysis, first call this function that copies the corrected functions
% into the folder for subsequent analysis
copyCorrectedVerniersFilesToFolder()

% Post-process the outlines
post_process_line_segments(workDir, 1);

% Extract normals
extract_normals(workDir, 0.132, 0.250, 25, 1, 0.025, extendLength);

% Export normals to csv
exportNormalsToCSV(workDir);

% Append extendLength to workDir name
movefile(workDir, [workDir, '_extend_', num2str(extendLength)]);

return;

%%
%% OLDER ANALYSES (OBSOLETE!)
%%

% GROUND TRUTH

% I manually corrected some issues in post_process_line_segments: to rerun
% analysis, first call this function that copies the corrected functions
% into the folder for subsequent analysis
copyCorrectedFilesToFolder()

% Extract normals
extract_normals('/work/projects/bill/GroundTruth/analysis_24', 0.132, 0.250, 5, 1, 0.025);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/GroundTruth/analysis_24');

return;

% GROUND TRUTH (Aaron's only)

% Segment the images
segment_regions(1, '/work/projects/bill/GroundTruth', {'GroundTruth_AaronOnly'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/GroundTruth/GroundTruth_AaronOnly/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/GroundTruth/GroundTruth_AaronOnly/analysis_24', 0.132, 0.250, 5, 1, 0.025);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/GroundTruth/GroundTruth_AaronOnly/analysis_24');

return;

% =========================================================================

% REGION1_SUBREGION_1

% Segment the images
segment_regions(1, '/work/projects/bill/', {'subregion1_q1_z_82_147'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/subregion1_q1_z_82_147/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/subregion1_q1_z_82_147/analysis_24', 0.132, 0.250, 5, 1, 0.025);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/subregion1_q1_z_82_147/analysis_24');

% =========================================================================

% REGION1_SUBREGION_2

% Segment the images
segment_regions(1, '/work/projects/bill/', {'subregion1_q2_z_23_55'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/subregion1_q2_z_23_55/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/subregion1_q2_z_23_55/analysis_24', 0.132, 0.250, 5, 1, 0.025);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/subregion1_q2_z_23_55/analysis_24');

% =========================================================================

% REGION1_SUBREGION_3

% Segment the images
segment_regions(1, '/work/projects/bill/', {'subregion1_q3_z_1_14'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/subregion1_q3_z_1_14/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/subregion1_q3_z_1_14/analysis_24', 0.132, 0.250, 5, 1, 0.025);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/subregion1_q3_z_1_14/analysis_24');

return;

% =========================================================================

% REGION 1

% Segment the images
segment_regions(1, '/work/projects/bill/', {'region1'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/region1/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/region1/analysis_24', 0.132, 0.250, 5, 1);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/region1/analysis_24');

% =========================================================================

% REGION 2

% Segment the images
segment_regions(1, '/work/projects/bill/', {'region2'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/region2/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/region2/analysis_24', 0.132, 0.250, 5, 1);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/region2/analysis_24');

% =========================================================================

% REGION 3

% Segment the images
segment_regions(1, '/work/projects/bill/', {'region3'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/region3/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/region3/analysis_24', 0.132, 0.250, 5, 1);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/region3/analysis_24');

% =========================================================================

% REGION 4

% Segment the images
segment_regions(1, '/work/projects/bill/', {'region4'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/region4/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/region4/analysis_24', 0.132, 0.250, 5, 1);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/region4/analysis_24');

% =========================================================================

% REGION 5

% Segment the images
segment_regions(1, '/work/projects/bill/', {'region5'}, 24);

% Post-process the outlines
post_process_line_segments('/work/projects/bill/region5/analysis_24', 1);

% Extract normals
extract_normals('/work/projects/bill/region5/analysis_24', 0.132, 0.250, 5, 1);

% Export normals to csv
exportNormalsToCSV('/work/projects/bill/region5/analysis_24');

