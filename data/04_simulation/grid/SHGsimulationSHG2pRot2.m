function [ signal ] = SHGsimulationSHG2pRot2(theta)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    period1 = 5;
    period2 = 5;
    sarcwidth = 1;
    sarcseparation = 0.15;
    numperiods = 1;
    offset = 0.4;
    
    lambda = 0.890;
    w0 = lambda/(nindex(lambda)*pi*0.8);
    b = (nindex(lambda)*2*pi/lambda)*w0^2;
    
    resolution = 0.02;
    downsample = 4;
    resolutionx = resolution*downsample;
    yfactor = 3;
    resolutiony = resolutionx*yfactor;
    
    totalwidth = 4*(sarcwidth+sarcseparation+offset)+2*lambda;

   
    intx = -2*lambda:resolution:2*lambda;
    inty = -5*w0:resolution:5*w0;
    
    bufferwidth = length(intx) + 1;
    
    y = 0:resolution:(totalwidth);
    x = 0:resolution:(numperiods*(period1 + period2));
    
    
   
    
    y = -2*lambda:resolutiony:(totalwidth+lambda);
    x = -2*lambda:resolutionx:(numperiods*(period1 + period2)+0*numperiods*5+2*lambda);
    y2 = -2*lambda:resolution:(totalwidth+lambda);
    x2 = -2*lambda:resolution:(numperiods*(period1 + period2)+0*numperiods*5+2*lambda);
    
    
    signal = zeros(length(x2),length(y2));

    
    muscle = zeros(length(x2),length(y2));
    
    duty = 80;
    phase = 180 * pi / 180.0;
    muscle = sarcomere(x2,y2+sarcwidth+sarcseparation,period1,sarcwidth,phase,duty)+sarcomere(x2,y2,period1,sarcwidth,phase,duty) + sarcomere(x2,y2-(sarcseparation+sarcwidth),period1,sarcwidth,phase,duty) + sarcomere(x2,y2-((2*sarcseparation+offset)+2*sarcwidth) ,period2,sarcwidth,0,duty) + sarcomere(x2,y2-((3*sarcseparation+offset)+3*sarcwidth) ,period2,sarcwidth,0,duty)+ sarcomere(x2,y2-((4*sarcseparation+offset)+4*sarcwidth) ,period2,sarcwidth,0,duty)+ sarcomere(x2,y2-((5*sarcseparation+offset)+5*sarcwidth) ,period2,sarcwidth,0,duty); 
    %muscle = sarcomere(x2,y2-(sarcseparation+sarcwidth),period1,sarcwidth,pi,duty) + exp(1i*pi)*sarcomere(x2,y2-((2*sarcseparation+offset)+2*sarcwidth) ,period2,sarcwidth,0,duty) 
    figure;
    imagesc(resolution*(0:(x2(length(y2))-x2(1))),resolution*(0:(y2(length(y2))-y2(1))),real(muscle));

        
    dtheta = 0.05;
    thetamax = asin(0.55/1.4); %max collection angle
    thetas = 0:dtheta:thetamax; 
    dphi = 0.2;
    phis = 0:dphi:2*pi;
    
    PSF = zeros(length(intx),length(intx),length(thetas),length(phis));
    PSF2p = zeros(length(intx));
    theta = theta*pi/180;
    
    %xnew = intx*cos(theta) - inty*sin(theta);
    %ynew = intx*sin(theta) + inty*cos(theta);
    for i = 1:length(intx)
        for j = 1:length(intx)
            xnew = intx(i)*cos(theta) - intx(j)*sin(theta);
            ynew = intx(i)*sin(theta) + intx(j)*cos(theta);
            PSF2p(i,j)  = laser2p(xnew,ynew,0,0,[w0,lambda]);
        end
    end
    
    for m = 1:length(thetas)
        for n = 1:length(phis)
            thetaout = thetas(m);
            phiout = phis(n);
            temp = zeros(length(intx),length(intx));
            for i = 1:length(intx)
                parfor j = 1:length(intx)
                    xnew = intx(i)*cos(theta) - intx(j)*sin(theta);
                    ynew = intx(i)*sin(theta) + intx(j)*cos(theta);
                    PSF(i,j,m,n) = laser2(xnew,ynew,0,0,[w0,lambda,thetaout,phiout]);
                end             
            end          
        end
    end
    
    
    %throw away very dim points
    %PSF(abs(PSF) < abs(laser(1.5*w0,3*b,0,0,[w0,lambda]))) = 0;
    
    %figure;
    %contourf(abs(PSF));
    sigsize = size(signal)
    
    lix = floor(length(intx)/2);
    
    sigtemp = zeros([size(muscle),length(thetas),length(phis)]);
    for i=1:length(thetas)
        parfor j = 1:length(phis)
            sigtemp(:,:,i,j) = dtheta*dphi*abs(conv2(muscle,PSF(:,:,i,j),'same')).^2;
        end
    end
    
  
    for(i=1:length(thetas))
        for j = 1:length(phis)
      %  figure;
      %  imagesc(0:resolution:(x2(length(y2))-x2(1)),0:resolution:(y2(length(y2))-y2(1)),sigtemp(:,:,i));
     
         signal = signal + (cos(thetas(i))^2 * sin(phis(j))^2 + cos(phis(j))^2)*sigtemp(:,:,i,j);
        end
    end
    
     figure;
     imagesc(resolution*(0:(x2(length(y2))-x2(1))),resolution*(0:(y2(length(y2))-y2(1))),signal);
     
     %figure;
     %imagesc(downsample(signal,5));
     
     signal2p = abs(conv2(abs(muscle),PSF2p.^2,'same')).^2;
     figure;
     imagesc(resolution*(0:(x2(length(y2))-x2(1))),resolution*(0:(y2(length(y2))-y2(1))),signal2p);
 
end

function result = sarcomere(x,y,period,width,phase,dutycycle)  
    cutoff = 0;
    
    myophase = true;
    if(myophase == 1)
     s = max(square(x*2*pi/period + phase,dutycycle),0).*square((x*2*pi/period + phase),dutycycle/2);
    else
        s = max(square(x*pi/period + phase,dutycycle),0);
    end
     prand = 1;%exp(1i*2*pi*rand(1)*0.1)*0.5*(1+rand(1));
     
  
  %  prand = cos(2*pi*1i*rand(1,length(y))/8);
    
    for j=1:length(y)       
       
        md = 1;%mod(j,2);
        for i=1:length(x)  
            md = s(i);
            if ( ( y(j) < width) &&  (y(j) > 0)  ) %&& (mod(j,3)==0) &&  (mod(i,3)==0)
                
                result(i,j) = md*prand;%*4*(1+square(2*pi*y(j)/4,25));
            else
                result(i,j) = 0;
            end
        end
    end    
end


%propgation direction = y
function intensity = laser(x,y,centerx,centery,params)
    w0 = params(1);
    lambda = params(2);
    n=nindex(lambda);
    k = 2*pi/lambda;
    %zr = n*pi*w0^2/lambda;
    %w2 = (w0*( 1 + ((y-centery)/zr).^2)).^-2;
    %wz = (w0*( 1 + ((y-centery)/zr).^2)).^-1;
    y = y-centery;
    x = x-centerx;
    %phase = exp(-1i*(y*k + k*(x.^2 )./(2*(y+0.000001)*(1+(zr/(y))^2)) - atan(y/zr+0.0000001)));
    %amplitude = w0 * wz .* exp( -( x.^2  ).* w2);
    %intensity = phase*amplitude;
    xi = 2*y/(n*k*w0^2);
    dk=-2*k*(n-nindex(lambda/2)) + gvdiff(lambda);
    
    %xfinal = -lambda/2;
    %yfinal = 5*lambda;
    %xlim=5*lambda;
    r = 10;
    %offsetfactor = 1i*k*(x).^2 ./r;%sqrt(yfinal-y).* (erf(-(xlim-x).*sqrt(k./(yfinal-y)))+erf(-(x+xlim).*sqrt(k./(yfinal-y))))/sqrt(k);%i*k*(x-xfinal).^2 ./(y-yfinal);
    intensity = 1/(1+1i*xi).^2 .* exp(-2*x.^2/(w0^2*(1+1i*xi))+1i*dk*y);%.*offsetfactor; %divide by two in the last term because of phase matching isn't squared
end

function intensity = laser2(x,y,centerx,centery,params)
    w0 = params(1);
    lambda = params(2);
    theta = params(3);
    phi = params(4);
    
    n=nindex(lambda);
    k = 2*pi/lambda;
    %zr = n*pi*w0^2/lambda;
    %w2 = (w0*( 1 + ((y-centery)/zr).^2)).^-2;
    %wz = (w0*( 1 + ((y-centery)/zr).^2)).^-1;
    y = y-centery;
    x = x-centerx;
    %phase = exp(-1i*(y*k + k*(x.^2 )./(2*(y+0.000001)*(1+(zr/(y))^2)) - atan(y/zr+0.0000001)));
    %amplitude = w0 * wz .* exp( -( x.^2  ).* w2);
    %intensity = phase*amplitude;
    xi = 2*y/(n*k*w0^2);
   % dk=2*k*(n-nindex(lambda/2)) + gvdiff(lambda);
    
    %xfinal = -lambda/2;
    %yfinal = 5*lambda;
    %xlim=5*lambda;
 
    %offsetfactor = 1i*k*(x).^2 ./r;%sqrt(yfinal-y).* (erf(-(xlim-x).*sqrt(k./(yfinal-y)))+erf(-(x+xlim).*sqrt(k./(yfinal-y))))/sqrt(k);%i*k*(x-xfinal).^2 ./(y-yfinal);
    intensity = 1/(1+1i*xi).^2 .* exp(-2*x.^2/(w0^2*(1+1i*xi))+1i*(2*k*n*y- 2*k*nindex(lambda/2)*(sin(theta)*sin(phi)*x + cos(theta)*y)));%.*offsetfactor; %divide by two in the last term because of phase matching isn't squared
end



function index = nindex(lambda)
 index = 1.*(1.36788- 0.000167006/lambda^4 + 0.00883429/lambda^2);
end

function indexd = nindexd(lambda)
  dl = 0.0001;
  indexd = (nindex(lambda) - nindex(lambda-dl))/dl;
end

function gvd = gvdiff(lambda)
    gv1 = 2*pi*3*10^8/lambda * (nindex(lambda)/300000000000000 - (lambda*nindexd(lambda))/300000000000000);
    gv2 = 2*pi*3*10^8/(0.5*lambda) * (nindex(0.5*lambda)/300000000000000 - (0.5*lambda*nindexd(0.5*lambda))/300000000000000);
    gvd = 0.5*gv2-gv1;
end

function intensity = laser2p(x,y,centerx,centery,params)
    w0 = params(1);
    lambda = params(2);
    n=nindex(0.850);
    k = 2*pi/lambda;
    zr = pi*w0^2/lambda;
    %w2 = (w0*( 1 + ((y-centery)/zr).^2)).^-2;
    %wz = (w0*( 1 + ((y-centery)/zr).^2)).^-1;
    y = y-centery;
    x = x-centerx;
    %phase = exp(-1i*(y*k + k*(x.^2 )./(2*(y+0.000001)*(1+(zr/(y))^2)) - atan(y/zr+0.0000001)));
    %amplitude = w0 * wz .* exp( -( x.^2  ).* w2);
    %intensity = phase*amplitude;
    xi = 2*y/(n*k*w0^2);
    dk=2*(n-nindex(0.850/2))*k;
    
    intensity = abs(1/(1+1i*xi) * exp(-x.^2/(w0^2*(1+1i*xi)))); %divide by two in the last term because of phase matching isn't squared
end

function z=ifelse(statement,trueans,falseans)
if statement
    z=trueans;
else
    z=falseans;
end
end


