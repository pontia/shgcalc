function SHGsimulationRecreateFiguresBatch(sim_folder)


step = pi / 16;
resolution = 0.02;

if nargin == 0
    sim_folder = 'C:\Users\pontia\Documents\projects_collaborations\shgcalc\data\04_simulation\grid\test\neg';
end

phase = 0 : step : (2 * pi - step);

for i = 1 : numel(phase)
 
    clear signal
    clear PSFP2
    
    phaseStr = sprintf('%5.4f', phase(i));

    matName = fullfile(sim_folder, ['output_theta_0_phase_', phaseStr, '_resolution_0.02.mat']);
    
    if ~exist(matName, 'file')
        continue
    end
    
    S = load(matName);
    
    if ~isfield(S, 'PSF2p')
        disp(['Skipping phase ', num2str(phase(i)), '...'])
        
        continue
    end
    
    

    [output, fH1, fH2, fH3]  = SHGsimulationRecreateFigures(phase(i), resolution, S.output, S.PSF2p);

    basename = ['output_theta_0_phase_', phaseStr, '_resolution_', num2str(resolution)];

    save(fullfile(sim_folder, [basename, '.mat']), 'output');

    saveas(fH1, fullfile(sim_folder, [basename, '_1.fig']), 'fig');
    saveas(fH1, fullfile(sim_folder, [basename, '_1.bmp']), 'bmp');

    saveas(fH2, fullfile(sim_folder, [basename, '_2.fig']), 'fig');
    saveas(fH2, fullfile(sim_folder, [basename, '_2.bmp']), 'bmp');

    saveas(fH3, fullfile(sim_folder, [basename, '_3.fig']), 'fig');
    saveas(fH3, fullfile(sim_folder, [basename, '_3.bmp']), 'bmp');

    close(fH1);
    close(fH2);
    close(fH3);
    
end
