function [ signal, fH1, fH2, fH3 ] = SHGsimulationRecreateFigures(phase, resolution, signal, PSF2p)

    period1 = 5;
    period2 = 5;
    sarcwidth = 1;
    sarcseparation = 0.15;
    numperiods = 1;
    offset = 0.4;
    
    lambda = 0.890;
    
    if nargin == 2
        resolution = 0.02;
    end
        
   
    totalwidth = 4*(sarcwidth+sarcseparation+offset)+2*lambda;

   
    y2 = -2*lambda:resolution:(totalwidth+lambda);
    x2 = -2*lambda:resolution:(numperiods*(period1 + period2)+0*numperiods*5+2*lambda);
    
    
    
    
    duty = 80;
 
    
    muscle = sarcomere(x2,y2+sarcwidth+sarcseparation,period1,sarcwidth,phase,duty)+sarcomere(x2,y2,period1,sarcwidth,phase,duty) + sarcomere(x2,y2-(sarcseparation+sarcwidth),period1,sarcwidth,phase,duty) + sarcomere(x2,y2-((2*sarcseparation+offset)+2*sarcwidth) ,period2,sarcwidth,0,duty) + sarcomere(x2,y2-((3*sarcseparation+offset)+3*sarcwidth) ,period2,sarcwidth,0,duty)+ sarcomere(x2,y2-((4*sarcseparation+offset)+4*sarcwidth) ,period2,sarcwidth,0,duty)+ sarcomere(x2,y2-((5*sarcseparation+offset)+5*sarcwidth) ,period2,sarcwidth,0,duty); 

    fH1 = figure;
    imagesc(resolution*(0:(x2(length(y2))-x2(1))),resolution*(0:(y2(length(y2))-y2(1))),real(muscle));

   
     fH2 = figure;
     imagesc(resolution*(0:(x2(length(y2))-x2(1))),resolution*(0:(y2(length(y2))-y2(1))),signal);
     
     %figure;
     %imagesc(downsample(signal,5));
     
     signal2p = abs(conv2(abs(muscle),PSF2p.^2,'same')).^2;
     fH3 = figure;
     imagesc(resolution*(0:(x2(length(y2))-x2(1))),resolution*(0:(y2(length(y2))-y2(1))),signal2p);
 
end

function result = sarcomere(x,y,period,width,phase,dutycycle)  
    cutoff = 0;
    
    myophase = true;
    if(myophase == 1)
     s = max(square(x*2*pi/period + phase,dutycycle),0).*square((x*2*pi/period + phase),dutycycle/2);
    else
        s = max(square(x*pi/period + phase,dutycycle),0);
    end
     prand = 1;%exp(1i*2*pi*rand(1)*0.1)*0.5*(1+rand(1));
     
  
  %  prand = cos(2*pi*1i*rand(1,length(y))/8);
    
    for j=1:length(y)       
       
        md = 1;%mod(j,2);
        for i=1:length(x)  
            md = s(i);
            if ( ( y(j) < width) &&  (y(j) > 0)  ) %&& (mod(j,3)==0) &&  (mod(i,3)==0)
                
                result(i,j) = md*prand;%*4*(1+square(2*pi*y(j)/4,25));
            else
                result(i,j) = 0;
            end
        end
    end    
end

